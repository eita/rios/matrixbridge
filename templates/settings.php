<?php
/**

 *
 * @author Vinicius Brand <vinicius@eita.org.br>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
script('matrixbridge', 'settings');
style('matrixbridge', 'style');
?>

<form id="riotjs" class="section" action="#" method="post">
	<h1><?php p($l->t('UI - Element integration')); ?></h1>

	<p><?php p($l->t('Element web URL')); ?></p>
	<input type="text" id="matrixbridge-riot-url" name="matrixbridge-riot-url"
		   value="<?php p($_['riotUrl']); ?>"/>

    <p><?php p($l->t(''))?></p>


	<br/><br/>
    <h1><?php p($l->t('Matrix Rooms integration with Nextcloud Circles')); ?></h1>

    <h2><?php p($l->t('User Groups and Matrix Rooms')); ?></h2>

	<p><?php p($l->t('When an user is inserted into a nextcloud group, they will be automatically inscribed into a specific matrix room (Group Room) that is related to that nextcloud group. By this way, this user will have other users of the group visible in matrix upon entrance in the group.'))?></p>
    <br/>
    <p><?php p($l->t('Matrix user - Group Rooms owner credentials'))?></p>
    <input type="text" id="matrixbridge-group-rooms-owner" name="matrixbridge-group-rooms-owner"
           value="<?php p($_['groupRoomsOwnerName']); ?>" placeholder="<?php p($l->t('Username'))?>"/>
    <input type="text" id="matrixbridge-group-rooms-owner-pw" name="matrixbridge-group-rooms-owner-pw"
           value="<?php p($_['groupRoomsOwnerPassword']); ?>" placeholder="<?php p($l->t('Password'))?>"/>

    <br/>
    <p><?php p($l->t('Home Server url'))?></p>
    <input type="text" id="matrixbridge-homeserver-url" name="matrixbridge-homeserver-url"
           value="<?php p($_['homeserverUrl']); ?>" placeholder="<?php p($l->t('Ex: https://matrix.example.com:8448'))?>"/>
    <p><?php p($l->t('Domain used in users/rooms identifiers, e.g. "matrix.org" in "@user:matrix.org"'))?></p>
    <input type="text" id="matrixbridge-identifiers-domain" name="matrixbridge-identifiers-domain"
           value="<?php p($_['identifiersDomain']); ?>" placeholder="<?php p($l->t('Ex: matrix.org in @user:matrix.org'))?>"/>

</form>
