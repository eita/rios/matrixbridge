<?php
script('matrixbridge', 'login');
style('matrixbridge', 'style');
?>

<?php if ($_['riot_url']) { ?>
	<div class="container" id="matrix_loading" style="">
		<div class="alert alert-info" role="alert"><strong id="matrix_loading_text">Carregando o chat, aguarde...</strong></div>
	</div>
    <iframe id="matrixIframe"
			scrolling="no"
			frameborder="0"
            style="position: relative; height: 100%; width: 100%; visibility: hidden;"
			src="<?php p($_['riot_url']) ?>"
			allow="camera; microphone" >

    </iframe>

	<div class="container" style="display:none;" id="matrix_error">
		<div class="alert alert-danger" role="alert"><strong>Atenção!</strong> Não foi possível carregar o chat. Caso este erro persista entre em contato conosco enviando um email para o endereço <strong><a href="mailto:incoming+eita/rios/matrixbridge@gitlab.com">incoming+eita/rios/matrixbridge@gitlab.com</a></strong></div>
	</div>
<?php } else { ?>
<p><?php p($l->t('The Riot URL must be configured in the chat plugin settings.')); ?></p>
<?php } ?>
