<?php

namespace OCA\MatrixBridge\Service;

use MatrixOrg_API;
use OCA\MatrixBridge\Db\MatrixUser;
use OCA\MatrixBridge\Db\MatrixUserMapper;

require_once \OC_App::getAppPath('matrixbridge') . '/3rdparty/matrixorg_php_client/autoload.php';

/**
 * Creates or gets the current Matrix connection
 */
class MatrixConnectionManager
{

    private $userMapper;
    private $matrixUtils;

    public function __construct(MatrixUserMapper $matrixUserMapper, MatrixUtils $utils)
    {
        $this->userMapper = $matrixUserMapper;
        $this->matrixUtils = $utils;
    }

    /**
     * Gets connection token for username and homeserver
     *
     * @param $uid
     * @param $homeserver
     * @param null $password if not null, tries to create connection if inexistent
     */
    public function getConnectionInfo($uid, $homeserver_url)
    {
        $api = new MatrixOrg_API($homeserver_url);

        try {
            $matrixUser = $this->fetchStoredMatrixUser($uid, $homeserver_url);
            $no_token_found_message = 'No valid token found. Please logout and login again in nextcloud to generate a new token, or reset your password.';

            if ($matrixUser) {
                $this->matrixUtils->log("Fetch stored user ($uid) in db:FOUND");
                $result = $matrixUser->verifyToken($api, $this->matrixUtils);
                if ($result === true) {
                    return array(
                        'home_server_url' => $matrixUser->getHomeServerUrl(),
                        'identity_server_url' => 'https://vector.im',
                        'username' => $matrixUser->getUsername(),
                        'matrix_username' => $matrixUser->getMatrixUsername(),
                        'matrix_token' => $matrixUser->getMatrixTokenDecrypted(),
                        'device_id' => $matrixUser->getDeviceId(),
                        'is_new_session' => false
                    );
                } else if ($result === false) {
                    $this->matrixUtils->log("deleted invalid token for user {$uid}");
                    $this->deleteMatrixToken($uid, $homeserver_url);
                    $error_message = $no_token_found_message;
                } else if ($result === null) { //$result === null
                    $error_message = "Matrix homeserver is down or unreachable.";
                }
            } else {
                $error_message = $no_token_found_message;
            }
            $this->matrixUtils->log($error_message);
            return array(
                'error' => $error_message
            );
        } catch (\Exception $e) {
            $this->matrixUtils->log("EXCEPTION in matrixbridge (user: $uid): " . json_encode($e, JSON_PRETTY_PRINT));
            return array('error' => $e->getMessage());
        }
    }

    private function fetchStoredMatrixUser($uid, $homeserver_url)
    {
        if ($this->userMapper) {
            return $this->userMapper->findByUidAndHomeServer($uid, $homeserver_url);
        }
        return null;
    }

    private function deleteMatrixToken($uid, $homeserver_url)
    {
        if ($this->userMapper) {
            $user = $this->fetchStoredMatrixUser($uid, $homeserver_url);

            if ($user) {
                $this->userMapper->delete($user);
            }
        }
    }
}
