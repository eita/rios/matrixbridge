<?php

namespace OCA\MatrixBridge\Service;

use MatrixOrg_API;
use OCA\Circles\Db\CircleRequest;
use OCA\Circles\Model\BaseMember;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\MatrixBridge\Db\MatrixGroupRoom;
use OCA\MatrixBridge\Db\MatrixGroupRoomMapper;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Helper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\BackgroundJob\IJobList;
use OCP\IGroupManager;
use OCP\IL10N;
use OCP\ILogger;
use OCP\Image;
use OCP\IUserManager;
use OCP\IUserSession;
use Sabre\VObject\Reader;


/**
 * Acts upon Matrix server (Client-Server API)
 * http://matrix.org/docs/spec/client_server/r0.2.0.html
 */
class MatrixClient
{
    private $groupRoomMapper;
    private $jobList;
    private $logger;
    private $l10n;
    private $helper;
    private $userMapper;
    private $userManager;
    private $matrixUtils;
    private $roomCircleMapper;
    private $userSession;
    private $circleRequest;

    /** @var IGroupManager  */
    private $groupManager;

    private $userId;

    /** @var  MatrixOrg_API */
    private $matrixApis = array();

    private $newAvatars = array();

    private $BOT_UID = "bote";

    public function __construct(MatrixGroupRoomMapper $groupRoomMapper,
                                IJobList              $jobList,
                                ILogger               $logger,
                                IL10N                 $l10n,
                                MatrixUserMapper      $userMapper,
                                IUserManager          $userManager,
                                MatrixUtils           $utils,
                                RoomCircleMapper      $roomCircleMapper,
                                IUserSession          $userSession,
                                CircleRequest         $circleRequest,
                                IGroupManager         $groupManager)
    {
        $this->groupRoomMapper = $groupRoomMapper;
        $this->jobList = $jobList;
        $this->logger = $logger;
        $this->l10n = $l10n;
        $this->helper = new Helper(\OC::$server->getConfig());
        $this->userMapper = $userMapper;
        $this->userManager = $userManager;
        $this->matrixUtils = $utils;
        $this->roomCircleMapper = $roomCircleMapper;
        $this->userSession = $userSession;
        $this->circleRequest = $circleRequest;
        $this->groupManager = $groupManager;

        register_shutdown_function(array($this, 'changeMatrixAvatarOnShutdown'));
    }

    public function createGroup($gid)
    {
        $groupRooms = $this->groupRoomMapper->findAllByGid($gid);

        if (!count($groupRooms)) {
            try {
                $api = $this->getMatrixApiForGroupRoomsOwner();

                $result = $api->createRoom($gid);
                if ($result['status'] == 200) {
                    $mxRoomId = $result['data']['room_id'];
                    $groupRoom = new MatrixGroupRoom();
                    $groupRoom->setGid($gid);
                    $groupRoom->setRoomId($mxRoomId);
                    $groupRoom->setCreatedAt(time());
                    return $this->groupRoomMapper->insert($groupRoom);
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CreateGroup", [
                    'gid' => $gid
                ]);
            }
        }

    }

    public function addUserToGroup($gid, $uid)
    {
        $groupRooms = $this->groupRoomMapper->findAllByGid($gid);

        if (count($groupRooms)) {
            try {
                $api = $this->getMatrixApiForGroupRoomsOwner();
                $api2 = $this->getMatrixApiForUser($uid);

                for ($i = 0, $iMax = count($groupRooms); $i < $iMax; $i++) {
                    $groupRoom = $groupRooms[$i];
                    $result = $api->invite($groupRoom->getRoomId(), $api2->getUserId());

                    if ($result['status'] == 200) {
                        $result2 = $api2->join($groupRoom->getRoomId());

                        if ($result2['status'] != 200) {
                            $this->logger->error('ERROR IN JOINING GROUP ROOM: ' . json_encode($result2['data']), array('matrixbridge', 'grouphooks'));
                        }
                    }
                }
            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\AddUserToGroup", [
                    'uid' => $uid,
                    'gid' => $gid
                ]);
            }
        }
    }

    public function removeUserFromGroup($gid, $uid)
    {

        $groupRooms = $this->groupRoomMapper->findAllByGid($gid);

        if (count($groupRooms)) {
            try {
                $api = $this->getMatrixApiForGroupRoomsOwner();
                $api2 = $this->getMatrixApiForUser($uid);

                for ($i = 0, $iMax = count($groupRooms); $i < $iMax; $i++) {
                    $groupRoom = $groupRooms[$i];
                    $reason = $this->l10n->t('You were removed from this room because you left group %s.', [$gid]);

                    $result = $api->kick($groupRoom->getRoomId(), $api2->getUserId(), $reason);

                    if ($result['status'] != 200) {
                        $this->logger->error('ERROR IN KICKING FROM GROUP ROOM: ' . json_encode($result['data']), array('matrixbridge', 'grouphooks'));
                    }
                }
            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\RemoveUserFromGroup", [
                    'uid' => $uid,
                    'gid' => $gid
                ]);
            }
        }
    }

    /**
     * Changes matrix user's avatar
     * Gets the avatar from avatarmanager
     *
     * @param string $uid
     */
    public function updateUserAvatar($uid)
    {
        $this->newAvatars[$uid] = $uid;
    }

    public function changeUserDisplayName($uid, $newDisplayName)
    {
        try {
            $matrixUsers = $this->userMapper->findAllByUser($uid);
            foreach ($matrixUsers as $matrixUser) {
                $api = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixUser->getMatrixTokenDecrypted());
                $api->setUserId($matrixUser->getMatrixUsername());

                $currentUserData = $api->getUserProfile($matrixUser->getMatrixUsername());

                #checks if server's displayname is equal to the new value; if positive, does not change anything.
                if ($currentUserData['status'] == 200 && $currentUserData['data']['displayname'] != $newDisplayName) {
                    $api->changeDisplayName($newDisplayName);
                }
            }
        } catch (\MatrixOrg_Exception $e) {
            //Try again later
            $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\ChangeUserDisplayName", [
                'uid' => $uid,
                'displayName' => $newDisplayName
            ]);
        }
    }

    /**
     * Awaits until the script is finished to change the avatar in matrix, as nextcloud destroys
     * and then add a new avatar, and we desire just the last operation be triggered.
     */
    public function changeMatrixAvatarOnShutdown()
    {

        foreach ($this->newAvatars as $uid) {
            try {
                $matrixUsers = $this->userMapper->findAllByUser($uid);

                if (count($matrixUsers)) {
                    $user = $this->userManager->get($uid);
                    $newAvatar = $user->getAvatarImage(-1);

                    foreach ($matrixUsers as $matrixUser) {

                        $api = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixUser->getMatrixTokenDecrypted());
                        $api->setUserId($matrixUser->getMatrixUsername());

                        $status = $api->getUserProfile();

                        if ($newAvatar) {
                            //Downloads current avatar for the user in Matrix, will upload just in case avatars differ
                            $mxcAddress = $status['data']['avatar_url'];
                            $currentAvatar = null;
                            if ($mxcAddress) {
                                $currentAvatar = new Image();
                                $currentAvatar->loadFromFileHandle(fopen($api->downloadUrl($mxcAddress), 'rb'));
                                $currentAvatar->centerCrop();
                            }

                            if ($currentAvatar == null or $this->matrixUtils->compareImages($currentAvatar, $newAvatar) > 11) {
                                $api->changeAvatar($newAvatar->data(), $newAvatar->mimeType());
                            }
                        } else {
                            $api->changeAvatar(null, null);
                        }
                    }
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\ChangeUserAvatar", [
                    'uid' => $uid
                ]);
            }
        }
    }

    /**************************************************************************
     *                             CIRCLE METHODS                             *
     **************************************************************************/

    /**
     * @param string $circleUniqueId
     */
    public function onCircleDestruction($circleUniqueId)
    {

        #deletes room
        #In matrix, a room is only deleted when the last user leave. So, the only way
        #to enforce room closing/deletion is to kick everybody from the room.
        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);

                if ($roomCircle) {
                    $circle = $this->circleRequest->getCircle($circleUniqueId);
                    $ownerUid = $circle->getOwner()->getUserId();
                    $api = $this->getMatrixApiForUser($ownerUid);

                    $mxRoomId = $roomCircle->getMxRoomId();
                    $members = $api->members($mxRoomId);
                    if ($members['status'] == 200) {
                        foreach ($members['data']['chunk'] as $mxMemberEvent) {
                            $user = $mxMemberEvent['sender'];
                            if ($this->matrixUIDtoCloudUID($user) != $ownerUid) {
                                $api->kick($mxRoomId, $mxMemberEvent['sender'], 'This group is being closed.');
                            }
                        }
                        $api->leave($mxRoomId);
                    }
                    $this->roomCircleMapper->delete($roomCircle);
                }
            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnDestruction", [
                    'circleUniqueId' => $circleUniqueId,
                    'sessionUid' => $currentUid
                ]);
            }
        }
    }

    /**
     * @param Circle $circle
     * @param Member $member
     */
    public function onMemberNew($circle, $member)
    {

        #fired when:
        # * owner adds other user in a public circle
        # * user requests entrance in a public circle (no confirmation needed)
        # * user accepts invitation in a closed circle
        # * owner approves entrance request

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/join/!aTlfmrXFfwGwkaHKNF:localhost

        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $owner = $circle->getOwner();
                $ownerApi = $this->getMatrixApiForUser($owner->getUserId());
                $memberApi = $this->getMatrixApiForUser($member->getUserId());
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

                if ($ownerApi and $memberApi and $roomCircle) {
                    $result = $ownerApi->invite($roomCircle->getMxRoomId(), $memberApi->getUserId());

                    if ($result['status'] == 200) {
                        $result2 = $memberApi->join($roomCircle->getMxRoomId());
                    }
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnMemberNew", [
                    'circleUniqueId' => $circle->getSingleId(),
                    'memberUid' => $member->getUserId()
                ]);
            }
        }
    }

    /**
     * @param Circle $circle
     * @param Member $member
     */
    public function onMemberInvited($circle, $member)
    {

        #fired when
        # * owner adds user in a closed circle

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/rooms/!aTlfmrXFfwGwkaHKNF:localhost/invite

        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $owner = $circle->getOwner();
                $ownerApi = $this->getMatrixApiForUser($owner->getUserId());
                $memberApi = $this->getMatrixApiForUser($member->getUserId());
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

                if ($ownerApi and $memberApi and $roomCircle) {
                    $result = $ownerApi->invite($roomCircle->getMxRoomId(), $memberApi->getUserId());
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnMemberInvited", [
                    'circleUniqueId' => $circle->getSingleId(),
                    'memberUid' => $member->getUserId()
                ]);
            }
        }
    }

    /**
     * @param Circle $circle
     * @param Member $member
     */
    public function onMemberLeaving($circle, $member)
    {

        #fired when
        # * user rejects invitation to enter closed circle
        # * user gives up entering a closed circle (leaves before invitation response)
        # * owner disapproves entrance request

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/rooms/!aTlfmrXFfwGwkaHKNF:localhost/leave

        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $owner = $circle->getOwner();
                $ownerApi = $this->getMatrixApiForUser($owner->getUserId());
                $memberApi = $this->getMatrixApiForUser($member->getUserId());
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

                if ($ownerApi and $memberApi and $roomCircle) {
                    switch ($member->getStatus()) {
                        case BaseMember::STATUS_KICKED:
                            $ownerApi->kick($roomCircle->getMxRoomId(), $memberApi->getUserId(), "");
                            break;
                        default:
                            $memberApi->leave($roomCircle->getMxRoomId());
                    }
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnMemberLeaving", [
                    'circleUniqueId' => $circle->getSingleId(),
                    'memberUid' => $member->getUserId()
                ]);
            }
        }

    }

    /**
     * @param Circle $circle
     * @param Member $member
     */
    public function onMemberLevel($circle, $member, $newUserLevel)
    {

        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $owner = $circle->getOwner();
                $ownerApi = $this->getMatrixApiForUser($owner->getUserId());
                $memberApi = $this->getMatrixApiForUser($member->getUserId());
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

                if ($ownerApi and $memberApi and $roomCircle) {
                    $newPowerLevels = $ownerApi->getPowerLevels($roomCircle->getMxRoomId());

                    $newPowerLevels['users'][$memberApi->getUserId()] = 0;
                    switch ($newUserLevel) {
                        case BaseMember::LEVEL_MODERATOR:
                            $newPowerLevels['users'][$memberApi->getUserId()] = 50;
                            break;
                        case BaseMember::LEVEL_ADMIN:
                            $newPowerLevels['users'][$memberApi->getUserId()] = 99;
                            break;
                        case BaseMember::LEVEL_OWNER:
                            $newPowerLevels['users'][$memberApi->getUserId()] = 100;
                            break;
                    }

                    $result2 = $ownerApi->setPowerLevels($roomCircle->getMxRoomId(), $newPowerLevels);
                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnMemberLevel", [
                    'circleUniqueId' => $circle->getSingleId(),
                    'memberUid' => $member->getUserId()
                ]);
            }
        }
    }

    /**
     * @param Circle $circle
     */
    public function onSettingsChange($circle, $newName = null, $newDescription = null)
    {
        if (!MatrixListener::$AS_TRANSACTION) {

            try {
                $owner = $circle->getOwner();
                $api = $this->getMatrixApiForUser($owner->getUserId());
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

                if ($api and $roomCircle) {
                    $mxRoomId = $roomCircle->getMxRoomId();

                    if ($newName !== null) {
                        $api->setName($mxRoomId, $newName);
                    }

                    if ($newDescription !== null) {
                        $api->setTopic($mxRoomId, $newDescription);
                    }

                    //Change other circle settings

                }

            } catch (\MatrixOrg_Exception $e) {
                //Try again later
                $this->jobList->add("OCA\MatrixBridge\BackgroundJobs\CircleOnSettingsChange", [
                    'circleUniqueId' => $circle->getSingleId()
                ]);
            }
        }
    }

    /**************************************************************************
     *                             CALENDAR METHODS                           *
     **************************************************************************/

    #iCalendar reference: http://sabre.io/vobject/icalendar/

    public function onCalendarChange($calendarId, $calendarData, $shares, $propertyMutations)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                if ($propertyMutations["{DAV:}displayname"]) {
                    $message = <<<EOM
_Um calendário, compartilhado com esta sala, mudou de nome. Novo nome: *{$propertyMutations["{DAV:}displayname"]}*_
EOM;

                    $formattedBody = <<<EOB
<em>Um calendário, compartilhado com esta sala, mudou de nome. Novo nome: <strong>{$propertyMutations["{DAV:}displayname"]}</strong></em>
EOB;

                    //Sends message to room
                    $format = "org.matrix.custom.html";
                    $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                }
            }
        }
    }

    public function onCalendarDeletion($calendarId, $calendarData, $shares)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                if ($calendarData["{DAV:}displayname"]) {
                    $message = <<<EOM
_O calendário *{$calendarData["{DAV:}displayname"]}* foi excluido com todos os eventos e tarefas nele contidos._
EOM;

                    $formattedBody = <<<EOB
<em>O calendário <strong>{$calendarData["{DAV:}displayname"]}</strong> foi excluido com todos os eventos e tarefas nele contidos.</em>
EOB;

                    //Sends message to room
                    $format = "org.matrix.custom.html";
                    $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                }
            }
        }
    }

    public function onCalendarRestore($calendarId, $calendarData, $shares)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                if ($calendarData["{DAV:}displayname"]) {
                    $message = <<<EOM
_O calendário *{$calendarData["{DAV:}displayname"]}* foi restaurado, com todos os eventos e tarefas nele contidos._
EOM;

                    $formattedBody = <<<EOB
<em>O calendário <strong>{$calendarData["{DAV:}displayname"]}</strong> foi restaurado, com todos os eventos e tarefas nele contidos.</em>
EOB;

                    //Sends message to room
                    $format = "org.matrix.custom.html";
                    $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                }
            }
        }
    }

    public function onCalendarObjectCreation($calendarId, $calendarData, $shares, $objectData)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                $vObject = Reader::read($objectData['calendardata']);
                if ($vObject->VEVENT) {
                    $now = new \DateTime();
                    switch ($vObject->VEVENT->DTSTART->getValueType()) {
                        case 'DATE':
                            $dateFormat = ($vObject->VEVENT->DTSTART->getDateTime()->format('Y') == $now->format('Y'))
                                ? 'd/m \(\d\i\a\ \t\o\d\o\)'
                                : 'd/m/Y \(\d\i\a \t\o\d\o\)';
                            break;
                        case 'DATE-TIME':
                            $dateFormat = ($vObject->VEVENT->DTSTART->getDateTime()->format('Y') == $now->format('Y'))
                                ? 'd/m à\s H:i'
                                : 'd/m/Y à\s H:i';
                            break;
                    }
                    $message = <<<EOM
🗓 _Novo evento na agenda **{$calendarData['{DAV:}displayname']}**_:
**O que?** {$vObject->VEVENT->SUMMARY}
**Quando?** {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}
{$extra}
EOM;
// _Fim: *{$vObject->VEVENT->DTEND->getDateTime()->format('d/m/Y à\s H:i')}*_

                    $formattedBody = <<<EOB
🗓 <em>Novo evento na agenda <strong>{$calendarData['{DAV:}displayname']}</strong>:</em><br />
<strong>O que?</strong> {$vObject->VEVENT->SUMMARY}<br />
<strong>Quando?</strong> {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}<br />
{$extra}
EOB;
                } else if ($vObject->VTODO) {

                    $message = <<<EOM
_Uma nova tarefa foi criada: *{$vObject->VTODO->SUMMARY}*_
EOM;

                    $formattedBody = <<<EOB
<em>Uma nova tarefa foi criada: <strong>{$vObject->VTODO->SUMMARY}</strong></em>
EOB;
                }

                //Sends message to room

                $format = "org.matrix.custom.html";

                $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
            }
        }
    }

    public function onCalendarObjectUpdate($calendarId, $calendarData, $shares, $objectData)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                $vObject = Reader::read($objectData['calendardata']);
                if ($vObject->VEVENT) {
                    $now = new \DateTime();
                    $dateFormat = ($vObject->VEVENT->DTSTART->getDateTime()->format('Y') == $now->format('Y'))
                        ? 'd/m à\s H:i'
                        : 'd/m/Y à\s H:i';
                    $message = <<<EOM
🗓 _Evento alterado na agenda **{$calendarData['{DAV:}displayname']}**:_
_**O que?** {$vObject->VEVENT->SUMMARY}_
_**Quando?** {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}_
EOM;
//_Fim: *{$vObject->VEVENT->DTEND->getDateTime()->format('d/m/Y à\s H:i')}*_

                    $formattedBody = <<<EOB
🗓 <em>Evento alterado na agenda <strong>{$calendarData['{DAV:}displayname']}</strong>:</em><br />
<em><strong>O que?</strong> {$vObject->VEVENT->SUMMARY}</em><br />
<em><strong>Quando?</strong> {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}</em>
EOB;
                } else if ($vObject->VTODO) {
                    $message = <<<EOM
_A tarefa *{$vObject->VTODO->SUMMARY}* foi alterada ou recebeu um novo comentário._
EOM;

                    $formattedBody = <<<EOB
<em>A tarefa <strong>{$vObject->VTODO->SUMMARY}</strong> foi alterada ou recebeu um novo comentário.</em>
EOB;
                }


                //Sends message to room

                $format = "org.matrix.custom.html";

                $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
            }
        }
    }

    public function onCalendarObjectDeletion($calendarId, $calendarData, $shares, $objectData)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                $vObject = Reader::read($objectData['calendardata']);

                if ($vObject->VEVENT) {
                    $message = <<<EOM
🗓 _O evento *{$vObject->VEVENT->SUMMARY}* foi cancelado._
EOM;

                    $formattedBody = <<<EOB
🗓 <em>O evento <strong>{$vObject->VEVENT->SUMMARY}</strong> foi cancelado.</em>
EOB;
                } else if ($vObject->VTODO) {
                    $message = <<<EOM
A tarefa *{$vObject->VTODO->SUMMARY}* foi cancelada.
EOM;

                    $formattedBody = <<<EOB
A tarefa <strong>{$vObject->VTODO->SUMMARY}</strong> foi cancelada.
EOB;
                }


                //Sends message to room

                $format = "org.matrix.custom.html";

                $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
            }
        }
    }

    public function onCalendarObjectRestore($calendarId, $calendarData, $shares, $objectData)
    {
        foreach ($shares as $share) {
            if (preg_match('/^principals\/circles\/(.*)/', $share["{http://owncloud.org/ns}principal"], $matches)) {
                $circleUniqueId = $matches[1];

                $vObject = Reader::read($objectData['calendardata']);

                if ($vObject->VEVENT) {
                    $now = new \DateTime();
                    $dateFormat = ($vObject->VEVENT->DTSTART->getDateTime()->format('Y') == $now->format('Y'))
                        ? 'd/m à\s H:i'
                        : 'd/m/Y à\s H:i';
                    $message = <<<EOM
🗓 _O evento *{$vObject->VEVENT->SUMMARY}* foi restaurado (reagendado)._
_**Quando?** {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}_

EOM;

                    $formattedBody = <<<EOB
🗓 <em>O evento <strong>{$vObject->VEVENT->SUMMARY}</strong> foi  restaurado (reagendado).</em><br />
<em><strong>Quando?</strong> {$vObject->VEVENT->DTSTART->getDateTime()->format($dateFormat)}</em>
EOB;
                } else if ($vObject->VTODO) {
                    $message = <<<EOM
A tarefa *{$vObject->VTODO->SUMMARY}* foi restaurada (reagendada).
EOM;

                    $formattedBody = <<<EOB
A tarefa <strong>{$vObject->VTODO->SUMMARY}</strong> foi restaurada (reagendada).
EOB;
                }


                //Sends message to room

                $format = "org.matrix.custom.html";

                $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
            }
        }
    }

    public function onCalendarUpdateShares($calendarId, $calendarData, $shares, $add, $remove)
    {
        $operations = [$add, $remove];
        foreach ($operations as $operation) {
            foreach ($operation as $share) {
                if (preg_match('/^principal:principals\/circles\/(.*)/', $share["href"], $matches)) {
                    $circleUniqueId = $matches[1];
                    $readOnlyMsg = $share['readOnly'] ? 'sim' : 'não';

                    $message = <<<EOM
🗓 Atualização de um calendário: *{$calendarData['uri']}*
Somente leitura: *{$readOnlyMsg}*
EOM;

                    $formattedBody = <<<EOB
🗓 Atualização de um calendário: <strong>{$calendarData['uri']}</strong><br/>
Somente leitura: <strong>{$readOnlyMsg}</strong>
EOB;

                    //Sends message to room

                    $format = "org.matrix.custom.html";

                    $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                }
            }
        }
    }

    /**
     * Translates a matrix UID to a Nextcloud UID
     * @param $matrix_user_id
     */
    private function matrixUIDtoCloudUID($matrix_user_id)
    {
        //TODO treat domain
        if (preg_match('/^@([^:]*):.*/', $matrix_user_id, $matches)) {
            return $matches[1];
        }
        return true;
    }

    private function getMatrixApiForGroupRoomsOwner()
    {
        $uid = $this->helper->getGroupRoomsOwnerName();

        $api = $this->getMatrixApiForUser($uid);
        if (!$api) {
            $api = ApiFactory::createApi($this->helper->getHomeserverUrl(), $uid, $this->helper->getGroupRoomsOwnerPassword());
        }
        return $api;
    }

    private function getMatrixApiForUser($uid)
    {
        if (isset($this->matrixApis[$uid]) && $this->matrixApis[$uid]) {
            return $this->matrixApis[$uid];
        }

        try {
            $homeserverUrl = $this->helper->getHomeserverUrl();
            $matrixUser = $this->userMapper->findByUidAndHomeServer($uid, $homeserverUrl);

            if ($matrixUser) {
                $matrixToken = $matrixUser->getMatrixTokenDecrypted();
                if ($matrixToken) {
                    $this->matrixApis[$uid] = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixUser->getMatrixTokenDecrypted());
                    $this->matrixApis[$uid]->setUserId($matrixUser->getMatrixUsername());
                    return $this->matrixApis[$uid];
                }
            }

        } catch (DoesNotExistException $exp) {

        }

        return null;
    }

    public function sendMessageToRoom($circleUniqueId, $message, $format = "", $formattedBody = "")
    {
        $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
        $res = null;
        try {
            $userId = $this->userSession->getUser()->getUID();
            if ($roomCircle) {
                $api = $this->getMatrixApiForUser($userId);
                if ($api) {
                    $res = $api->sendMessage($roomCircle->getMxRoomId(), "$message", $format, $formattedBody);
                }
            }
        } catch (\Exception $e) {

        }
        return $res && $res['status'] == 200;
    }

    public function alertBotOnUserAdd($uid,$gid)
    {
        if ($uid == $this->BOT_UID || $gid == $this->BOT_UID) {
            return;
        }
        try {
            $homeserverUrl = $this->helper->getHomeserverUrl();
            $matrixUser = $this->userMapper->findByUidAndHomeServer($uid, $homeserverUrl);

            if ($matrixUser) {
                $matrixUsername = $matrixUser->getMatrixUsername();
                if ($matrixUsername) {
                    $this->ensureBotUserAndGroupCreated();
                    $botGroupRooms = $this->groupRoomMapper->findAllByGid($this->BOT_UID);

                    if (count($botGroupRooms)) {
                        $api = $this->getMatrixApiForGroupRoomsOwner();
                        $message1_body = 'op: novo usuario no grupo';
                        $message2_body = 'usr: '.$matrixUser->getMatrixUsername();
                        $message3_body = 'grupo: '.$gid;

                        foreach ($botGroupRooms as $botGroupRoom) {
                            $res1 = $api->sendMessage($botGroupRoom->getRoomId(),$message1_body);
                            $res2 = $api->sendMessage($botGroupRoom->getRoomId(),$message2_body);
                            $res3 = $api->sendMessage($botGroupRoom->getRoomId(),$message3_body);
                        }
                    }
                }
            }

        } catch (DoesNotExistException $exp) {

        }
    }

    private function ensureBotUserAndGroupCreated() {
        $group = $this->groupManager->get($this->BOT_UID);
        if (!$group) {
            $group = $this->groupManager->createGroup($this->BOT_UID);
        }

        $userBot = $this->userManager->get($this->BOT_UID);
        if (!$userBot) {
            $userBot = $this->userManager->createUser($this->BOT_UID,"mypassword1!");
        }
        $group->addUser($userBot);
    }

}
