<?php
namespace OCA\MatrixBridge\Service;

/**
 * Listen to nextcloud events (hooks)
 */
class NCListener {

  // key = room_id ; value = folder path
	private $rooms;
	
	public function __construct() {
	  $this->rooms = array();
	}
	
	public function getFolderOfRoom($roomId) {
	
	}
	
	public function getFolderNameOfRoom($roomId) {
	
	}
	
	public function getUserFromMatrixUserId($matrixUserId) {
	
	}
	
}
