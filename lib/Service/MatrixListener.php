<?php

namespace OCA\MatrixBridge\Service;

use MatrixOrg_API;
use OC\Files\Node\File;
use OC\Files\Node\Folder;
use OCA\Circles\Db\CircleRequest;
use OCA\Circles\Exceptions\CircleAlreadyExistsException;
use OCA\Circles\Exceptions\CircleNotFoundException;
use OCA\Circles\Model\BaseMember;
use OCA\Circles\Model\Circle;
use OCA\Circles\Service\CircleService;
use OCA\Circles\Service\FederatedUserService;
use OCA\Circles\Service\MemberService;
use OCA\MatrixBridge\Db\MatrixFileCache;
use OCA\MatrixBridge\Db\MatrixFileCacheMapper;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Helper;
use OCA\MatrixBridge\UserHooks;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IAvatarManager;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\IUserSession;
use OCP\Share\IManager;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Handles the messages received from Matrix Server (Application Service API)
 * https://spec.matrix.org/latest/application-service-api/
 */
class MatrixListener
{

    const ALLOWED_ROOM_DOMAINS = array(
        'localhost',
        'chat.localhost',
        'synapse.localhost',
        'chat.rios.org.br',
        'matrix.eita.org.br'
    );

    //Flag if received displayname from matrix to avoid hook loop
    public static $receivedDisplayName = false;

    private $userManager;
    private $eventBody;
    private $matrixUtils;
    private $avatarManager;
    private $userMapper;
    private $userSession;
    private $roomCircleMapper;
    private $logger;
    private $helper;
    private $mxFileCacheMapper;
    private $shareManager;

    private $circleService;
    private $memberService;
    private $federatedUserService;
    private $circleRequest;

    public static $AS_TRANSACTION = false;
    private $apis = array();

    /**
     * $storage: \OCP\Files\IRootFolder or \OCP\Files\Folder
     * $userSession: IUserSession
     */
    public function __construct(IUserManager $userManager,
                                MatrixUtils $utils,
                                IAvatarManager $avatarManager,
                                MatrixUserMapper $mapper,
                                IUserSession $userSession,
                                RoomCircleMapper $roomCircleMapper,
                                ILogger $logger,
                                MatrixFileCacheMapper $mxFileCacheMapper,
                                IManager $shareManager,
                                CircleService $circleService,
                                MemberService $memberService,
                                CircleRequest $circleRequest,
                                FederatedUserService $federatedUserService
    )
    {
        $this->userManager = $userManager;
        $this->eventBody = file_get_contents("php://input");
        $this->matrixUtils = $utils;
        $this->avatarManager = $avatarManager;
        $this->userMapper = $mapper;
        $this->userSession = $userSession;
        $this->roomCircleMapper = $roomCircleMapper;
        $this->logger = $logger;
        $this->helper = new Helper(\OC::$server->getConfig());
        $this->mxFileCacheMapper = $mxFileCacheMapper;
        $this->shareManager = $shareManager;
        $this->circleService = $circleService;
        $this->circleRequest = $circleRequest;
        $this->memberService = $memberService;
        $this->federatedUserService = $federatedUserService;
    }

    public function usersHandler()
    {
        //$this->matrixUtils->log('/users');

        //$this->matrixUtils->log($this->eventBody);
        return true;
    }

    public function roomsHandler()
    {
        //$this->matrixUtils->log('/rooms');

        //$this->matrixUtils->log($this->eventBody);
        return true;
    }

    public function transactionsHandler()
    {
        //$this->matrixUtils->log('/transactions');

        $message = json_decode($this->eventBody);
        $events = $message->events;
        $ret = true;
        //Used to test SyncRoomsToCircles
        //return true;

        self::$AS_TRANSACTION = true;

        try {
            foreach ($events as $event) {

                if ($this->isEventAllowed($event)) {

                    //$this->matrixUtils->log($event->type);
                    $this->matrixUtils->log($this->eventBody);
                    switch ($event->type) {
                        case "m.room.message":
                            $ret = $this->onRoomMessage($event);
                            break;
                        case "m.room.create":
                            $ret = $this->onRoomCreate($event);
                            break;
                        case "m.room.member":
                            $ret = $this->onRoomMember($event);
                            break;
                        case "m.room.power_levels":
                            $ret = $this->onRoomPowerLevels($event);
                            break;
                        case "m.room.canonical_alias":
                            $ret = $this->onRoomCanonicalAlias($event);
                            break;
                        case "m.room.join_rules":
                            $ret = $this->onRoomJoinRules($event);
                            break;
                        case "m.room.history_visibility":
                            $ret = $this->onRoomHistoryVisibility($event);
                            break;
                        case "m.room.aliases":
                            $ret = $this->onRoomAliases($event);
                            break;
                        case "m.room.name":
                            $ret = $this->onRoomName($event);
                            break;
                        case "m.presence":
                            $ret = $this->onPresenceStatusChanged($event);
                            break;
                        case "m.room.guest_access":
                            $ret = $this->onGuestAccess($event);
                            break;
                        case "m.room.redaction":
                            $ret = $this->onRedaction($event);
                            break;
                        case "m.room.topic":
                            $ret = $this->onTopic($event);
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (\Exception $err) {
            $this->logger->error("Error handling AS request: event=" . json_encode($event) . " exception=" . $err);
            $this->matrixUtils->log("\nexception: " . $err->getMessage() . " | file: " . $err->getFile() . " | line: " . $err->getLine() . "\n" . "matrix_message: " . json_encode($event) . "\n" . $err->getTraceAsString(),"matrix_listener_errors");
        } catch (\Error $err) {
            $this->matrixUtils->log("\nerror: " . $err->getMessage() . " | file: " . $err->getFile() . " | line: " . $err->getLine() . "\n" . "matrix_message: " . json_encode($event) . "\n" . $err->getTraceAsString(),"matrix_listener_errors");
        }

        self::$AS_TRANSACTION = false;
        return true;
    }

    /**
     * Called when user enters a room, leaves a room, is invited or changes his/her displayname or avatar
     */
    public function onRoomMember($e)
    {
        /*
        {
            "age": 33854,
            "content": {
                "avatar_url": null,
                "displayname": null,
                "membership": "join"
            },
            "event_id": "$15042656028ZuHdc:localhost",
            "membership": "join",
            "origin_server_ts": 1504265602296,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "@vini3:localhost",
            "type": "m.room.member",
            "unsigned": {
                "age": 33854
            },
            "user_id": "@vini3:localhost"
        }
        */
        //$this->matrixUtils->log($e);

        try {
            $user = $this->setMxSessionUser($e->user_id);
            $circle = $this->getCircle($e->room_id, !in_array($e->content->membership,['leave','ban']));
            if (!$circle) {
                return true;
            }
            $circleMember = null;
            $uid = $this->matrixUIDtoCloudUID($e->user_id);

            $members = $circle->getMembers();
            foreach ($members as $member) {
                if ($member->getUserId() == $uid) {
                    $circleMember = $member;
                    break;
                }
            }

            //$circleMember = $circle->getInitiator();

            switch ($e->content->membership) {
                case 'join': //Concedes permission from the user to the folder, User change name or avatar_url
                    if ($user) {
                        // add user to circle if not already there
                        if (!$circleMember || $circleMember->getLevel() == BaseMember::LEVEL_NONE) {
                            $this->circleService->circleJoin($circle->getSingleId());
                        }
                        $this->fixCircleName($circle);

                        //Change displayName if needed
                        if ($e->content->displayname != $user->getDisplayName()) {
                            self::$receivedDisplayName = true;
                            $user->setDisplayName($e->content->displayname);
                        }
                        //Change avatar if needed
                        /*
                        if (isset($e->prev_content) && $e->content->avatar_url != $e->prev_content->avatar_url) {
                            try {
                                $image = new Image();
                                $image->loadFromFileHandle(fopen(MatrixOrg_API::downloadUrlHs($e->content->avatar_url, $this->helper->getHomeserverUrl()), 'rb'));
                                $image->centerCrop();
                                $avatar = $this->avatarManager->getAvatar($user->getUID());
                                $currentImage = $avatar->get();

                                #if images are not similar, change current avatar
                                if ($this->matrixUtils->compareImages($currentImage, $image) > 11) {
                                    $avatar->set($image);
                                }
                            } catch (\Exception $exp) {
                                \OC::$server->getLogger()->error("Error in changing avatar through AS: " . $exp->getMessage(), array('app' => 'matrixbridge'));
                            }
                        }
                        */
                    }

                    break;
                case 'leave':
                case 'ban':
                    if (!$circleMember || $circleMember->getLevel() == BaseMember::LEVEL_NONE) {
                        return;
                    }
                    if ($e->user_id === $e->state_key) { //user is leaving
                        if ($user) {
                            //Current user to exit circle is owner - must find a new owner or destroy circle
                            if ($circleMember->getLevel() == BaseMember::LEVEL_OWNER) {
                                $this->setSessionUser($user->getUID());
                                $members = $circle->getMembers();
                                if (count($members) == 1) {
                                    $this->circleService->destroy($circle->getSingleId());
                                } else {
                                    //Find a moderator, preferably, to convert in owner
                                    for ($i = 0, $iMax = count($members); $i < $iMax; $i++) {
                                        $member = $members[$i];
                                        if ($member->getLevel() == BaseMember::LEVEL_ADMIN || $i == $iMax - 1) {
                                            $this->helper->memberLevel($this->memberService, $circle->getSingleId(), $member->getId(), BaseMember::LEVEL_OWNER);
                                            break;
                                        }
                                    }
                                    $this->fixCircleName($circle, array('exclude_member' => $this->userSession->getUser()->getUID()));
                                    //$this->setSessionUser($user->getUID());
                                    $this->circleService->circleLeave($circle->getSingleId());
                                }
                            } else {
                                if ($circleMember->getLevel() != BaseMember::LEVEL_NONE) {
                                    $this->fixCircleName($circle, array('exclude_member' => $this->userSession->getUser()->getUID()));
                                }
                                $this->circleService->circleLeave($circle->getSingleId());
                            }
                        }
                    } else { //user is being kicked or banned.
                        // ban: there is not in NC. TODO: handle bans in NC
                        $targetUser = $this->matrixUIDtoUser($e->state_key);
			if ($targetUser) {
                        $members = $circle->getMembers();
                        foreach ($members as $member) {
                            if ($member->getUserId() == $targetUser->getUID()) {
                                $this->memberService->removeMember($member->getId());
                                break;
                            }
                        }
			}
                    }
                    break;
                case 'invite':
                    $user = $this->matrixUIDtoUser($e->user_id);
                    if ($user) {
                        // User asks to join circle if not already there
                        $targetUser = $this->matrixUIDtoUser($e->state_key);
                        if ($targetUser) {
                            try {
                                #use circle owner to invite user, as in matrix every member can invite...
//                                $owner = $circle->getOwner();
//                                $this->setSessionUser($owner->getUserId());
//                                $circle = $this->getCircle($e->room_id);
//                                $this->matrixUtils->log("circle_id: " . $circle->getSingleId() . "  user_id: " . $targetUser->getUID() . "   current_user:" . $e->user_id);
                                $this->helper->memberAdd($this->federatedUserService, $this->memberService, $circle->getSingleId(), $targetUser->getUID());
                            } catch (\Exception $exp) {
                                $this->matrixUtils->log($exp->getMessage());
                                //Already invited / member
                            }
                        }
                    }
                    break;
            }
        } catch (\Exception $exp) {
            $this->matrixUtils->log($exp->getMessage());
        }
        return true;
    }

    /**
     * Called when user changes his/her status or changes his/her displayname or avatar
     */
    public function onPresenceStatusChanged($e)
    {
        //$this->matrixUtils->log($e);
        return true;
    }

    /**
     * Called when a room is created
     */
    public function onRoomCreate($e)
    {
        /*
        {
            "age": 365,
            "content": {
                "creator": "@vini3:localhost"
            },
            "event_id": "$150426678314mPuQO:localhost",
            "origin_server_ts": 1504266783786,
            "room_id": "!BLBaGuaHDKeNfapZMR:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.create",
            "unsigned": {
                "age": 365
            },
            "user_id": "@vini3:localhost"
        }
        */

        $this->setMxSessionUser($e->content->creator);
        $mxRoomId = $e->room_id;

        //MREF
        $circle = $this->getCircle($mxRoomId);

        return true;
    }

    /**
     * Called when room power levels are set
     *
     * Options
     * * m.room.power_levels    -> who can change the power levels of the room/folder
     * * m.room.avatar          -> who can change the folder/room avatar
     * * m.room.canonical_alias -> who can change the name of the folder/room
     * * m.room.name            -> ?
     * * m.room.invite          -> who can invite other to room, or share folder to others
     * * m.room.kick            -> who can revoke permissions from other users
     * * m.room.redact          -> who can add files
     * * m.room.state_default   -> default permissions of new users (of the folder)
     */
    public function onRoomPowerLevels($e)
    {
        /* Esrutura do evento recebido:
        {
            "age": 33377,
            "content": {
                "ban": 50,
                "events": {
                    "m.room.avatar": 50,
                    "m.room.canonical_alias": 50,
                    "m.room.history_visibility": 100,
                    "m.room.name": 50,
                    "m.room.power_levels": 100
                },
                "events_default": 0,
                "invite": 0,
                "kick": 50,
                "redact": 50,
                "state_default": 50,
                "users": {
                    "@vini3:localhost": 100
                },
                "users_default": 0
            },
            "event_id": "$15042656029PdsAE:localhost",
            "origin_server_ts": 1504265602773,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.power_levels",
            "unsigned": {
                "age": 33377
            },
            "user_id": "@vini3:localhost"
        }
        */

        $this->setMxSessionUser($e->user_id);
        $circle = $this->getCircle($e->room_id);

        #make the owner of the circle the current user

        /**
         * FIXME: no circles, o usuário não pode mudar o nivel do outro para um
         * nivel igual ao próprio, somente menor.
         * No matrix, pode mudar para um nivel igual, mas não pode diminuir o
         * nivel de outro usuario que tem o mesmo nivel.
         *
         * Como está implementado: transforma o owner do ciculo no current_user e
         * faz qualquer transformação necessária.
         *
         * Como deve ser feito: verificar o nivel de circle do usuario atual e do
         * usuario cuja permissão deve mudada, e a nova permissao. Caso seja uma mudança
         * para um power_level igual à do usuario atual, e usuário atual tem permissão para mudar,
         * deve-se usar o owner para fazer esta mudança. Caso contrário, usar o usuario atual
         * para fazer a mudança.
         **/

        $owner = $circle->getOwner();
        $this->setSessionUser($owner->getUserId());

        //$this->userSession->setUser($this->userManager->get($owner->getUserId()));

        #get the circle again, because the previous circle had wrong viewer set
        $circle = $this->getCircle($e->room_id);


        if (property_exists($e, 'prev_content')) {
            $diff = array_diff_assoc(get_object_vars($e->content->users), get_object_vars($e->prev_content->users));
        } else {
            $diff = get_object_vars($e->content->users);
        }

        foreach ($diff as $mxUserId => $newLevel) {
            $uid = $this->matrixUIDtoCloudUID($mxUserId);

            if ($uid != $owner->getUserId()) {
                $newMemberLevel = BaseMember::LEVEL_MEMBER;
                if ($newLevel == 100) {
                    $newMemberLevel = BaseMember::LEVEL_ADMIN;
                } else if ($newLevel >= 50) {
                    $newMemberLevel = BaseMember::LEVEL_MODERATOR;
                }

                $members = $circle->getMembers();

                foreach ($members as $member) {
                    if ($member->getUserId() == $uid) {
                        $this->helper->memberLevel($this->memberService, $circle->getSingleId(), $member->getId(), $newMemberLevel);
                        break;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Changes name of the folder
     */
    public function onRoomCanonicalAlias($e)
    {
        return true;
    }

    /**
     * Join rules for the room/circle
     * @param $e
     * @return bool
     */
    public function onRoomJoinRules($e): bool
    {
        /* Esrutura do evento recebido:
        {
            "age": 32991,
            "content": {
                "join_rule": "invite"
            },
            "event_id": "$150426560310kbACy:localhost",
            "origin_server_ts": 1504265603159,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.join_rules",
            "unsigned": {
                "age": 32991
            },
            "user_id": "@vini3:localhost"
        }
        */

        try {
            $this->setMxSessionUser($e->user_id);
            $circle = $this->getCircle($e->room_id);
            $config = $circle->getConfig();
            //In matrix
            switch ($e->content->join_rule) {
                case "public":
                    $config = ($config | Circle::CFG_VISIBLE | Circle::CFG_FRIEND | Circle::CFG_INVITE | Circle::CFG_OPEN);
                    break;
                case "invite":
                    $config = ($config | Circle::CFG_INVITE | Circle::CFG_FRIEND) & ~Circle::CFG_VISIBLE;
                    break;
                case "knock":
                    $config = ($config | Circle::CFG_INVITE | Circle::CFG_FRIEND | Circle::CFG_REQUEST) & ~Circle::CFG_VISIBLE;
                    break;
                default:
                    return true;
            }
            #$config |= Circle::CFG_APP;
            $this->circleService->updateConfig($circle->getSingleId(), $config);
        } catch (\Exception $exp) {
            $this->matrixUtils->log($exp->getMessage());
        }

        return true;
    }

    /**
     * Makes room visibility public or private
     * TBD
     */
    public function onRoomHistoryVisibility($e)
    {
        /*
        {
            "age": 32555,
            "content": {
                "history_visibility": "shared"
            },
            "event_id": "$150426560311ziunX:localhost",
            "origin_server_ts": 1504265603595,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.history_visibility",
            "unsigned": {
                "age": 32555
            },
            "user_id": "@vini3:localhost"
        }
        */

        // TODO: What will happen when history visibility is changed? Will older files be uploaded/removed from the default directory?
        //$user = $this->setSessionUser($e->user_id);

        return true;
    }

    /**
     * TBD
     */
    public function onRoomAliases($e)
    {
        return true;
    }


    /**
     * Handles incoming messages (type m.text or m.file)
     */
    public function onRoomMessage($e)
    {
        /**
         *
         * {
         * "age": 793,
         * "content": {
         * "body": "91c9e4f5-c3f7-4dc7-92bd-87f6db73a69.jpg",
         * "info": {
         * "h": 756,
         * "mimetype": "image\/jpeg",
         * "size": 102753,
         * "thumbnail_info": {
         * "h": 600,
         * "mimetype": "image\/jpeg",
         * "size": 137852,
         * "w": 476
         * },
         * "thumbnail_url": "mxc:\/\/localhost\/lbmlyxpIYbaApCjdtHYDFTtn",
         * "w": 600
         * },
         * "msgtype": "m.image",
         * "url": "mxc:\/\/localhost\/EhQMEQHDNsBJxTtxsqoxYGUT"
         * },
         * "event_id": "$15046188440TiZkf:localhost",
         * "origin_server_ts": 1504618844596,
         * "room_id": "!QQTHQoSGEgSgrYrHEy:localhost",
         * "sender": "@vini3:localhost",
         * "type": "m.room.message",
         * "unsigned": {
         * "age": 793
         * },
         * "user_id": "@vini3:localhost"
         * }
         */
        return true;

        #copy file in matrix to the default folder of the circle, if exists
        if (in_array($e->content->msgtype, array('m.image', 'm.file', 'm.video', 'm.audio'))) {

            //$this->matrixUtils->log(json_encode($e,JSON_PRETTY_PRINT));

            $this->logger->debug("MatrixListener evt received: " . json_encode($e));

            $this->logger->debug("MatrixListener: will query filecahes");

            //Search if received file is already in the file system, in other point
            $fileCaches = $this->mxFileCacheMapper->findAllByEventId($e->event_id);

            $this->logger->debug("MatrixListener: queried filecahes count=" . count($fileCaches));


            if (!count($fileCaches)) {
                $roomCircle = $this->roomCircleMapper->findByMxRoomId($e->room_id);
                $uid = $this->matrixUIDtoCloudUID($e->user_id);

                if (!$roomCircle) {
                    $api = $this->getMatrixApiForUser($uid);

                    $state = $api->getState($e->room_id);

                    $currentUser = $this->userSession->getUser();

                    $this->setMxSessionUser($state->creatorUid);

                    $circle = $this->getCircle($e->room_id);
                    $this->circleService->updateName($circle->getSingleId(), $state->name);

                    foreach ($state->state_events as $event) {
                        if ($event->type === 'm.room.member') {
                            $this->onRoomMember($event);
                        }
                    }

                    $roomCircle = $this->roomCircleMapper->findByMxRoomId($e->room_id);

                    if ($currentUser) {
                        $this->setSessionUser($currentUser->getUID());
                    }
                    //$this->userSession->setUser($currentUser);
                }

                $fileId = null;
                if (property_exists($e->content, 'm.cloud_folder')) {
                    $cloud_folder = $e->content->{'m.cloud_folder'};
                    list($fileId, $folder_name) = explode(':', $cloud_folder);
                    $fileId = (int)$fileId;
                }

                //$fileId = $roomCircle->getDefaultFolderFileid();
                $createShare = false;

                if (!$fileId) {
                    $userFolder = \OC::$server->getUserFolder($uid);
                    if ($userFolder) {
                        $fileId = $userFolder->getId();
                        $createShare = true;
                        $this->setMxSessionUser($e->user_id);
                    }
                }

                if ($fileId) {
                    $rootFolder = \OC::$server->query(
                        'RootFolder');
                    $folders = $rootFolder->getById($fileId);
                    if (count($folders)) {
                        /** @var Folder $folder */
                        $folder = $folders[0];
                    } else {
                        //Shared folder with circle
                        $userFolder = \OC::$server->getUserFolder($uid);
                        $folders = $userFolder->getById($fileId);
                        $folder = $folders[0];
                    }

                    if ($folder) {
                        $filename = $e->content->body;

                        $nodeExisted = $folder->nodeExists($filename);

                        $file = $folder->newFile($filename);

                        #download file from matrix
                        $mxFileUrl = MatrixOrg_API::downloadUrlHs($e->content->url, $this->helper->getHomeserverUrl());
                        #TODO treat error cases : file not found, network unreachable, user has no permission to overwrite file / write in folder
                        $file->putContent(file_get_contents($mxFileUrl));

                        $fileCache = null;

                        if ($nodeExisted) {
                            $fileCaches = $this->mxFileCacheMapper->findAllByFileid($file->getId());
                            $fileCache = $fileCaches[0];
                        }

                        if ($fileCache) {
                            #redacts old file
                            $api = $this->getMatrixApiForUser($uid);
                            $res2 = $api->redact($fileCache->getMxRoomId(), $fileCache->getEventId());

                            #updates fileCache
                            $fileCache->setEventId($e->event_id);
                            $fileCache->setMxcUrl($e->content->url);
                            $this->mxFileCacheMapper->update($fileCache);
                        } else {
                            $this->mxFileCacheMapper->createOrUpdate($file->getId(), $e->event_id, $e->content->url, $this->matrixUIDtoCloudUID($e->user_id), $e->room_id, $e->content->body);

                            //Share file
                            if ($createShare) {
                                $share = $this->shareManager->newShare();
                                $share->setNode($file);
                                $share->setPermissions(\OCP\Constants::PERMISSION_ALL &
                                    ~\OCP\Constants::PERMISSION_DELETE &
                                    ~\OCP\Constants::PERMISSION_CREATE);

                                $share->setSharedWith($roomCircle->getCircleUniqueId());
                                $share->setSharedBy($uid);
                                $share->setShareType(\OCP\Share::SHARE_TYPE_CIRCLE);
                                $share = $this->shareManager->createShare($share);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     *
     */
    public function onGuestAccess($e)
    {
        /*
        {
            "age": 32116,
            "content": {
                "guest_access": "can_join"
            },
            "event_id": "$150426560412SvpXY:localhost",
            "origin_server_ts": 1504265604034,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.guest_access",
            "unsigned": {
                "age": 32116
            },
            "user_id": "@vini3:localhost"
        }
        */
        try {
            $this->setMxSessionUser($e->user_id);
            $circle = $this->getCircle($e->room_id);
            $config = $circle->getConfig();
            switch ($e->content->guest_access) {
                case "can_join":
                    $config = $config | Circle::CFG_OPEN;
                    break;
                case "forbidden":
                    $config = $config | ~Circle::CFG_OPEN;
                    break;
                default:
                    return true;
            }
            $this->circleService->updateConfig($circle->getSingleId(), $config);
        } catch (\Exception $exp) {
            $this->matrixUtils->log($exp->getMessage());
        }

        return true;
    }

    /**
     *
     */
    public function onRoomName($e)
    {
        /*
        {
            "age": 31726,
            "content": {
                "name": "C"
            },
            "event_id": "$150426560413EphBa:localhost",
            "origin_server_ts": 1504265604424,
            "room_id": "!yUHseepkpAGxRfbqgb:localhost",
            "sender": "@vini3:localhost",
            "state_key": "",
            "type": "m.room.name",
            "unsigned": {
                "age": 31726
            },
            "user_id": "@vini3:localhost"
        }
        */

        $mxRoomId = $e->room_id;

        $this->setMxSessionUser($e->user_id);
        $circle = $this->getCircle($mxRoomId);

        if (empty(trim($e->content->name))) {
            $this->fixCircleName($circle, array('force' => true));
        } else {
            if ($circle->getName() == $e->content->name) {
                return true;
            }

            $done = false;
            $circleOriginalName = $circleName = $e->content->name;
            $circleExt = 0;

            while (!$done) {
                try {
                    $this->circleService->updateName($circle->getSingleId(), $circleName);
                    $done = true;
                } catch (CircleAlreadyExistsException $exception) {
                    $circleExt++;
                    $circleName = $circleOriginalName . " ($circleExt)";
                    //Will change room name
                    self::$AS_TRANSACTION = false;
                }
            }
            self::$AS_TRANSACTION = true;
        }

        return true;
    }

    public function onRedaction($e)
    {
        /**
         * {
         * "age": 340,
         * "content": {},
         * "event_id": "$150462652010tqRbM:localhost",
         * "origin_server_ts": 1504626520105,
         * "redacts": "$15046221533pGWZe:localhost",
         * "room_id": "!QQTHQoSGEgSgrYrHEy:localhost",
         * "sender": "@vini3:localhost",
         * "type": "m.room.redaction",
         * "unsigned": {
         * "age": 340
         * },
         * "user_id": "@vini3:localhost"
         * }
         */

        $fileCaches = $this->mxFileCacheMapper->findAllByEventId($e->redacts);
        if (count($fileCaches)) {
            /** @var MatrixFileCache $fileCache */
            $fileCache = $fileCaches[0];
            $this->setMxSessionUser($e->user_id);
            $rootFolder = \OC::$server->query('RootFolder');
            $files = $rootFolder->getById($fileCache->getFileid());
            if (count($files)) {
                /** @var File $file */
                $file = $files[0];
                if ($fileCache->getUnshareOnRedact()) {
                    $user = $this->userManager->get($fileCache->getCreatorUid());
                    if ($user) {
                        $this->setSessionUser($user->getUID());
                        //$this->userSession->setUser($user);
                        $shares = $this->shareManager->getSharesByPath($file);
                        foreach ($shares as $share) {
                            $this->shareManager->deleteShare($share);
                        }
                    }
                } else {
                    $file->delete();
                }
            }
            $this->mxFileCacheMapper->delete($fileCache);
        }
        return true;
    }

    public function onTopic($e)
    {
        /**
         * {
         * "age": 368,
         * "content": {
         * "topic": "teste2"
         * },
         * "event_id": "$15046331451WiZZq:localhost",
         * "origin_server_ts": 1504633145635,
         * "prev_content": {
         * "topic": "teste"
         * },
         * "replaces_state": "$15046331340KRQlq:localhost",
         * "room_id": "!QQTHQoSGEgSgrYrHEy:localhost",
         * "sender": "@vini3:localhost",
         * "state_key": "",
         * "type": "m.room.topic",
         * "unsigned": {
         * "age": 368,
         * "prev_content": {
         * "topic": "teste"
         * },
         * "prev_sender": "@vini3:localhost",
         * "replaces_state": "$15046331340KRQlq:localhost"
         * },
         * "user_id": "@vini3:localhost"
         * }
         */

        try {
            $roomCircle = $this->roomCircleMapper->findByMxRoomId($e->room_id);
            if ($roomCircle) {
                $this->setMxSessionUser($e->user_id);
                $this->circleService->updateDescription($roomCircle->getCircleUniqueId(), $e->content->topic);

                $roomCircle->setMxRoomTopic($e->content->topic);
                $this->roomCircleMapper->update($roomCircle);
            }
        } catch (\Exception $exp) {
            $this->matrixUtils->log($exp->getMessage());
        }

        return true;
    }

    /**
     * Translates a matrix UID to a Nextcloud UID
     * @param $matrix_user_id
     */
    private function matrixUIDtoCloudUID($matrix_user_id)
    {
        if (preg_match('/^@([^:]*):' . $this->helper->getIdentifiersDomain() . '/', $matrix_user_id, $matches)) {
            return $matches[1];
        }
        return false;
    }

    /**
     * Translates a matrix UID to a Nextcloud UID
     * @param $matrix_user_id
     */
    private function matrixUIDtoUser($matrix_user_id)
    {
        $cloud_uid = $this->matrixUIDtoCloudUID($matrix_user_id);

        if ($cloud_uid) {
            return $this->userManager->get($cloud_uid);
        }
        return true;
    }


    private function setMxSessionUser($mxUserId)
    {
        $user = $this->matrixUIDtoUser($mxUserId);
        if ($user) {
            $this->userSession->setUser($user);
            $this->federatedUserService->setLocalCurrentUser($user);
            //$this->helper->changeUserIdCirclesService($this->circleService, $user->getUID());
            //$this->helper->changeUserIdMembersService($this->memberService, $user->getUID());
        }
        return $user;
    }

    private function setSessionUser($userId)
    {
        $user = $this->userManager->get($userId);
        if ($user) {
            $this->userSession->setUser($user);
            $this->federatedUserService->setLocalCurrentUser($user);
            //$this->helper->changeUserIdCirclesService($this->circleService, $user->getUID());
            //$this->helper->changeUserIdMembersService($this->memberService, $user->getUID());
        }
        return $user;
    }

    private function isEventAllowed($event)
    {
        preg_match('/:(.*)$/', $event->room_id, $parts);
        return ($parts and $parts[1] and in_array($parts[1], self::ALLOWED_ROOM_DOMAINS));
    }

    /**
     * @param $uid
     * @return MatrixOrg_API
     */
    private function getMatrixApiForUser($uid)
    {
        if (isset($this->apis[$uid]) && $this->apis[$uid]) {
            return $this->apis[$uid];
        }

        try {
            $homeserverUrl = $this->helper->getHomeserverUrl();
            $matrixUser = $this->userMapper->findByUidAndHomeServer($uid, $homeserverUrl);

            if ($matrixUser) {
                $matrixToken = $matrixUser->getMatrixTokenDecrypted();
                if ($matrixToken) {
                    $this->apis[$uid] = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixUser->getMatrixTokenDecrypted());
                    $this->apis[$uid]->setUserId($matrixUser->getMatrixUsername());
                    return $this->apis[$uid];
                }
            }

        } catch (DoesNotExistException $exp) {

        }

        return null;
    }

    /**
     * Changes circle name according to memberships if current room name
     *        is not existent,
     * @param $circle
     * @param $params :   force => false (default) | true
     *                    exclude_member => (member_name)
     */
    private function fixCircleName(Circle $circle, $params = array())
    {

        $must_change = 0;
        //Analyze if it is necessary to change the circle name
        if (array_key_exists('force', $params) and $params['force']) {
            $must_change = 1;
        } else {
            $name = $circle->getName();

            $patterns = array(
                '/^ *$/',
                '/^!.*:[a-z0-9_\.\-]*$/',
                '/^Conversa entre .* e .*$/',
                '/^Sala de .* e mais .* pessoas$/',
                '/^Sala de .* vazia/'
            );

            if ($name) {
                foreach ($patterns as $pattern) {
                    $must_change |= preg_match($pattern, $name);
                }
            }
        }

        if ($must_change) {
            $currentUser = $this->userSession->getUser();
            $owner = $circle->getOwner();
            $this->setSessionUser($owner->getUserId());

            $circle = $this->circleService->getCircle($circle->getSingleId());

            $members = $circle->getMembers();

            $member_names = array();

            foreach ($members as $member) {
                $member_names[$member->getUserId()] = $member->getDisplayName();
            }

            if (array_key_exists('exclude_member', $params) and $params['exclude_member']) {
                unset($member_names[$params['exclude_member']]);
            }

            if (count($member_names) > 2) {
                $owner = $circle->getOwner();
                $circle_name = "Sala de {$owner->getDisplayName()} e mais " . (count($member_names) - 1) . " pessoas";

            } else if (count($member_names) == 2) {
                natcasesort($member_names);
                $i = 0;
                $member_display_names = array();
                foreach ($member_names as $member_name) {
                    $member_display_names[$i++] = $member_name;
                }
                $circle_name = "Conversa entre {$member_display_names[0]} e {$member_display_names[1]}";
            } else if (count($member_names) == 1) {
                reset($member_names);
                $circle_name = "Sala de " . current($member_names) . " vazia";
            }

            if (!empty($circle_name) && $circle_name != $circle->getName()) {
                $name_fixed = false;
                $original_circle_name = $circle_name;
                $n = 0;

                while (!$name_fixed) {
                    try {
                        $this->circleService->updateName($circle->getSingleId(), $circle_name);
                        $name_fixed = true;
                    } catch (CircleAlreadyExistsException $exp) {
                        $n++;
                        $circle_name = $original_circle_name . " ($n)";
                    } catch (\Exception $exp) {
                        $name_fixed = true;
                    }
                }

            }

            if ($currentUser) {
                $this->setSessionUser($currentUser->getUID());
            }
            //$this->userSession->setUser($currentUser);
        }

    }

    /**
     * @param false|string $eventBody
     */
    public function setEventBody($eventBody): void
    {
        $this->eventBody = $eventBody;
    }

    private function getCircle($mxRoomId, $createIfInexistent = true)
    {
        return $this->helper->getCircle($this->circleRequest, $this->circleService, $this->roomCircleMapper, $mxRoomId, null, null, false, $createIfInexistent);
    }

}
