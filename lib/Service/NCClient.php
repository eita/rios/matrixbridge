<?php
namespace OCA\MatrixBridge\Service;
use OCA\MatrixBridge\Db\Room;
use OCA\MatrixBridge\Db\RoomMapper;

/**
 * ACTS upon nextcloud
 */
class NCClient {


	public function __construct() {
	}

	public function getFolderOfRoom($roomId) {
        return null;
	}

	public function getFolderNameOfRoom($roomId) {
        return null;
	}

	public function getUserFromMatrixUserId($matrixUserId) {
        return null;
	}

	public function appendMessageToFolder($fileid) {
		return null;
	}

	public function addFileToFolder($field) {
		return null;
	}

	public function createRoomFolder($external_room_id, $external_creator, $platform) {
        return null;
	}

}
