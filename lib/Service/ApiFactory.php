<?php
namespace OCA\MatrixBridge\Service;


use MatrixOrg_API;
use OCA\MatrixBridge\Db\MatrixUser;
use OCA\MatrixBridge\Db\MatrixUserMapper;

require_once \OC_App::getAppPath('matrixbridge').'/3rdparty/matrixorg_php_client/autoload.php';

class ApiFactory {
    private static $cache = array();

    public static function createApi($homeserver, $username, $password) {
        if (isset(self::$cache[$homeserver.$username]) and self::$cache[$homeserver.$username]) {
            return self::$cache[$homeserver.$username];
        }

        $access_token = null;
        $user_id = null;
        $api = null;

        $matrixUser = self::fetchStoredMatrixUser($username,$homeserver,'https://vector.im');
        if ($matrixUser) {
            $api = new MatrixOrg_API($homeserver, $matrixUser->getMatrixTokenDecrypted());

            $api->setAutoLoginUsername($username);
            $api->setAutoLoginPassword($password);

            $user_id = $matrixUser->getMatrixUsername();
            $api->setUserId($user_id);

            $result = $api->presenceStatus($user_id);

            if ($result['status'] != 200 and $result['errcode'] == 'M_UNKNOWN_TOKEN') {
                $res2 = $api->login($username,$password);
                if ($res2['status'] == 200) {
                    self::persistConnectionInfo($username,$homeserver,$api->getAccessToken(),$api->getUserId(), 'https://vector.im', $api->getDeviceId());
                }
            }

        } else {
            $api = new MatrixOrg_API($homeserver);

            $api->setAutoLoginUsername($username);
            $api->setAutoLoginPassword($password);

            $api->login($username, $password);

            self::persistConnectionInfo($username,$homeserver,$api->getAccessToken(),$api->getUserId(), 'https://vector.im', $api->getDeviceId());
        }

        self::$cache[$homeserver.$username] = $api;

        return $api;
    }


    private static function fetchStoredMatrixUser($username, $home_server, $identity_server) {
        $userMapper = \OC::$server->get(MatrixUserMapper::class);

        if ($userMapper) {
            return $userMapper->findByUserAndServers($username,$home_server, $identity_server);
        }
        return null;
    }

    private static function persistConnectionInfo($username, $home_server, $token, $matrix_username, $identity_server, $device_id ) {
        $res = false;
        $userMapper = \OC::$server->get(MatrixUserMapper::class);

        if ($userMapper) {
            $user = self::fetchStoredMatrixUser($username, $home_server, $identity_server);

            if ($user) {
                //Stores only the token
                $user->setAndEncryptMatrixToken($token);
                $user->setDeviceId($device_id);
                $res = $userMapper->update($user);
            } else {
                $user = new MatrixUser();
                $user->setUsername($username);
                $user->setMatrixUsername($matrix_username);
                $user->setHomeServerUrl($home_server);
                $user->setIdentityServerUrl($identity_server);
                $user->setAndEncryptMatrixToken($token);
                $user->setDeviceId($device_id);
                $user->setCreatedAt(time());
                $res = $userMapper->insert($user);
            }
        }
        return $res;
    }

}