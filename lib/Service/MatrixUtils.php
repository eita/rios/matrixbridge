<?php

namespace OCA\MatrixBridge\Service;

use CompareImages;
use OCP\IImage;
use \OCP\ILogger;
use \OCP\IL10N;

/**
 * Listen to nextcloud events (hooks)
 */
class MatrixUtils {

	public static $MATRIX_EXCHANGE_LOG = true;

	private $logger;

	/** @var  */
	private $AppName;
	private $il10n;

	public function __construct(ILogger $logger, $AppName, IL10N $il10n) {
		$this->logger = $logger;
		$this->AppName = $AppName;
		$this->il10n = $il10n;
	}

	public function log($msg, $file = 'input') {
		//return $this->logger->info($msg,array('app' => $this->AppName));
		$root = $_SERVER["DOCUMENT_ROOT"] ?: $_SERVER['PWD'];
		$filename = $root . "/{$file}.log";
		ob_start();
		echo "\n\n";
		echo date(DATE_ISO8601) . ": \n";
		print_r($msg);
		$info = ob_get_contents();
		ob_end_clean();
		file_put_contents($filename, $info, FILE_APPEND);
	}

	public function _($msg, $params = array()) {
		return $this->il10n->t($msg, $params);
	}

	/**
	 * @param IImage $image1
	 * @param IImage $image2
	 * @return int  If value < 11, the images are similar
	 */
	public function compareImages(IImage $image1, IImage $image2) {
		$imageComparator = new CompareImages($image1->resource());
		return $imageComparator->compareWith($image2->resource());
	}

	/*
	 * $uid - user id
	 * $source - 'AS' or 'HOOK'
	 */
	public static function matrix_exchange_log($uid, $source, $message, $file = 'matrix_exchange_log') {
		if (!self::$MATRIX_EXCHANGE_LOG) {
			return;
		}
		
		//return $this->logger->info($msg,array('app' => $this->AppName));
		$filename = $_SERVER["DOCUMENT_ROOT"] . "/{$file}.log";
		ob_start();
		$date = date(DATE_ISO8601);
		print_r("\n{$date} : UID={$uid} : FROM={$source} : {$message}");
		$info = ob_get_contents();
		ob_end_clean();
		file_put_contents($filename, $info, FILE_APPEND);
	}
}
