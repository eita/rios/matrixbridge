<?php
/**
 * @author Alan Tygel <alan@eita.org.br>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\MatrixBridge;

use OC\Group\Group;
use OC\User\User;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\IGroupManager;

class GroupHooks
{

    private $groupManager;
    private $matrixClient;

    public function __construct(IGroupManager $groupManager, MatrixClient $matrixClient)
    {

        $this->groupManager = $groupManager;
        $this->matrixClient = $matrixClient;
    }

    public function register()
    {
        $this->groupManager->listen('\OC\Group', 'preAddUser', [$this, 'preAddUserToGroup']);
        $this->groupManager->listen('\OC\Group', 'preRemoveUser', [$this, 'preRemoveUserFromGroup']);
        $this->groupManager->listen('\OC\Group', 'postCreate', [$this, 'postCreateGroup']);

    }

    public function preAddUserToGroup(Group $group, User $user)
    {
        $this->matrixClient->addUserToGroup($group->getGID(), $user->getUID());
        $this->matrixClient->alertBotOnUserAdd($user->getUID(), $group->getGID());
    }

    public function preRemoveUserFromGroup(Group $group, User $user)
    {
        $this->matrixClient->removeUserFromGroup($group->getGID(), $user->getUID());
    }

    public function postCreateGroup(Group $group)
    {
        $this->matrixClient->createGroup($group->getGID());
    }

}