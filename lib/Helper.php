<?php
/**
 *
 * @author Vinicius Brand <vinicius@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

namespace OCA\MatrixBridge;

use OCA\Circles\Db\CircleRequest;
use OCA\Circles\Exceptions\CircleNotFoundException;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\Circles\Service\CircleService;
use OCA\Circles\Service\CirclesService;
use OCA\Circles\Service\FederatedUserService;
use OCA\Circles\Service\MemberService;
use OCA\Circles\Service\MembersService;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCP\AppFramework\Http\DataResponse;
use OCP\IConfig;
use ReflectionProperty;
use Symfony\Component\Console\Output\OutputInterface;

class Helper
{

    /** @var IConfig */
    private $config;

    /**
     * Helper constructor.
     *
     * @param IConfig $config
     */
    public function __construct(IConfig $config)
    {
        $this->config = $config;
    }


    public function getRiotUrl()
    {
        return $this->config->getAppValue('matrixbridge', "riot_url", '');
    }

    public function setRiotUrl($url)
    {
        $this->config->setAppValue('matrixbridge', "riot_url", $url);

    }

    public function getGroupsToRoomsMap()
    {
        return json_decode($this->config->getAppValue('matrixbridge', "groups_to_rooms_map", '{}'), true);
    }

    public function setGroupsToRoomsMap($map)
    {
        $this->config->setAppValue('matrixbridge', "groups_to_rooms_map", json_encode($map));
    }

    public function getGroupRoomsOwnerName()
    {
        return $this->config->getAppValue('matrixbridge', "group_rooms_owner_name", '');
    }

    public function setGroupRoomsOwnerName($name)
    {
        $this->config->setAppValue('matrixbridge', "group_rooms_owner_name", $name);
    }

    public function getGroupRoomsOwnerPassword()
    {
        return $this->config->getAppValue('matrixbridge', "group_rooms_owner_p", '');
    }

    public function setGroupRoomsOwnerPassword($pw)
    {
        $this->config->setAppValue('matrixbridge', "group_rooms_owner_p", $pw);
    }

    public function setHomeserverUrl($value)
    {
        $this->config->setAppValue('matrixbridge', "homeserver_url", $value);
    }

    public function getHomeserverUrl()
    {
        return $this->config->getAppValue('matrixbridge', "homeserver_url", '');
    }

    public function setIdentifiersDomain($value)
    {
        $this->config->setAppValue('matrixbridge', "identifiers_domain", $value);
    }

    public function getIdentifiersDomain()
    {
        return $this->config->getAppValue('matrixbridge', "identifiers_domain", '');
    }

    public function getTestUserName()
    {
        return $this->config->getAppValue('matrixbridge', "test_user_name", '');
    }

    public function setTestUserName($pw)
    {
        $this->config->setAppValue('matrixbridge', "test_user_name", $pw);
    }

    public function getTestUserPassword()
    {
        return $this->config->getAppValue('matrixbridge', "test_user_password", '');
    }

    public function setTestUserPassword($pw)
    {
        $this->config->setAppValue('matrixbridge', "test_user_password", $pw);
    }

    public function changeUserIdCirclesService(CirclesService $circlesService, $userId)
    {
        $method = new ReflectionProperty('OCA\Circles\Service\CirclesService', 'userId');
        $method->setAccessible(true);

        $method->setValue($circlesService, $userId);
    }

    public function changeUserIdMembersService(MembersService $membersService, $userId)
    {
        $method = new ReflectionProperty('OCA\Circles\Service\MembersService', 'userId');
        $method->setAccessible(true);

        $method->setValue($membersService, $userId);
    }

    /**
     * @throws \OCA\Circles\Exceptions\InitiatorNotFoundException
     * @throws \OCA\Circles\Exceptions\RemoteResourceNotFoundException
     * @throws \OCA\Circles\Exceptions\ParseMemberLevelException
     * @throws \OCA\Circles\Exceptions\MemberNotFoundException
     * @throws \OCA\Circles\Exceptions\OwnerNotFoundException
     * @throws \OCA\Circles\Exceptions\InitiatorNotConfirmedException
     * @throws \OCA\Circles\Exceptions\FederatedItemException
     * @throws \OCA\Circles\Exceptions\FederatedEventException
     * @throws \OCA\Circles\Exceptions\RequestBuilderException
     * @throws \OCA\Circles\Exceptions\RemoteNotFoundException
     * @throws \OCA\Circles\Exceptions\UnknownRemoteException
     */
    public function memberLevel(MemberService $memberService, string $circleId, string $memberId, $level)
    {
        if (is_int($level)) {
            $level = Member::parseLevelInt($level);
        } else {
            $level = Member::parseLevelString($level);
        }

        $memberService->getMemberById($memberId, $circleId);
        return $memberService->memberLevel($memberId, $level);
    }


    public function memberAdd(FederatedUserService $federatedUserService, MemberService $memberService, string $circleId, string $userId)
    {
        $federatedUser = $federatedUserService->generateFederatedUser($userId, Member::TYPE_USER);
        $memberService->addMember($circleId, $federatedUser);
    }

    /**
     * Gets a circle for a room, or creates one if circle is inexistent
     * @param $mx_room_id
     * @param null $owner
     * @param OutputInterface|null $output
     * @param false $simulate
     * @return array|Circle|null
     * @throws \OCA\Circles\Exceptions\CircleNameTooShortException
     * @throws \OCA\Circles\Exceptions\FederatedEventException
     * @throws \OCA\Circles\Exceptions\FederatedItemException
     * @throws \OCA\Circles\Exceptions\InitiatorNotConfirmedException
     * @throws \OCA\Circles\Exceptions\InitiatorNotFoundException
     * @throws \OCA\Circles\Exceptions\OwnerNotFoundException
     * @throws \OCA\Circles\Exceptions\RemoteInstanceException
     * @throws \OCA\Circles\Exceptions\RemoteNotFoundException
     * @throws \OCA\Circles\Exceptions\RemoteResourceNotFoundException
     * @throws \OCA\Circles\Exceptions\RequestBuilderException
     * @throws \OCA\Circles\Exceptions\UnknownRemoteException
     */
    public function getCircle(CircleRequest $circleRequest, CircleService $circleService, RoomCircleMapper $roomCircleMapper, $mx_room_id, $owner = null, OutputInterface $output = null, $simulate = false, $createIfInexistent = true)
    {
        $roomCircle = $roomCircleMapper->findByMxRoomId($mx_room_id);
        $circle = null;
        if ($roomCircle) {
            try {
                $circle = $circleRequest->getCircle($roomCircle->getCircleUniqueId(),null);
                //$circle2 = $this->circleRequest->getCircle($roomCircle->getCircleUniqueId(), null);

            } catch (CircleNotFoundException $e) {
                // maybe current user is creator that is not anymore in the circle.
                // Must set user session to the current circle owner.
            }
        }

        if ($circle === null && $createIfInexistent) {
            if ($output) {
                $output->writeln("     * create circle $mx_room_id");
            }
            if (!$simulate) {
                $outcome = $circleService->create($mx_room_id, $owner);
                $circleId = $outcome['id'];
                $config = $outcome['config'];
                $circleService->updateConfig($circleId, ($config | Circle::CFG_MOUNTPOINT | Circle::CFG_HIDDEN) & !Circle::CFG_VISIBLE );

                $roomCircleMapper->createOrUpdate($circleId, $mx_room_id);
                $circle = $circleService->getCircle($circleId);
            }
        }
        return $circle;
    }

}
