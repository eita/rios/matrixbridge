<?php
/**
 * @author Alan Tygel <alan@eita.org.br>
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


namespace OCA\MatrixBridge;

require_once \OC_App::getAppPath('matrixbridge') . '/3rdparty/matrixorg_php_client/autoload.php';
require_once \OC_App::getAppPath('matrixbridge') . '/3rdparty/compare_images/CompareImages.php';


use MatrixOrg_API;
use OC\User\User;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Service\ApiFactory;
use OCA\MatrixBridge\Service\MatrixClient;
use OCA\MatrixBridge\Service\MatrixListener;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\ISession;
use OCP\IUserManager;
use OCP\Security\ICrypto;


class UserHooks
{

    private $userManager;
    private $userMapper;
    private $helper;
    private $matrixClient;
    private $dispatcher;
    private $userId;

    public function __construct($UserId,
                                IUserManager $UserManager,
                                ISession $session,
                                ICrypto $crypto,
                                MatrixUserMapper $matrixUserMapper,
                                MatrixClient $matrixClient,
                                IEventDispatcher $dispatcher)
    {
        $this->userManager = $UserManager;
        $this->crypto = $crypto;
        $this->session = $session;
        $this->userMapper = $matrixUserMapper;
        $this->dispatcher = $dispatcher;
        $this->userId = $UserId;

        $this->helper = new Helper(\OC::$server->getConfig());

        $this->matrixClient = $matrixClient;
    }

    public function register()
    {
        \OCP\Util::connectHook('OC_User', 'post_login', $this, 'getMatrixToken');
        \OCP\Util::connectHook('OC_User', 'post_setPassword', $this, 'getMatrixToken');
        \OCP\Util::connectHook('OC_User', 'post_createUser', $this, 'getMatrixToken');
        // \OCP\Util::connectHook('OC_User', 'logout', $this, 'invalidateMatrixAccessToken'); // We gave up invalidating the token when nextcloud logs out.
        $this->userManager->listen('\OC\User', 'changeUser', array($this, 'changeUserHook'));
    }

    /**
     * Gets a matrix token from the matrix server for an user, storing it in the matrixbridge_users table
     * If there is a record already, does not try to get a new token;
     * @param array $params
     */
    public function getMatrixToken(array $params)
    {
        $homeServerUrl = filter_var($this->helper->getHomeserverUrl(), FILTER_VALIDATE_URL);
        $matrixUser = $this->userMapper->findByUidAndHomeServer($params['uid'], $homeServerUrl);
        if (!$matrixUser) {
            $api = ApiFactory::createApi($homeServerUrl, $params['uid'], $params['password']);
        }
    }

    /**
     * Gets matrix token stored in DB and invalidates it in (all) the logged-in Matrix servers
     */
    public function invalidateMatrixAccessToken()
    {
        $homeserverUrl = $this->helper->getHomeserverUrl();
        $matrixUser = $this->userMapper->findByUidAndHomeServer($this->userId, $homeserverUrl);
        if ($matrixUser) {
            $api = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixUser->getMatrixTokenDecrypted());
            $api->setUserId($matrixUser->getMatrixUsername());

            $result = $api->logout();
            if ($result['status'] == 200) {
                $this->userMapper->delete($matrixUser);
            }
        }
    }


    /**
     * @param User $user
     * @param $feature
     */
    public function changeUserHook($user, $feature)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            switch ($feature) {
                //Change avatar in all matrix accounts associated with current user
                /*
				case 'avatar':
					$this->matrixClient->updateUserAvatar($user->getUID());
					break;
                */
                case 'displayName':
                    $this->matrixClient->changeUserDisplayName($user->getUID(), $user->getDisplayName());
                    break;
                default:
            }
        }
    }
}
