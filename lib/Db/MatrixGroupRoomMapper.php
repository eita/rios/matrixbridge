<?php
// db/MatrixUserMapper.php

namespace OCA\MatrixBridge\Db;

use OCP\IDBConnection;
use OCP\AppFramework\Db\Mapper;

class MatrixGroupRoomMapper extends Mapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrixbridge_group_rooms');
	}


	/**
	 * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
	 * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
	 */
	public function find($id) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_group_rooms` ' .
			'WHERE `id` = ?';
		return $this->findEntity($sql, [$id]);
	}


	public function findAll($limit = null, $offset = null) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_group_rooms`';
		return $this->findEntities($sql, $limit, $offset);
	}

	public function findAllByGid($gid) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_group_rooms`' .
			'WHERE `gid` = ?';
		return $this->findEntities($sql, [$gid]);
	}

    public function findAllByRoomId($roomId) {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_group_rooms`' .
            'WHERE `room_id` = ?';
        return $this->findEntities($sql, [$roomId]);
    }

}