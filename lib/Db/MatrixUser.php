<?php
// db/MatrixUser.php
namespace OCA\MatrixBridge\Db;

use OCP\AppFramework\Db\Entity;
use OCP\Security\ICrypto;

class MatrixUser extends Entity
{

    protected $username;
    protected $matrixUsername;
    protected $matrixToken;
    protected $homeServerUrl;
    protected $identityServerUrl;
    protected $createdAt;
    protected $deviceId;
    protected $matrixTokenEncoded;


    public function __construct()
    {
        // add types in constructor
        $this->addType('createdAt', 'integer');
        $this->addType('matrixTokenEncoded', 'boolean');
    }

    /**
     * Verify if Matrix token is valid
     * @param $api
     * @param null $matrixUtils
     * @return bool|null null if server is offline
     */
    public function verifyToken($api, $matrixUtils = null)
    {
        $api->setAccessToken($this->getMatrixTokenDecrypted());
        $result = $api->presenceStatus($this->matrixUsername);
        if ($matrixUtils) {
            $matrixUtils->log("* verifyToken (username={$this->username}) result: " . json_encode($result, JSON_PRETTY_PRINT));
        }

        switch ($result['status']) {
            case 401:
                return false;
            case 200:
                return true;
            case 503: //Server unreachable
                return null;
            default:
                return null;
        }
    }

    public function getMatrixTokenDecrypted() {
        if ($this->getMatrixTokenEncoded()) {
            $crypto = \OC::$server->get(ICrypto::class);
            return $crypto->decrypt($this->getMatrixToken());
        } else {
            return $this->getMatrixToken();
        }
    }

    public function setAndEncryptMatrixToken($matrixToken) {
        $crypto = \OC::$server->get(ICrypto::class);
        $this->setMatrixToken($crypto->encrypt($matrixToken));
        $this->setMatrixTokenEncoded(true);
    }

}
