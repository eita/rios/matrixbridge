<?php
// db/MatrixUserMapper.php

namespace OCA\MatrixBridge\Db;

use OCP\IDBConnection;
use OCP\AppFramework\Db\Mapper;

use Exception;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;

class MatrixUserMapper extends Mapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrixbridge_users');
	}

	/**
	 * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
	 * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
	 */
	public function find($id) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users` ' .
			'WHERE `id` = ?  AND device_id is not null  ';
		return $this->findEntity($sql, [$id]);
	}


	public function findAll($limit = null, $offset = null) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users` WHERE device_id is not null ';
		return $this->findEntities($sql, $limit, $offset);
	}

	public function findAllByUser($username) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users`' .
			'WHERE `username` = ?  AND device_id is not null ';
		return $this->findEntities($sql, [$username]);
	}

	public function findByUser($username) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users`' .
			'WHERE `username` = ?  AND device_id is not null ORDER BY id DESC LIMIT 1 ';
		return $this->findEntity($sql, [$username]);
	}

	public function findByMxUsername($mx_username) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users`' .
			'WHERE `matrix_username` = ?  AND device_id is not null ORDER BY id DESC LIMIT 1 ';
		return $this->findEntity($sql, [$mx_username]);
	}

	public function findByUidAndHomeServer($uid, $home_server) {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_users` ' .
            'WHERE `username` = ? AND home_server_url = ? AND device_id is not null ORDER BY id DESC LIMIT 1 ';
        try {
            return $this->findEntity($sql, [$uid, $home_server]);
        } catch (Exception $e) {
            if ($e instanceof DoesNotExistException ||
                $e instanceof MultipleObjectsReturnedException
            ) {
                return null;
            } else {
                throw $e;
            }
        }
    }

	public function findByUserAndServers($username, $home_server, $identity_server) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_users` ' .
			'WHERE `username` = ? AND home_server_url = ? and identity_server_url = ? AND device_id is not null ORDER BY id DESC LIMIT 1 ';
		try {
			return $this->findEntity($sql, [$username, $home_server, $identity_server]);
		} catch (Exception $e) {
			if ($e instanceof DoesNotExistException ||
				$e instanceof MultipleObjectsReturnedException
			) {
				return null;
			} else {
				throw $e;
			}
		}
	}


}