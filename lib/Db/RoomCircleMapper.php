<?php
// db/MatrixUserMapper.php

namespace OCA\MatrixBridge\Db;

use OCA\Circles\Api\v1\Circles;
use OCA\Circles\Exceptions\CircleAlreadyExistsException;
use OCA\Circles\Model\BaseCircle;
use OCA\Circles\Model\Circle;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\IDBConnection;
use OCP\AppFramework\Db\Mapper;
use Symfony\Component\Console\Output\OutputInterface;

class RoomCircleMapper extends Mapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'matrixbridge_room_circle');
    }


    /**
     * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
     */
    public function find($id)
    {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_room_circle` ' .
            'WHERE `id` = ?';
        return $this->findEntity($sql, [$id]);
    }


    public function findAll($limit = null, $offset = null)
    {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_room_circle`';
        return $this->findEntities($sql, [], $limit, $offset);
    }

    /**
     * @param Circle $circle
     * @param MatrixRoomCache $mxRoomCache
     */
    public function createOrUpdate($circleSingleId, $mxRoomId)
    {
        $roomCircle = $this->findByMxRoomId($mxRoomId);
        if ($roomCircle) {
            $roomCircle->setMxRoomId($mxRoomId);
            $roomCircle->setCircleUniqueId($circleSingleId);
            return $this->update($roomCircle);
        } else {
            $roomCircle = new RoomCircle();
            $roomCircle->setMxRoomId($mxRoomId);
            $roomCircle->setCircleUniqueId($circleSingleId);
            return $this->insert($roomCircle);
        }
    }

    public function findByMxRoomId($mx_room_id)
    {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_room_circle` ' .
            'WHERE `mx_room_id` = ?';
        try {
            $ret = $this->findEntity($sql, [$mx_room_id]);
        } catch (DoesNotExistException $err) {
            $ret = false;
        } catch (MultipleObjectsReturnedException $err) {
            $ret = false;
        }
        return $ret;
    }

    public function findByCircleUniqueId($circle_unique_id)
    {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_room_circle` ' .
            'WHERE `circle_unique_id` = ?';
        try {
            $ret = $this->findEntity($sql, [$circle_unique_id]);
        } catch (DoesNotExistException $err) {
            $ret = false;
        } catch (MultipleObjectsReturnedException $err) {
            $ret = false;
        }
        return $ret;
    }

    public function findAllByDefaultFolder($fileid)
    {
        $sql = 'SELECT * FROM `*PREFIX*matrixbridge_room_circle` ' .
            'WHERE `default_folder_fileid` = ?';
        return $this->findEntities($sql, [$fileid]);
    }

}