<?php
// db/MatrixUser.php
namespace OCA\MatrixBridge\Db;

use OCP\AppFramework\Db\Entity;

class RoomCircle extends Entity {

	protected $mxRoomId;
	protected $mxRoomTopic;
	protected $circleUniqueId;
	protected $defaultFolderFileid;
	protected $defaultFolderOwnerUid;


	public function __construct() {
		$this->addType('$defaultFolderFileid', 'integer');
	}

}
