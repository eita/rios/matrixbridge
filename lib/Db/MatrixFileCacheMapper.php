<?php
// db/MatrixUserMapper.php

namespace OCA\MatrixBridge\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IDBConnection;
use OCP\AppFramework\Db\Mapper;

class MatrixFileCacheMapper extends Mapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrixbridge_mx_filecache');
	}


	/**
	 * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
	 * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
	 */
	public function find($id) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache` ' .
			'WHERE `id` = ?';
		return $this->findEntity($sql, [$id]);
	}


	public function findAll($limit = null, $offset = null) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache`';
		return $this->findEntities($sql, $limit, $offset);
	}

	public function findAllByFileid($fileid) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache` ' .
			   'WHERE `fileid` = ?';

		return $this->findEntities($sql, [$fileid]);
	}


	public function findByFileidAndRoomId($fileid, $mx_room_id) {
		try {
			$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache` ' .
			       'WHERE `fileid` = ? and `mx_room_id` = ?';
			return $this->findEntity($sql, [$fileid, $mx_room_id]);
		} catch (DoesNotExistException $exception) {
			return null;
		}
	}

	public function findAllByEventId($mx_event_id) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache` ' .
			'WHERE `event_id` = ?';

		return $this->findEntities($sql, [$mx_event_id]);
	}

	public function findAllByRoomIdAndFilename($mx_room_id, $mx_filename) {
		$sql = 'SELECT * FROM `*PREFIX*matrixbridge_mx_filecache` ' .
			'WHERE `mx_room_id` = ? AND `mx_filename` = ?';

		return $this->findEntities($sql, [$mx_room_id, $mx_filename]);
	}

	public function findAllByCircleId($circle_unique_id) {
		$sql = 'SELECT fcache.* FROM `*PREFIX*matrixbridge_mx_filecache` fcache ' .
		       '  JOIN `*PREFIX*matrixbridge_mx_room_circle` roomc ON roomc.mx_room_id = fcache.mx_room_id' .
			'WHERE roomc.circle_unique_id = ?';

		return $this->findEntities($sql, [$circle_unique_id]);
	}

	/**
	 * Creates or updates a fileCache for a given mx_room_id and mx_file_name
	 *
	 * @param $eventId
	 * @param $mxcUrl
	 * @param $creatorUid
	 * @param $fileid
	 * @return MatrixFileCache
	 */
	public function createOrUpdate($fileid, $eventId, $mxcUrl, $creatorUid, $mxRoomId, $mxFileName, $unshareOnRedact=false) {
		$fileCaches = $this->findAllByRoomIdAndFilename($mxRoomId, $mxFileName);
		if (count($fileCaches)) {
			$fileCache = $fileCaches[0];
			$fileCache->setFileid($fileid);
			$fileCache->setEventId($eventId);
			$fileCache->setMxcUrl($mxcUrl);
			$fileCache->setCreatorUid($creatorUid);
			$fileCache->setUnshareOnRedact($unshareOnRedact);
			return $this->update($fileCache);
		} else {
			$fileCache = new MatrixFileCache();
			$fileCache->setFileid($fileid);
			$fileCache->setEventId($eventId);
			$fileCache->setMxcUrl($mxcUrl);
			$fileCache->setCreatorUid($creatorUid);
			$fileCache->setMxRoomId($mxRoomId);
			$fileCache->setMxFilename($mxFileName);
			$fileCache->setUnshareOnRedact($unshareOnRedact);
			return $this->insert($fileCache);
		}
	}

}