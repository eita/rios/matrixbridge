<?php
// db/MatrixUser.php
namespace OCA\MatrixBridge\Db;

use OCP\AppFramework\Db\Entity;

class MatrixFileCache extends Entity {

	protected $eventId;
	protected $mxcUrl;
	protected $creatorUid;
	protected $fileid;
	protected $mxRoomId;
	protected $mxFilename;

	//Only unshare - does not delete - on file redaction in room
	protected $unshareOnRedact;

	public function __construct() {
		$this->addType('fileid', 'integer');
		$this->addType('unshareOnRedact', 'boolean');
	}

}
