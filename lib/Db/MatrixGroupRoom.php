<?php
// db/MatrixUser.php
namespace OCA\MatrixBridge\Db;

use OCP\AppFramework\Db\Entity;

class MatrixGroupRoom extends Entity {

	protected $gid;
	protected $roomId;
	protected $createdAt;

	public function __construct() {
		// add types in constructor
		$this->addType('createdAt', 'integer');
	}

}
