<?php
/**
 * Matrix.org chat integration
 *
 * @copyright 2017, 2022 EITA Workers' Cooperative (eita.org.br)
 *
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace OCA\MatrixBridge\Command;

use Error;
use Exception;
use MatrixOrg_API;
use OC\User\User;
use OCA\Circles\Db\CircleRequest;
use OCA\Circles\Exceptions\CircleAlreadyExistsException;
use OCA\Circles\Exceptions\CircleDoesNotExistException;
use OCA\Circles\Exceptions\CircleNotFoundException;
use OCA\Circles\Exceptions\FederatedItemBadRequestException;
use OCA\Circles\Exceptions\FederatedUserException;
use OCA\Circles\Exceptions\MemberAlreadyExistsException;
use OCA\Circles\Exceptions\MemberDoesNotExistException;
use OCA\Circles\Exceptions\MemberIsNotOwnerException;
use OCA\Circles\Exceptions\MemberLevelException;
use OCA\Circles\Exceptions\MemberNotFoundException;
use OCA\Circles\Model\BaseMember;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Probes\CircleProbe;
use OCA\Circles\Service\CircleService;
use OCA\Circles\Service\FederatedUserService;
use OCA\Circles\Service\MemberService;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Helper;
use OCA\MatrixBridge\Service\MatrixListener;
use OCA\MatrixBridge\UserHooks;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IUser;
use OCP\IUserManager;
use OCP\IUserSession;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncRoomsToCircles extends Command
{


    /*

        * Loop through all users:
            * for each user, query matrix server for all visible rooms, creating an array of the room with the informations: name, id and members
            * If the room id domain is not in the whitelist MatrixListener.ALLOWED_ROOM_DOMAINS, it must __not__ be added to the array.

        * Loop through all matrix rooms:
            * if there is a roomCircle defined for the room, check and fix:
                * circle members are equal to the matrix room members? (check and fix permissions too)
                * circle name is equal to the matrix room name?
            * if there is no roomCircle for this room:
                * create circle with same name as the room
                * create roomCircle linking the room to the newly created circle
                * add the users to the circle
            * __Important__: client-server hooks must be disabled when creating or updating circle, circle memberships and circle name:
            * __Important 2__: if an user's HomeServer is not in MatrixListener.ALLOWED_ROOM_DOMAINS, nothing should be done! (The user must not be added to the circle, since it does not exist in NextCloud/LDAP database.

        * Remove orphaned roomCircles:
           * Get all roomCircles, and remove those that have unknown matrix room id. Use the matrix Rooms array to check.

        * Remove orphaned circles:
           * Get all circles, and remove those that are not roomCircles

     */


    /** @var IUserManager */
    private $userManager;

    /** @var RoomCircleMapper */
    private $roomCircleMapper;

    /** @var MatrixUserMapper */
    private $userMapper;

    /** @var IUserSession */
    private $userSession;

    private $helper;

    private $simulate = false;
    private $verbose = false;

    private $allCircles = array();

    private $syncs = array();

    /** @var User[] */
    private $allUsers = array();

    /** @var array */
    private $allUserIds = array();

    private $circleService;
    private $memberService;
    private $circleRequest;
    private $federatedUserService;

    public function __construct(IUserManager         $userManager,
                                RoomCircleMapper     $roomCircleMapper,
                                MatrixUserMapper     $userMapper,
                                IUserSession         $userSession,
                                CircleService        $circleService,
                                MemberService        $memberService,
                                FederatedUserService $federatedUserService,
                                CircleRequest        $circleRequest)
    {
        $this->userManager = $userManager;
        $this->roomCircleMapper = $roomCircleMapper;
        $this->userMapper = $userMapper;
        $this->userSession = $userSession;
        $this->helper = new Helper(\OC::$server->getConfig());
        $this->circleService = $circleService;
        $this->memberService = $memberService;
        $this->federatedUserService = $federatedUserService;
        $this->circleRequest = $circleRequest;

        parent::__construct();

        $this->initAllCircles();
    }

    protected function configure()
    {
        $this
            ->setName('matrix:sync-rooms-to-circles')
            ->setDescription('syncs matrix rooms to circles and clears orphaned circles')
            ->addOption(
                'sim',
                null,
                InputOption::VALUE_NONE,
                'does not change database; just simulate'
            )
            ->addOption(
                'verb',
                null,
                InputOption::VALUE_NONE,
                'verbose'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        if ($input->getOption('sim')) {
            $this->simulate = true;
        }

        if ($input->getOption('verb')) {
            $this->verbose = true;
        }

        //Disables circle hooks -> prevent changes to persist again into matrix rooms
        MatrixListener::$AS_TRANSACTION = true;
        try {
            if ($this->simulate) {
                $output->writeln("SIMULATE MODE ON");
            }
            $this->allUsers = $this->userManager->search('');
            foreach ($this->allUsers as $user) {
                $this->allUserIds[] = $user->getUID();
            }

            $output->writeln("Creating matrix room array...");
            $matrixRooms = $this->getAllMatrixRooms($output);
            $output->writeln("Done. " . count($matrixRooms) . " matrix rooms found in Home Server: " . $this->helper->getHomeserverUrl());

            if (count($matrixRooms) == 0) {
                throw new \Exception("Error: no room found!");
            }

            /*
            if (!$this->simulate) {
                $helper = $this->getHelper('question');

                $question = new ConfirmationQuestion("This is NOT a simulation. Will consider only circles&rooms of the successful users.\nWill delete any other eventual circles with rooms that have not been found (so be sure it was OK for every user that matters).\nDo you want to continue?");
                if (!$helper->ask($input, $output, $question)) {
                    return 1;
                }
            }
            */

            /* DEBUG
            $matrixCircles = $this->getAllMatrixCircles();*/

            try {
                $rooms = json_encode($matrixRooms);
                file_put_contents("/var/www/html/matrix_rooms.json",$rooms);
            } catch (\Exception $exp) {

            }

            $output->writeln("Syncing matrix rooms to circles...");
            $roomCircles = $this->syncMatrixRoomsToCircles($matrixRooms, $output);
            $output->writeln("Done. ");
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
        }
    }

    /**
     * @return \MatrixOrg_RoomState[]
     */
    private function getAllMatrixRooms(OutputInterface $output)
    {

        $output->writeln("    * Getting visible rooms for " . count($this->allUsers) . " users.");

        $rooms = array();

        foreach ($this->allUsers as $user) {
            $output->write("      * Querying matrix server for user {$user->getUID()}...");

            try {
                $uid = $user->getUID();
                $matrixUser = $this->userMapper->findByUidAndHomeServer($uid,$this->helper->getHomeserverUrl());
                $api = null;

                if ($matrixUser) {
                    $matrixToken = $matrixUser->getMatrixTokenDecrypted();
                    if ($matrixToken) {
                        $api = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixToken);
                        $api->setUserId($matrixUser->getMatrixUsername());
                    }
                } else {
                    $output->writeln("\t\t ERROR: No Matrix Token.");
                }

                if ($api) {
                    $output->write("  APIOK...");
                    $result = $api->sync();
                    if ($result['status'] == 200) {
                        #$this->syncs[$user->getUID()] = $result['data'];
                        $output->writeln("\t\tOK!");

                        foreach ($result['data']['rooms']['join'] as $room_key => $room_data) {
                            if (!array_key_exists($room_key, $rooms) and $this->allowedRoomId($room_key)) {
                                $roomState = $api->getState($room_key);
                                if (!empty($roomState->name)) {
                                    $rooms[$room_key] = $roomState;
                                }
                            }
                        }
                    } else {
                        $output->writeln("\t\tERROR: " . json_encode($result['data']));

                        #throw new \Exception("Could not connect to matrix server with user {$user->getUID()}.");
                    }
                }
            } catch (\MatrixOrg_Exception_Connection $exp) {
                $output->writeln("\t\t Could not connect to matrix server with this user.");
            } catch (DoesNotExistException $exp) {
                $output->writeln("\t\t Matrix token not found for this user.");
            }

        }

        return $rooms;
    }

    /**
     * @param \MatrixOrg_RoomState[] $matrixRooms
     */
    private function syncMatrixRoomsToCircles($matrixRooms, OutputInterface $output)
    {
        $foundCircles = array();

        foreach ($matrixRooms as $matrixRoomState) {
            try {
                $output->writeln("    syncing room: {$matrixRoomState->name} ({$matrixRoomState->roomId})");

                /*
                $probableRoomOwner = $this->matrixUIDtoCloudUID($matrixRoomState->creatorUid);
                if (!$probableRoomOwner) {
                    #get any member to be room creator, and changes later in the script
                    foreach ($matrixRoomState->members as $mx_user_id => $data) {
                        $probableRoomOwner = $this->matrixUIDtoCloudUID($mx_user_id);
                        if ($probableRoomOwner) break;
                    }
                }
                if (!$probableRoomOwner) continue;
                */

                $circleOwner = $this->guessCircleOwner($matrixRoomState->roomId);
                $this->setSessionUser($circleOwner);
                $circle = null;

                try {
                    try {
                        $circle = $this->getCircle($matrixRoomState->roomId, null, $output, $this->simulate);
                    } catch (CircleAlreadyExistsException $exp) {
                        #try to find circle by roomId
                        $circle = $this->getCircleByName($matrixRoomState->roomId);
                        #if circle is found, must check for possible inexistent room circle
                        if ($circle) {
                            $this->resolveRoomCircle($circle, $matrixRoomState, $output);
                        }
                    }
                } catch (\Exception $exp) {
                    $output->writeln("Exception: could not get/create circle. Exception=" . json_encode($exp));
                }


                #when simulated, circle may be inexistent
                if (!$circle) {
                    continue;
                }

                $this->verifyCircleName($circle, $matrixRoomState, $output);
                $this->addMembers($circle, $matrixRoomState, $output);
                $this->changeOwnerIfNeeded($circle, $matrixRoomState, $output);
                $this->fixMemberLevels($circle, $matrixRoomState, $output);
                $this->membersLeave($circle, $matrixRoomState, $output);
                $this->fixJoinRules($circle, $matrixRoomState, $output);

                $foundCircles[] = $circle;
            } catch (Exception $exp) {
                $output->writeln("EXCEPTION dealing with room. \n  message={$exp->getMessage()}\n  trace: {$exp->getTraceAsString()}");
            } catch (Error $exp) {
                $output->writeln("ERROR dealing with room. \n  message={$exp->getMessage()}\n  trace: {$exp->getTraceAsString()}");
            }

        }

        //$output->writeln("will try to remove orphaned circles");

        /* TODO change this code to remove only orphaned circles that once related to matrix rooms
        #remove orphaned circles
        $allCircles = $this->listCircles();

        foreach ($allCircles as $circle) {
            $found = false;
            $circleUniqueId = $circle->getSingleId();
            foreach ($foundCircles as $foundCircle) {
                if ($circleUniqueId == $foundCircle->getSingleId()) {
                    $found = true;
                }
            }
            if (!$found) {
                $owner = $this->userManager->get($circle->getOwner()->getUserId());
                $this->setSessionUser($owner);
                $output->writeln("    destroying circle #{$circle->getName()}");

                if (!$this->simulate) {
                    try {
                        $this->circleService->destroy($circleUniqueId);
                    } catch (CircleDoesNotExistException $e) {
                        $output->writeln("     Exception: circle does not exist");
                    } catch (MemberIsNotOwnerException $e) {
                        $output->writeln("     Exception: member {$circle->getOwner()->getUserId()} is not owner. owner=" . ($owner ? $owner->getUID() : 'null. (did not find user?)'));

                    }
                }

                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
                $output->writeln("    destroying roomCircle for circle: #{$circle->getName()}");

                if (!$this->simulate) {
                    if ($roomCircle) {
                        $this->roomCircleMapper->delete($roomCircle);
                    } else {
                        $output->writeln("      RoomCircle not found!");
                    }
                }
            }
        }
        */
    }

    private function allowedRoomId($room_key)
    {
        preg_match('/:(.*)$/', $room_key, $parts);
        return ($parts and $parts[1] and in_array($parts[1], MatrixListener::ALLOWED_ROOM_DOMAINS));
    }

    /**
     * Translates a matrix UID to a Nextcloud UID
     * @param $matrix_user_id
     */
    private function matrixUIDtoCloudUID($matrix_user_id)
    {
        if (preg_match('/^@([^:]*):' . $this->helper->getIdentifiersDomain() . '/', $matrix_user_id, $matches)) {
            return $matches[1];
        }
        return false;
    }

    private function initAllCircles()
    {
        $circles = array();

        foreach ($this->allUsers as $user) {
            $this->setSessionUser($user);
            $circles[$user->getUID()] = $this->listCircles();
        }
        $this->allCircles = $circles;
    }

    private function getCircleByName($name)
    {
        foreach ($this->allCircles as $uid => $circles) {
            /** @var Circle $circle */
            foreach ($circles as $circle) {
                if ($circle->getName() == $name) {
                    return $circle;
                }
            }
        }
        return null;
    }

    /**
     * Solves the case when there is a circle with the room's name but not a roomCircle
     * @param Circle $circle
     * @param \MatrixOrg_RoomState $matrixRoomState
     */
    private function resolveRoomCircle(Circle $circle, \MatrixOrg_RoomState $matrixRoomState, OutputInterface $output)
    {
        if (!$circle) {
            return;
        }

        //check if the circle is associated with another room.
        $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circle->getSingleId());

        if ($roomCircle) {
            $output->writeln("     * found circle that have the same name but belongs to other room. Possible room name collision.");
            //TODO
            //  * create a new circle for the matrixRoomState, with the name as room_id
            //  * create a new roomCircle for the matrixRoomState
            //  * changes the name of the circle so it does not have the same name of the previous circle
            //  * changes the name of the room
        } else {
            $output->writeln("     * Creating roomCircle for room={$matrixRoomState->name} and circle={$circle->getSingleId()}");
            if (!$this->simulate) {
                $this->roomCircleMapper->createOrUpdate($circle->getSingleId(), $matrixRoomState->roomId);
            }
        }
    }

    private function setSessionUser($userId)
    {
        if (is_string($userId)) {
            $user = $this->userManager->get($userId);
        } elseif (is_a($userId, IUser::class)) {
            $user = $userId;
        }

        //END
        if ($user) {
            $this->userSession->setUser($user);
            $this->federatedUserService->setLocalCurrentUser($user);
        }
        return $user;
    }

    private function changeOwnerIfNeeded(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output)
    {
        $this->mountCircleOwnerContext($circle);

        # must change owner
        #  if current circle owner is not a room admin anymore
        #   OR
        #  if current circle owner is not a room member anymore

        #choosing a owner:
        # if the room creator is a room admin
        #   s/he is the new owner
        # else
        #   pick the first user with the admin status

        $owner = $circle->getOwner();
        $ownerUid = $owner->getUserId();

        $ownerCandidates = array();
        $ownerLevel = -1;
        $nextOwner = null;
        foreach ($roomState->members as $member) {
            $memberUid = $this->matrixUIDtoCloudUID($member['user_id']);
            if (!$memberUid) continue;
            $powerLevel = (int)$member['power_level'];

            if ($memberUid == $ownerUid) {
                $ownerLevel = $powerLevel;
            }

            if (!$ownerCandidates[$powerLevel]) {
                $ownerCandidates[$powerLevel] = $memberUid;
            }
        }

        krsort($ownerCandidates);

        foreach ($ownerCandidates as $pl => $oc) {
            if ($ownerLevel == $pl) {
                break;
            }
            $user = $this->userManager->get($oc);
            if ($user) { //only if user exists...
                $nextOwner = $oc;
                break;
            }
        }

        if ($nextOwner) {
            $output->writeln("     * new circle owner: circle={$circle->getName()} oldOwner={$ownerUid} newOwner={$nextOwner}");

            if (!$this->simulate) {
                try {
                    //adds user if they is not part of the circle
                    try {
                        $this->setSessionUser($owner->getUserId());
                        $this->helper->memberAdd($this->federatedUserService, $this->memberService, $circle->getSingleId(), $nextOwner);
                        $user = $this->userManager->get($nextOwner);
                        $this->setSessionUser($user);
                        $this->circleService->circleJoin($circle->getSingleId());
                        $this->setSessionUser($owner->getUserId());
                    } catch (MemberAlreadyExistsException $exp) {

                    } catch (FederatedItemBadRequestException $exp) {
                    }

                    //get memberId
                    $members = $circle->getMembers();
                    foreach ($members as $member) {
                        if ($member->getUserId() == $nextOwner) {
                            $this->helper->memberLevel($this->memberService, $circle->getSingleId(), $member->getId(), BaseMember::LEVEL_OWNER);
                            return;
                        }
                    }
                } catch (MemberNotFoundException $exp) {
                    $output->writeln("       ###### ERROR: MEMBER {$nextOwner} NOT FOUND IN CIRCLE {$circle->getName()} ({$circle->getSingleId()}) ######");
                }

            }
        }

    }

    private function addMembers(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output)
    {
        $this->mountCircleOwnerContext($circle);

        #verify circle members
        $ownerMember = $circle->getOwner();
        $ownerUser = $this->userManager->get($ownerMember->getUserId());
        $circleMembers = $circle->getMembers();

        foreach ($roomState->members as $member) {
            try {
                if (!$member['user_id']) {
                    continue;
                }

                $roomMemberName = $this->matrixUIDtoCloudUID($member['user_id']);

                if (!$roomMemberName) {
                    $output->writeln("        could not add member " . $member['user_id'] . " because s/he is not a nextcloud user");
                    continue;
                }

                if ($ownerMember->getUserId() != $roomMemberName) {
                    $circleMember = null;

                    #check if room member is in circle
                    foreach ($circleMembers as $cm) {
                        if ($cm->getUserId() == $roomMemberName) {
                            $circleMember = $cm;
                            break;
                        }
                    }

                    if (!$circleMember) {
                        #must add member to circle
                        $output->writeln("     * Member($roomMemberName): adding to circle");

                        if (!$this->simulate) {
                            try {
                                $this->setSessionUser($ownerUser);
                                $this->helper->memberAdd($this->federatedUserService, $this->memberService, $circle->getSingleId(), $roomMemberName);
                                if ($member['membership'] == 'join') {
                                    $user = $this->userManager->get($roomMemberName);
                                    $this->setSessionUser($user);
                                    $this->circleService->circleJoin($circle->getSingleId());
                                }
                            } catch (MemberAlreadyExistsException $exp) {

                            }

                        }
                    } else {
                        if ($this->verbose) {
                            $output->writeln("     * Member($roomMemberName): found in circle. Does not need to add...");
                        }
                    }
                }
            } catch (\Exception $exp) {
                $output->writeln("        could not add member " . $member['user_id'] . " Exception=" . json_encode($exp));
            }

        }
    }

    private function fixMemberLevels(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output)
    {
        $this->mountCircleOwnerContext($circle);

        $ownerMember = $circle->getOwner();
        $ownerUser = $this->userManager->get($ownerMember->getUserId());
        $circle = $this->circleService->getCircle($circle->getSingleId());
        $circleMembers = $circle->getMembers();

        foreach ($roomState->members as $member) {
            $roomMemberName = $this->matrixUIDtoCloudUID($member['user_id']);
            if (!$roomMemberName) continue;
            $circleMember = null;

            #check if room member is in circle
            foreach ($circleMembers as $cm) {
                if ($cm->getUserId() == $roomMemberName) {
                    $circleMember = $cm;
                    break;
                }
            }

            if ($circleMember) {
                #check user permissions in circle
                $level = (array_key_exists('power_level', $member)) ? (int)$member['power_level'] : 1;
                $newMemberLevel = null;

                if ($member['membership'] == 'join') {
                    if ($level >= 99) {
                        $newMemberLevel = !in_array($circleMember->getLevel(), [BaseMember::LEVEL_ADMIN, BaseMember::LEVEL_OWNER]) ? BaseMember::LEVEL_ADMIN : null;
                    } elseif ($level >= 50) {
                        $newMemberLevel = $circleMember->getLevel() != BaseMember::LEVEL_MODERATOR ? BaseMember::LEVEL_MODERATOR : null;
                    } else {
                        $newMemberLevel = $circleMember->getLevel() != BaseMember::LEVEL_MEMBER ? BaseMember::LEVEL_MEMBER : null;
                    }
                }

                if ($newMemberLevel and $circleMember->getLevel() != BaseMember::LEVEL_OWNER) {
                    $output->writeln("     * Member($roomMemberName): changing level from {$circleMember->getLevel()} to $newMemberLevel");

                    if (!$this->simulate) {
                        if ($circleMember->getLevel() == BaseMember::LEVEL_NONE) {
                            switch ($circleMember->getStatus()) {
                                case BaseMember::STATUS_INVITED:
                                    $output->writeln("       * User was previously invited, accepting invitation on behalf of user");
                                    $this->setSessionUser($roomMemberName);
                                    $this->circleService->circleJoin($circle->getSingleId());
                                    break;
                                case BaseMember::STATUS_REQUEST:
                                    $output->writeln("       * User requested to enter circle, accepting on behalf of circle owner");
                                    $this->setSessionUser($ownerUser);
                                    $this->helper->memberAdd($this->federatedUserService, $this->memberService, $circle->getSingleId(), $roomMemberName);
                                    break;
                            }

                        }

                        $this->setSessionUser($ownerUser);
                        $members = $circle->getMembers();
                        try {
                            $this->helper->memberLevel($this->memberService, $circle->getSingleId(), $circleMember->getId(), $newMemberLevel);
                        } catch (MemberNotFoundException $exp) {
                            $output->writeln("       MEXCEPTION could not change level: exp=" . $exp->getMessage());
                        } catch (\Exception $exp) {
                            $output->writeln("       EXCEPTION could not change level: exp=" . $exp->getMessage());
                        }

                    }
                } else {
                    if ($this->verbose) {
                        $output->writeln("     * VERBOSE: did not change level. Member=$roomMemberName oldMemberLevel={$circleMember->getLevel()} membership={$member['membership']}");
                    }
                }
            } else {
                if ($this->verbose) {
                    $output->writeln("     * CircleMember not found for user $roomMemberName");
                }
            }
            $roomMemberNames[] = $roomMemberName;
        }
    }

    private function membersLeave(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output)
    {
        $this->mountCircleOwnerContext($circle);

        $roomMemberNames = array();

        foreach ($roomState->members as $member) {
            $roomMemberName = $this->matrixUIDtoCloudUID($member['user_id']);
            if ($roomMemberName) {
                $roomMemberNames[] = $roomMemberName;
            }
        }

        $circleMembers = $circle->getMembers();
        foreach ($circleMembers as $cm) {
            if (!in_array($cm->getUserId(), $roomMemberNames) and $cm->getLevel() != BaseMember::LEVEL_NONE) {
                $output->writeln("     * Member({$cm->getUserId()}): leaving circle");
                if (!$this->simulate) {
                    $this->setSessionUser($cm->getUserId());
                    try {
                        $this->circleService->circleLeave($circle->getSingleId());
                    } catch (MemberLevelException $e) {
                        $output->writeln("       * ERROR: {$cm->getUserId()} could not leave circle (is this user the owner?)");
                    }
                }
            }
        }
    }

    private function verifyCircleName(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output)
    {
        $this->mountCircleOwnerContext($circle);

        #verify circle name
        try {
            if ($circle->getName() != $roomState->name) {
                $output->writeln("     * changing circle name from {$circle->getName()} to {$roomState->name}");
                if (!$this->simulate) {
                    $this->circleService->updateName($circle->getSingleId(), $roomState->name);
                }
            }
        } catch (\Exception $exp) {
            $output->writeln("       Exception: could not change circle name. exp=" . json_encode($exp));
        }

        #verify circle description / room topic
        try {
            if ($circle->getDescription() != $roomState->topic) {
                $output->writeln("     * changing circle description/topic from \"{$circle->getDescription()}\" to \"{$roomState->topic}\"");
                if (!$this->simulate) {
                    $this->circleService->updateDescription($circle->getSingleId(), (string) $roomState->topic);
                }
            }
        } catch (\Exception $exp) {
            $output->writeln("       Exception: could not change circle description/topic. exp=" . json_encode($exp));
        }

    }

    private function fixJoinRules(Circle $circle, \MatrixOrg_RoomState $roomState, OutputInterface $output) {
        $owner = $this->guessCircleOwner2($circle->getSingleId());
        $this->setSessionUser($owner);

        $circle = $this->circleService->getCircle($circle->getSingleId());

        foreach ($roomState->state_events as $e) {
            if ($e->type === 'm.room.join_rules') {
                $orig_config = $config = $circle->getConfig();
                //In matrix
                switch ($e->content->join_rule) {
                    case "public":
                        $config = ($config | Circle::CFG_VISIBLE | Circle::CFG_FRIEND | Circle::CFG_INVITE | Circle::CFG_OPEN);
                        break;
                    case "invite":
                        $config = ($config | Circle::CFG_INVITE | Circle::CFG_FRIEND) & ~Circle::CFG_VISIBLE;
                        break;
                    case "knock":
                        $config = ($config | Circle::CFG_INVITE | Circle::CFG_FRIEND | Circle::CFG_REQUEST) & ~Circle::CFG_VISIBLE;
                        break;
                    default:
                        return true;
                }
                #$config |= Circle::CFG_APP;
                if ($orig_config != $config) {
                    $output->writeln("       Join Rules. Changing circle config. old={$orig_config} new={$config} (join_rule={$e->content->join_rule})");
                    $this->circleService->updateConfig($circle->getSingleId(), $config);
                }
            }
        }
    }

    /**
     * Sets owner of circle as session user, reloads circle on behalf of owner
     * @param $circle
     */
    private function mountCircleOwnerContext(&$circle)
    {
        if (is_string($circle)) {
            #unique id
            $circle = $this->circleService->getCircle($circle);
        }
        $sessionUser = $this->userSession->getUser();
        if (!$sessionUser or $sessionUser->getUID() != $circle->getOwner()->getUserId()) {
            $this->setSessionUser($circle->getOwner()->getUserId());
            $circle = $this->circleService->getCircle($circle->getSingleId());
        }
    }

    private function listCircles()
    {

        $probe = new CircleProbe();
        $probe->includeHiddenCircles();
        $probe->includeNonVisibleCircles();
        $probe->includePersonalCircles();

        return $this->circleService->getCircles($probe);

    }

    private function getCircle($mxRoomId, $owner = null, OutputInterface $output = null, $simulate = false)
    {
        return $this->helper->getCircle($this->circleRequest, $this->circleService, $this->roomCircleMapper, $mxRoomId, $owner, $output, $simulate);
    }

    /**
     * @param $mxRoomId
     * @return string|null Returns the owner's uid
     * @throws \OCA\Circles\Exceptions\RequestBuilderException
     */
    private function guessCircleOwner($mxRoomId)
    {
        $roomCircle = $this->roomCircleMapper->findByMxRoomId($mxRoomId);
        if ($roomCircle) {
            try {
                $circle = $this->circleRequest->getCircle($roomCircle->getCircleUniqueId(), null);
                if ($circle->hasOwner()) {
                    return $circle->getOwner()->getUserId();
                }
            } catch (CircleNotFoundException $exp) {
                $i = 1;
            }
        }
        return null;
    }

    private function guessCircleOwner2($circleId) {
        try {
            $circle = $this->circleRequest->getCircle($circleId, null);
            if ($circle->hasOwner()) {
                return $circle->getOwner()->getUserId();
            }
        } catch (CircleNotFoundException $exp) {

        }
        return null;
    }

    private function cleanRoomData($matrixRooms, $circleRooms)
    {
        $rooms = [];
        foreach ($matrixRooms as $roomKey => $roomData) {
            $room = [];
            foreach (['name', 'creatorUid', 'joinRule', 'guestAccess', 'topic', 'avatarUrl', 'roomId', 'powerLevels', 'members'] as $roomProperty) {
                $room[$roomProperty] = $roomData->$roomProperty;
            }
            $rooms[$roomData->roomId] = ['matrix' => $room, 'circle' => $circleRooms[$roomKey]];
        }
        return $rooms;
    }


}
