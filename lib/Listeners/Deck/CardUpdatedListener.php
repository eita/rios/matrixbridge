<?php

namespace OCA\MatrixBridge\Listeners\Deck;

use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CardUpdatedListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof \OCA\Deck\Event\CardUpdatedEvent)) {
            return;
        }

        //TODO

    }
}