<?php

namespace OCA\MatrixBridge\Listeners\Deck;

use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\IURLGenerator;

class CardCreatedListener implements IEventListener
{
    private $matrixClient;
    private $stackMapper;
    private $boardMapper;
    private $aclMapper;
    private $roomCircleMapper;
    private $urlGenerator;

    public function __construct(IURLGenerator $urlGenerator, MatrixClient $matrixClient, \OCA\Deck\Db\StackMapper $stackMapper, \OCA\Deck\Db\AclMapper $aclMapper, \OCA\Deck\Db\BoardMapper $boardMapper, RoomCircleMapper $roomCircleMapper)
    {
        $this->matrixClient = $matrixClient;
        $this->stackMapper = $stackMapper;
        $this->aclMapper = $aclMapper;
        $this->boardMapper = $boardMapper;
        $this->roomCircleMapper = $roomCircleMapper;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof \OCA\Deck\Event\CardCreatedEvent)) {
            return;
        }

        $card = $event->getCard();
        $stack = $this->stackMapper->find($card->getStackId());
        $acls = $this->aclMapper->findAll($stack->getBoardId());
        $board = null;

        foreach ($acls as $acl) {
            if ($acl->getType() == \OCA\Deck\Db\Acl::PERMISSION_TYPE_CIRCLE) {
                $circleId = $acl->getParticipant();
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleId);
                if ($roomCircle) {
                    if (!$board) {
                        $board = $this->boardMapper->find($stack->getBoardId());
                    }
                    $message = <<<EOM
🗒 I just added a card (_{$card->getTitle()}_) to *{$board->getTitle()}/{$stack->getTitle()}*.
EOM;
                    $cardUrl = $this->urlGenerator->linkToRouteAbsolute('deck.page.index') . '#/board/' . $board->getId() . '/card/' . $card->getId();
                    $boardUrl = $this->urlGenerator->linkToRouteAbsolute('deck.page.index') . '#/board/' . $board->getId();

                    $formattedBody = <<<EOB
🗒 I just added <a href="{$cardUrl}"><strong>a card</strong></a> (<em>{$card->getTitle()}</em>) to <a href="{$boardUrl}"><strong>{$board->getTitle()}/{$stack->getTitle()}</strong></a>.
EOB;
                    $format = "org.matrix.custom.html";

                    $this->matrixClient->sendMessageToRoom($circleId, $message, $format, $formattedBody);
                }
            }
        }

    }
}