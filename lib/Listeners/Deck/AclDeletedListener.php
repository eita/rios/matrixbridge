<?php

namespace OCA\MatrixBridge\Listeners\Deck;

use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class AclDeletedListener implements IEventListener
{
    private $matrixClient;
    private $boardMapper;
    private $roomCircleMapper;

    public function __construct(MatrixClient $matrixClient, \OCA\Deck\Db\BoardMapper $boardMapper, RoomCircleMapper $roomCircleMapper) {
        $this->matrixClient = $matrixClient;
        $this->boardMapper = $boardMapper;
        $this->roomCircleMapper = $roomCircleMapper;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof \OCA\Deck\Event\AclDeletedEvent)) {
            return;
        }
        $acl = $event->getAcl();
        if ($acl->getType() == \OCA\Deck\Db\Acl::PERMISSION_TYPE_CIRCLE) {
            $circleId = $acl->getParticipant();
            $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleId);
            if ($roomCircle) {
                $board = $this->boardMapper->find($acl->getBoardId());

                $message = <<<EOM
📝 I just unshared the board *{$board->getTitle()}* with this room.
EOM;

                $formattedBody = <<<EOB
📝 I just unshared the board <strong>{$board->getTitle()}</strong> with this room. 
EOB;
                $format = "org.matrix.custom.html";

                $this->matrixClient->sendMessageToRoom($circleId, $message, $format, $formattedBody);
            }
        }
    }
}