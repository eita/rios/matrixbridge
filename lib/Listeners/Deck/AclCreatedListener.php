<?php

namespace OCA\MatrixBridge\Listeners\Deck;

use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\IURLGenerator;

class AclCreatedListener implements IEventListener
{
    private $matrixClient;
    private $boardMapper;
    private $urlGenerator;
    private $roomCircleMapper;

    public function __construct(IURLGenerator $urlGenerator, MatrixClient $matrixClient, \OCA\Deck\Db\BoardMapper $boardMapper, RoomCircleMapper $roomCircleMapper)
    {
        $this->matrixClient = $matrixClient;
        $this->boardMapper = $boardMapper;
        $this->urlGenerator = $urlGenerator;
        $this->roomCircleMapper = $roomCircleMapper;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof \OCA\Deck\Event\AclCreatedEvent)) {
            return;
        }

        $acl = $event->getAcl();
        if ($acl->getType() == \OCA\Deck\Db\Acl::PERMISSION_TYPE_CIRCLE) {
            $circleId = $acl->getParticipant();
            $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleId);
            if ($roomCircle) {
                $board = $this->boardMapper->find($acl->getBoardId());

                $message = <<<EOM
📝 I just shared the board *{$board->getTitle()}* with this room.
EOM;
                $boardUrl = $this->urlGenerator->linkToRouteAbsolute('deck.page.index') . '#' . '/board/' . $board->getId();

                $formattedBody = <<<EOB
📝 I just shared the board <a href="{$boardUrl}"><strong>{$board->getTitle()}</strong></a> with this room. 
EOB;
                $format = "org.matrix.custom.html";

                $this->matrixClient->sendMessageToRoom($circleId, $message, $format, $formattedBody);
            }
        }
    }
}