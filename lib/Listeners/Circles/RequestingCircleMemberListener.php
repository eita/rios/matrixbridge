<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\RequestingCircleMemberEvent;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class RequestingCircleMemberListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof RequestingCircleMemberEvent) {
            return;
        }

        #fired when
        # * owner adds user in a closed circle

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/rooms/!aTlfmrXFfwGwkaHKNF:localhost/invite

        /** @var Circle $circle */
        $circle = $event->getCircle();
        /** @var Member $member */
        $member = $event->getMember();

        $this->matrixClient->onMemberInvited($circle, $member);
    }
}