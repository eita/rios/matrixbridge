<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\EditingCircleMemberEvent;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class EditingCircleMemberListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof EditingCircleMemberEvent) {
            return;
        }

        /** @var Circle $circle */
        $circle = $event->getCircle();
        /** @var Member $member */
        $member = $event->getMember();

        $this->matrixClient->onMemberLevel($circle, $member, $event->getLevel());
    }
}