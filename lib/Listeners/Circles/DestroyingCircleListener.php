<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\DestroyingCircleEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class DestroyingCircleListener implements IEventListener
{

    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof DestroyingCircleEvent) {
            return;
        }

        $this->matrixClient->onCircleDestruction($event->getCircle()->getSingleId());
    }
}