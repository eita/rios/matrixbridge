<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\EditingCircleEvent;
use OCA\Circles\Model\Circle;
use OCA\Circles\Tools\Exceptions\ItemNotFoundException;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class EditingCircleListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof EditingCircleEvent) {
            return;
        }

        /** @var Circle $circle */
        $circle = $event->getCircle();

        $federatedEvent = $event->getFederatedEvent();
        $newName = null;
        $newDescription = null;

        $eventData = $federatedEvent->getData();

        if ($eventData->hasKey('name')) {
            $newName = $eventData->gItem('name');
        }

        if ($eventData->hasKey('description')) {
            $newDescription = $eventData->gItem('description');
        }

        $this->matrixClient->onSettingsChange($circle, $newName, $newDescription);
    }
}