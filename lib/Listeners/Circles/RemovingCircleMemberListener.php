<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\RemovingCircleMemberEvent;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class RemovingCircleMemberListener implements IEventListener
{

    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof RemovingCircleMemberEvent) {
            return;
        }


        #fired when
        # * user rejects invitation to enter closed circle
        # * user gives up entering a closed circle (leaves before invitation response)
        # * owner disapproves entrance request

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/rooms/!aTlfmrXFfwGwkaHKNF:localhost/leave

        /** @var Circle $circle */
        $circle = $event->getCircle();
        /** @var Member $member */
        $member = $event->getMember();

        $this->matrixClient->onMemberLeaving($circle, $member);
    }
}