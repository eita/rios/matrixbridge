<?php

namespace OCA\MatrixBridge\Listeners\Circles;

use OCA\Circles\Events\AddingCircleMemberEvent;
use OCA\Circles\Model\Circle;
use OCA\Circles\Model\Member;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class AddingCircleMemberListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!$event instanceof AddingCircleMemberEvent) {
            return;
        }

        #fired when:
        # * owner adds other user in a public circle
        # * user requests entrance in a public circle (no confirmation needed)
        # * user accepts invitation in a closed circle
        # * owner approves entrance request

        # matrix equivalent: http://localhost:8008/_matrix/client/r0/join/!aTlfmrXFfwGwkaHKNF:localhost

        /** @var Circle $circle */
        $circle = $event->getCircle();
        /** @var Member $member */
        $member = $event->getMember();

        $this->matrixClient->onMemberNew($circle, $member);

    }
}