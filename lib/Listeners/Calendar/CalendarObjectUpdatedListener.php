<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarObjectUpdatedEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarObjectUpdatedListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarObjectUpdatedEvent)) {
            return;
        }

        $this->matrixClient->onCalendarObjectUpdate(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares(),
            $event->getObjectData()
        );
    }
}