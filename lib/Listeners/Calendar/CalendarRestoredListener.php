<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarObjectRestoredEvent;
use OCA\DAV\Events\CalendarRestoredEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarRestoredListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarRestoredEvent)) {
            return;
        }

        $this->matrixClient->onCalendarRestore(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares()
        );
    }
}