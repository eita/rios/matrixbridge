<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarShareUpdatedEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarShareUpdatedListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarShareUpdatedEvent)) {
            return;
        }

        $this->matrixClient->onCalendarUpdateShares(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getOldShares(),
            $event->getAdded(),
            $event->getRemoved()
        );
    }
}