<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarUpdatedEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarUpdatedListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarUpdatedEvent)) {
            return;
        }

        $this->matrixClient->onCalendarChange(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares(),
            $event->getMutations()
        );
    }
}