<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarObjectDeletedEvent;
use OCA\DAV\Events\CalendarObjectMovedToTrashEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarObjectMovedToTrashListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarObjectMovedToTrashEvent)) {
            return;
        }
        $this->matrixClient->onCalendarObjectDeletion(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares(),
            $event->getObjectData()
        );
    }
}