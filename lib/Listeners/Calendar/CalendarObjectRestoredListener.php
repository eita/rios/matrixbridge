<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarObjectRestoredEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarObjectRestoredListener implements IEventListener
{
    private $matrixClient;

    public function __construct(MatrixClient $matrixClient) {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarObjectRestoredEvent)) {
            return;
        }

        $this->matrixClient->onCalendarObjectRestore(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares(),
            $event->getObjectData()
        );
    }
}