<?php

namespace OCA\MatrixBridge\Listeners\Calendar;

use OCA\DAV\Events\CalendarObjectCreatedEvent;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

class CalendarObjectCreatedListener implements IEventListener
{

    private $matrixClient;

    public function __construct(MatrixClient $matrixClient)
    {
        $this->matrixClient = $matrixClient;
    }

    /**
     * @inheritDoc
     */
    public function handle(Event $event): void
    {
        if (!($event instanceof CalendarObjectCreatedEvent)) {
            return;
        }
        $this->matrixClient->onCalendarObjectCreation(
            $event->getCalendarId(),
            $event->getCalendarData(),
            $event->getShares(),
            $event->getObjectData()
        );
    }
}