<?php

declare(strict_types=1);

namespace OCA\MatrixBridge\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

/**
 * Auto-generated migration step: Please modify to your needs!
 */
class Version1Date20211123235815 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function preSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options): ?ISchemaWrapper {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if (!$schema->hasTable('matrixbridge_users')) {
			$table = $schema->createTable('matrixbridge_users');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
				'length' => 4,
			]);
			$table->addColumn('username', 'string', [
				'notnull' => true,
				'length' => 64,
			]);
			$table->addColumn('matrix_username', 'string', [
				'notnull' => true,
				'length' => 128,
			]);
			$table->addColumn('home_server_url', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('identity_server_url', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('matrix_token', 'string', [
				'notnull' => true,
				'length' => 1024,
			]);
			$table->addColumn('created_at', 'integer', [
				'notnull' => false,
				'unsigned' => true,
			]);
			$table->addColumn('device_id', 'string', [
				'notnull' => false,
				'length' => 200,
			]);
			$table->setPrimaryKey(['id'], 'matrixbridge_users_id');
		}

		if (!$schema->hasTable('matrixbridge_group_rooms')) {
			$table = $schema->createTable('matrixbridge_group_rooms');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
				'length' => 4,
			]);
			$table->addColumn('gid', 'string', [
				'notnull' => true,
				'length' => 64,
			]);
			$table->addColumn('room_id', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('created_at', 'integer', [
				'notnull' => false,
				'unsigned' => true,
			]);
			$table->setPrimaryKey(['id'], 'matrixbridge_group_rooms_id');
		}

		if (!$schema->hasTable('matrixbridge_mx_filecache')) {
			$table = $schema->createTable('matrixbridge_mx_filecache');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
				'length' => 4,
			]);
			$table->addColumn('event_id', 'string', [
				'notnull' => true,
				'length' => 256,
			]);
			$table->addColumn('mxc_url', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('creator_uid', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('fileid', 'integer', [
				'notnull' => true,
				'unsigned' => true,
			]);
			$table->addColumn('mx_room_id', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('mx_filename', 'string', [
				'notnull' => true,
				'length' => 511,
			]);
			$table->addColumn('unshare_on_redact', 'boolean', [
				'notnull' => false,
				'default' => false,
			]);
			$table->setPrimaryKey(['id'], 'matrixbridge_filecache_id');
		}

		if (!$schema->hasTable('matrixbridge_room_circle')) {
			$table = $schema->createTable('matrixbridge_room_circle');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
				'length' => 4,
			]);
			$table->addColumn('mx_room_id', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('circle_unique_id', 'string', [
				'notnull' => true,
				'length' => 255,
			]);
			$table->addColumn('default_folder_fileid', 'integer', [
				'notnull' => false,
				'unsigned' => true,
			]);
			$table->addColumn('default_folder_owner_uid', 'string', [
				'notnull' => false,
			]);
			$table->addColumn('mx_room_topic', 'string', [
				'notnull' => false,
				'length' => 255,
			]);
			$table->setPrimaryKey(['id'], 'matrixbridge_room_circle_id');
			$table->addUniqueIndex(['circle_unique_id'], 'circle_unique_id_index');
			$table->addUniqueIndex(['mx_room_id'], 'mx_room_id_index');
		}
		return $schema;
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function postSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}
}
