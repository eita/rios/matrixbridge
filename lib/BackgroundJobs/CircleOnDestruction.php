<?php
/**
 * @author Alan Tygel <alan@eita.org.br>
 * @author Daniel Tygel <dtygel@eita.org.br>
 * @author Vinicius Brand <vinicius@eita.org.br>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\MatrixBridge\BackgroundJobs;

use OC\BackgroundJob\Job;
use OCA\Circles\Api\v1\Circles;
use OCA\MatrixBridge\Service\MatrixClient;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\IUserSession;

class CircleOnDestruction extends Job {

	/** @var ILogger */
	private $logger;

	/** @var MatrixClient */
	private $matrixClient;

	/** @var IUserSession */
	private $userSession;

	/** @var IUserManager */
	private $userManager;

	public function __construct(ILogger $logger, MatrixClient $matrixClient, IUserSession $userSession, IUserManager $userManager) {
		$this->logger = $logger;
		$this->matrixClient = $matrixClient;
		$this->userSession = $userSession;
		$this->userManager = $userManager;
	}

	protected function run($argument) {
		$currentUser = $this->userSession->getUser();
		$user = $this->userManager->get($argument['sessionUid']);
		$this->userSession->setUser($user);

		$this->matrixClient->onCircleDestruction($argument['circleUniqueId']);

		$this->userSession->setUser($currentUser);
	}

	public function execute($jobList, ILogger $logger = null) {
		$jobList->remove($this, $this->argument);
		parent::execute($jobList, $logger);
	}
}
