<?php
/**
 * Matrix.org chat integration
 *
 * @copyright 2017 Cooperativa EITA (eita.org.br)
 *
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace OCA\MatrixBridge;

use MatrixOrg_API;
use OC\Files\Filesystem;
use OC\Files\Node\Folder;
use OC\Files\View;
use OCA\MatrixBridge\Db\MatrixFileCacheMapper;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Db\RoomCircle;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCA\MatrixBridge\Service\MatrixListener;
use OCA\MatrixBridge\Service\MatrixUtils;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\Defaults;
use OCP\Files\FileInfo;
use OCP\Files\IRootFolder;
use OCP\Files\Node;
use OCP\Files\NotFoundException;
use OCP\ILogger;
use OCP\IUserSession;
use OCP\Util;
use OCP\Share;


class FilesHooks
{

    private $rootFolder;
    private $roomCircleMapper;
    private $helper;
    private $userSession;
    private $fileCacheMapper;
    private $logger;
    private $defaults;
    private $userId = null;
    private $currentUser;
    private $shareManager;
    private $userMapper;

    /**
     * shareId => sharedWith
     *
     * @var array
     */
    private $sharedWith = array();

    /** @var  MatrixOrg_API */
    private $matrixApis = array();

    private $uploaded = array();

    private $justRewrote = array();

    public function __construct($UserId,
                                IRootFolder $rootFolder,
                                RoomCircleMapper $roomCircleMapper,
                                IUserSession $userSession,
                                MatrixFileCacheMapper $fileCacheMapper,
                                ILogger $logger,
                                Defaults $defaults,
                                Share\IManager $shareManager,
                                MatrixUserMapper $matrixUserMapper)
    {
        $user = $userSession->getUser();
        $this->userId = $user ? $user->getUID(): null;
        $this->rootFolder = $rootFolder;
        $this->roomCircleMapper = $roomCircleMapper;
        $this->helper = new Helper(\OC::$server->getConfig());
        $this->userSession = $userSession;
        $this->fileCacheMapper = $fileCacheMapper;
        $this->logger = $logger;
        $this->defaults = $defaults;
        $this->currentUser = $this->userSession->getUser();
        $this->shareManager = $shareManager;
        $this->userMapper = $matrixUserMapper;
    }


    public function register()
    {
        #Util::connectHook('OC_Filesystem', 'post_create', $this, 'postCreate');
        #Util::connectHook('OC_Filesystem', 'post_write', $this, 'postWrite');


        /** Sharing / unsharing files */
        #Util::connectHook('OCP\Share', 'post_shared', $this, 'postShare');
        #Util::connectHook('OCP\Share', 'pre_unshare', $this, 'preUnshare');
        #Util::connectHook('OCP\Share', 'post_unshare', $this, 'postUnshare');
        #Util::connectHook('OCP\Share', 'post_update_permissions', $this, 'postUpdateSharePermissions');
    }

    public static function onLoadFilesAppScripts()
    {
        Util::addStyle('matrixbridge', 'style');
        Util::addScript('matrixbridge', 'chat-sidebar');
    }

    public function postCreate($params)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            $log_message = "PATH={$params['path']} : postCreate";

            $node = $this->getNodeFromPath($params['path']);
            //if (!$node || $node->getType() != FileInfo::TYPE_FOLDER) return;
            if (!$node) {
                MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "PATH={$params['path']} : ERROR postCreate Hook - node not found");
                return;
            }

            $shares = $this->collectSharedCircles($node);

            if ($shares) {
                $this->currentUser = $this->userSession->getUser();
            }

            $circleUniqueIds = array();
            foreach ($shares as $shareObject) {
                $share = $shareObject[0];
                $relativePath = $shareObject[1];
                $circleUniqueId = $share->getSharedWith();
                if (in_array($circleUniqueId, $circleUniqueIds)) {
                    continue;
                }
                $circleUniqueIds[] = $circleUniqueId;
                /** @var RoomCircle $roomCircle */
                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
                if ($roomCircle) {
                    $fileName = (($relativePath) ? ($relativePath . '/') : '') . $node->getName();
                    if ($node->getType() == FileInfo::TYPE_FILE) {
                        $bigVideo = (explode('/', $node->getMimetype())[0] === 'video' and $node->getSize() > 10 * 1024 * 1024);

                        if ($node->getMimetype() === "application/x-ownpad") {
                            $message = "_*{$this->currentUser->getDisplayName()}* criou o documento *{$fileName}*: " . $this->makePermalink($node) . "._";
                            $formattedBody = "<em><strong>{$this->currentUser->getDisplayName()}</strong> criou o documento <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";
                        } elseif (in_array($tmpType = explode('/', $node->getMimetype())[0], ["image", "audio", "video"]) and !$bigVideo) {
                            switch ($tmpType) {
                                case 'image':
                                    $tmpTypeText = "uma imagem";
                                    break;
                                case 'audio':
                                    $tmpTypeText = "um áudio";
                                    break;
                                case 'video':
                                    $tmpTypeText = "um vídeo";
                                    break;
                            }

                            $message = "☝🏾 _*{$this->currentUser->getDisplayName()}* enviou {$tmpTypeText}: " . $this->makePermalink($node) . "._";
                            $formattedBody = "☝🏾 <em><strong>{$this->currentUser->getDisplayName()}</strong> enviou <a href='" . $this->makePermalink($node) . "'><strong>{$tmpTypeText}</strong></a>.</em>";
                            $this->uploadFileToRoom($roomCircle, $node, false, $params['path']);
                        } else {
                            $what = $bigVideo ? 'o vídeo' : 'o arquivo';
                            $message = "_*{$this->currentUser->getDisplayName()}* enviou {$what} *{$fileName}*: " . $this->makePermalink($node) . "._";
                            $formattedBody = "<em><strong>{$this->currentUser->getDisplayName()}</strong> enviou {$what} <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";
                        }
                    } else {
                        $message = '';
                    }

                    $format = "org.matrix.custom.html";
                    if ($message) {
                        $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                    }
                    $this->justRewrote[$params['path']] = true;
                }
            }

            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', $log_message);
        } else {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "PATH={$params['path']} : IGNORED postCreate Hook because it is from AS");
        }
    }

    public function postWrite($params)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            $log_message = "PATH={$params['path']} : postWrite";

            $this->currentUser = $this->userSession->getUser();

            $node = $this->getNodeFromPath($params['path']);
            if ($node->getType() == FileInfo::TYPE_FILE and $node->getMimetype() !== "application/x-ownpad" and !array_key_exists($params['path'], $this->justRewrote)) {
                $shares = $this->collectSharedCircles($node);

                $circleUniqueIds = array();
                foreach ($shares as $shareObject) {
                    $share = $shareObject[0];
                    $relativePath = $shareObject[1];
                    $circleUniqueId = $share->getSharedWith();
                    if (in_array($circleUniqueId, $circleUniqueIds)) {
                        continue;
                    }
                    $circleUniqueIds[] = $circleUniqueId;
                    /** @var RoomCircle $roomCircle */
                    $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
                    if ($roomCircle) {
                        $fileName = (($relativePath) ? ($relativePath . '/') : '') . $node->getName();

                        $message = "_*{$this->currentUser->getDisplayName()}* atualizou o arquivo *{$fileName}*: " . $this->makePermalink($node) . "._";
                        $formattedBody = "<em><strong>{$this->currentUser->getDisplayName()}</strong> atualizou o arquivo <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";

                        $format = "org.matrix.custom.html";
                        $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                        $this->justRewrote[$params['path']] = true;
                    }
                }
            }
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', $log_message);
        } else {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "PATH={$params['path']} : IGNORED postWrite Hook because it is from AS");
        }
    }

    public function postShare(array $params)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            $log_message = "SOURCE={$params['fileSource']}, TARGET={$params['itemTarget']}, SHAREWITH={$params['shareWith']} : postShare";

            if ($params['shareType'] === Share::SHARE_TYPE_CIRCLE) {
                //Search for room associated with circle
                $circleUniqueId = $params['shareWith'];

                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
                if ($roomCircle) {
                    $nodes = $this->rootFolder->getById($params['fileSource']);
                    $node = $nodes[0];
                    $this->currentUser = $this->userSession->getUser();
                    $fileName = str_replace('/', '', $params['itemTarget']);
                    if ($node->getType() == FileInfo::TYPE_FILE) {
                        if ($node->getMimetype() === "application/x-ownpad") {
                            $message = "_*{$this->currentUser->getDisplayName()}* compartilhou o documento *{$fileName}*: " . $this->makePermalink($node) . "._";
                            $formattedBody = "<em><strong>{$this->currentUser->getDisplayName()}</strong> compartilhou o documento <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";
                        } elseif (in_array($tmpType = explode('/', $node->getMimetype())[0], ["image", "audio", "video"])) {
                            switch ($tmpType) {
                                case 'image':
                                    $tmpTypeText = "uma imagem";
                                    break;
                                case 'audio':
                                    $tmpTypeText = "um áudio";
                                    break;
                                case 'video':
                                    $tmpTypeText = "um vídeo";
                                    break;
                            }

                            $message = "☝🏾 _*{$this->currentUser->getDisplayName()}* compartilhou {$tmpTypeText}: " . $this->makePermalink($node) . "._";
                            $formattedBody = "☝🏾 <em><strong>{$this->currentUser->getDisplayName()}</strong> compartilhou <a href='" . $this->makePermalink($node) . "'><strong>{$tmpTypeText}</strong></a>.</em>";
                            $unshareOnRedact = (0 === strpos($node->getMimetype(), "image"));
                            $this->uploadFileToRoom($roomCircle, $node, $unshareOnRedact, $params['path']);
                        } else {
                            $message = "_*{$this->currentUser->getDisplayName()}* compartilhou com esta sala o arquivo *{$fileName}*: " . $this->makePermalink($node) . "._";
                            $formattedBody = "<em><strong>{$this->currentUser->getDisplayName()}</strong> compartilhou com esta sala o arquivo <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";
                        }
                    } else {
                        $message = "_*{$this->currentUser->getDisplayName()}* compartilhou com esta sala a pasta *{$fileName}*: " . $this->makePermalink($node) . "._";
                        $formattedBody = "<strong>{$this->currentUser->getDisplayName()}</strong> compartilhou com esta sala a pasta <a href='" . $this->makePermalink($node) . "'><strong>{$fileName}</strong></a>.</em>";
                        $this->updateRoomFolders($circleUniqueId);
                    }

                    $format = "org.matrix.custom.html";
                    $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                }
            }
        } else {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "SOURCE={$params['fileSource']}, TARGET={$params['itemTarget']}, SHAREWITH={$params['shareWith']} : IGNORED postShare Hook because it is from AS");
        }
    }

    public function preUnshare(array $params)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "SHAREID={$params['id']} : preUnshare");

            if ($params['shareType'] === Share::SHARE_TYPE_CIRCLE) {
                $shareManager = \OC::$server->getShareManager();
                #ocinternal
                $share = $shareManager->getShareById("ocCircleShare:" . $params['id']);
                if ($share) {
                    $this->sharedWith[$params['id']] = $share->getSharedWith();
                }
            }
        } else {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "SHAREID={$params['id']} : IGNORED preUnshare Hook because it is from AS");
        }
    }

    public function postUnshare(array $params)
    {
        if (!MatrixListener::$AS_TRANSACTION) {
            $log_message = "PATH={$params['path']} : postUnshare";

            if ($params['shareType'] === Share::SHARE_TYPE_CIRCLE) {
                //Search for room associated with circle

                $circleUniqueId = $this->sharedWith[$params['id']];

                $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
                if ($roomCircle) {
                    $this->currentUser = $this->userSession->getUser();
                    $nodes = $this->rootFolder->getById($params['fileSource']);
                    $node = $nodes[0];
                    if ($node->getType() == FileInfo::TYPE_FOLDER) {
                        $this->updateRoomFolders($circleUniqueId);
                        $fileName = basename($params['fileTarget']);

                        $message = "_*{$this->currentUser->getDisplayName()}* desfez o compartilhamento da pasta *{$fileName}* com esta sala._";
                        $formattedBody = "<strong>{$this->currentUser->getDisplayName()}</strong> desfez o compartilhamento da pasta <strong>{$fileName}</strong> com esta sala.</em>";
                        $format = "org.matrix.custom.html";
                        $this->sendMessageToRoom($circleUniqueId, $message, $format, $formattedBody);
                    }
                }
            }
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', $log_message);
        } else {
            MatrixUtils::matrix_exchange_log($this->userId, 'HOOK', "SOURCE={$params['fileSource']}, TARGET={$params['itemTarget']}, SHAREWITH={$params['shareWith']} : IGNORED postUnshare Hook because it is from AS");
        }
    }

    public function postUpdateSharePermissions($params)
    {
        // TODO postUpdateSharePermissions
    }

    /**
     * @param $uid
     * @return MatrixOrg_API
     */
    private function getMatrixApiForUser($uid)
    {
        if (isset($this->matrixApis[$uid]) && $this->matrixApis[$uid]) {
            return $this->matrixApis[$uid];
        }

        try {
            $homeserverUrl = $this->helper->getHomeserverUrl();
            $matrixUser = $this->userMapper->findByUidAndHomeServer($uid, $homeserverUrl);

            if ($matrixUser) {
                $matrixToken = $matrixUser->getMatrixTokenDecrypted();
                if ($matrixToken) {
                    $this->matrixApis[$uid] = new MatrixOrg_API($matrixUser->getHomeServerUrl(), $matrixToken);
                    $this->matrixApis[$uid]->setUserId($matrixUser->getMatrixUsername());
                    return $this->matrixApis[$uid];
                }
            }

        } catch (DoesNotExistException $exp) {

        }

        return null;
    }

    /**
     * Sends file to room. CurrentUser is the sender.
     *
     * @param RoomCircle $roomCircle
     * @param Node $node
     * @param bool $unshareOnRedact
     * @return bool
     * @throws NotFoundException
     * @throws \OCP\Files\InvalidPathException
     */
    private function uploadFileToRoom(RoomCircle $roomCircle, Node $node, $unshareOnRedact = false, $path = '')
    {
        $nodeId = $node->getId();
        $roomId = $roomCircle->getMxRoomId();
        if (array_key_exists($nodeId, $this->uploaded) and array_key_exists($roomId, $this->uploaded[$nodeId])) {
            return true;
        }

        #check if file already exists in room
        $oldFileCache = $this->fileCacheMapper->findByFileidAndRoomId($nodeId, $roomId);

        $uid = $this->userId;
        if (!$uid) {
            return false;
        }
        $api = $this->getMatrixApiForUser($uid);
        if ($api) {
            $internalPath = $this->getInternalPath($path, $node);
            $name = $node->getName();
            #$this->justCreated = true;
            $res = $api->sendFile($roomCircle->getMxRoomId(), $internalPath, $name);
            if ($res['status'] == 200) {
                $this->fileCacheMapper->createOrUpdate($node->getId(), $res['event_id'], $res['url'], $node->getOwner()->getUID(), $roomCircle->getMxRoomId(), $node->getName(), $unshareOnRedact);
                $this->uploaded[$nodeId][$roomId] = true;

                if ($oldFileCache) {
                    $res2 = $api->redact($oldFileCache->getMxRoomId(), $oldFileCache->getEventId());
                }

                return true;
            }
        }
        return false;
    }

    private function makePermalink(Node $node)
    {
        return \OC::$server->getURLGenerator()->getAbsoluteURL('index.php/f/' . $node->getId());
    }

    private function sendMessageToRoom($circleUniqueId, $message, $format = "", $formattedBody = "")
    {
        $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);
        $res = null;
        if ($roomCircle && $this->currentUser) {
            $api = $this->getMatrixApiForUser($this->currentUser->getUID());
            if ($api) {
                $res = $api->sendMessage($roomCircle->getMxRoomId(), "$message", $format, $formattedBody);
            }
        }
        return $res && $res['status'] == 200;
    }

    /**
     * Updates in matrix metadata the current list of folders that the matrix
     *   user is allowed to choose to upload a file to.
     *
     * @param $circleUniqueId
     */
    private function updateRoomFolders($circleUniqueId)
    {
        /* Method \OCA\Circles\Api\v1\Circles::getFilesForCircles became deprecated;  */
        return;

        $roomCircle = $this->roomCircleMapper->findByCircleUniqueId($circleUniqueId);

        if ($roomCircle) {
            //Gets list of folders
            $fileIds = \OCA\Circles\Api\v1\Circles::getFilesForCircles(array($circleUniqueId));

            $userFolder = \OC::$server->getUserFolder($this->userId);
            $folderList = array();

            if ($userFolder != null) {
                foreach ($fileIds as $fileId) {
                    $entry = $userFolder->getById($fileId);
                    if ($entry) {
                        $entry = current($entry);
                        if ($entry instanceof \OCP\Files\Folder and $entry->isCreatable()) {
                            $folderList[] = array(
                                'id' => $entry->getId(),
                                'name' => $entry->getName()
                            );
                        }
                    }
                }
            }

            //Sends message
            $api = $this->getMatrixApiForUser($this->userId);
            $api->setState($roomCircle->getMxRoomId(), "m.room.rios.destination_folders", json_encode(array("destinationFolders" => $folderList)));
        }
    }

    /**
     * @param $path
     * @return Node
     */
    private function getNodeFromPath($path)
    {
        $userFolder = \OC::$server->getUserFolder($this->userId);
        return $userFolder->get($path);
    }

    /**
     * The circles that this node is shared to.
     *
     * @param Node $node
     * @return Share\IShare[]
     */
    private function collectSharedCircles(Node $node)
    {
        $shareManager = \OC::$server->getShareManager();

        //Does not use $this->userId, because in some cases this is set in userSession by MatrixListener
        $uid = null;
        $user = $this->userSession->getUser();
        if ($user) {
            $uid = $user->getUID();
        }

        $nextNode = $node;
        $relativePath = "";
        do {
            $currentNode = $nextNode;

            if ($currentNode != $node) {
                $relativePath = $currentNode->getName() . '/' . $relativePath;
            }

            $newShares = $shareManager->getSharedWith($uid, \OCP\Share\IShare::TYPE_CIRCLE, $currentNode);
            $parentFolder = $nextNode = $currentNode->getParent();

        } while (!$newShares && !empty($parentFolder->getPath()) and $parentFolder->getPath() !== '/');

        $relativePath = preg_replace('/\/$/','',$relativePath);

        $shares = array();

        foreach ($newShares as $newShare) {
            if (!isset($shares[$newShare->getId()])) {
                $shares[$newShare->getId()] = array($newShare, $relativePath);
            }
        }

        return $shares;
    }


    private function getFilesFromFolder(Node $node)
    {
        $result = array();
        if (empty($node)) {
            return $result;
        } else if ($node->getType() == FileInfo::TYPE_FOLDER) {
            /** @var Folder $folder */
            $folder = $node;
            try {
                foreach ($folder->getDirectoryListing() as $child) {
                    $result = array_merge($result, $this->getFilesFromFolder($child));
                }
            } catch (NotFoundException $exception) {
                return $result;
            }
        } else {
            $result = array($node);
        }
        return $result;
    }

    /**
     * Return the source
     *
     * @param string $path
     * @return array
     */
    protected function getSourcePathAndOwner($path)
    {
        $view = Filesystem::getView();
        $owner = $view->getOwner($path);
        $owner = !is_string($owner) || $owner === '' ? null : $owner;
        $fileId = 0;
        $currentUser = $this->currentUser->getUID();

        if ($owner === null || $owner !== $currentUser) {
            /** @var \OCP\Files\Storage\IStorage $storage */
            list($storage,) = $view->resolvePath($path);

            if ($owner !== null && !$storage->instanceOfStorage('OCA\Files_Sharing\External\Storage')) {
                Filesystem::initMountPoints($owner);
            } else {
                // Probably a remote user, let's try to at least generate activities
                // for the current user
                if ($currentUser === null) {
                    list(, $owner,) = explode('/', $view->getAbsolutePath($path), 3);
                } else {
                    $owner = $currentUser;
                }
            }
        }

        $info = Filesystem::getFileInfo($path);
        if ($info !== false) {
            $ownerView = new View('/' . $owner . '/files');
            $fileId = (int)$info['fileid'];
            $path = $ownerView->getPath($fileId);
        }

        return array($path, $owner, $fileId);
    }

    private function getInternalPath($path = '', $node = null)
    {
        if ($path) {
            list($oPath, $owner, $fileId) = $this->getSourcePathAndOwner($path);
            $internalPath = \OC::$server->getConfig()->getSystemValue('datadirectory', \OC::$SERVERROOT . '/data') . '/' . $owner . '/files' . $oPath;
        } else {
            $internalPath = \OC::$server->getConfig()->getSystemValue('datadirectory', \OC::$SERVERROOT . '/data') . $node->getPath('');
        }

        return $internalPath;
    }
}
