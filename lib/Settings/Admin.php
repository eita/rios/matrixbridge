<?php

namespace OCA\MatrixBridge\Settings;

use OCA\User_LDAP\Configuration;
use OCA\MatrixBridge\Helper;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IL10N;
use OCP\Settings\ISettings;
use OCP\Template;

class Admin implements ISettings {

	/**
	 * @return TemplateResponse returns the instance with all parameters set, ready to be rendered
	 * @since 9.1
	 */
	public function getForm() {
		$helper = new Helper(\OC::$server->getConfig());

		$parameters = array(
			'riotUrl'	              => $helper->getRiotUrl(),
            'groupRoomsOwnerName'     => $helper->getGroupRoomsOwnerName(),
            'groupRoomsOwnerPassword' => $helper->getGroupRoomsOwnerPassword(),
            'homeserverUrl'           => $helper->getHomeserverUrl(),
            'identifiersDomain'       => $helper->getIdentifiersDomain()
		);

		$expected = new TemplateResponse('matrixbridge', 'settings', $parameters);
		return $expected;
	}

	/**
	 * @return string the section ID, e.g. 'sharing'
	 * @since 9.1
	 */
	public function getSection() {
		return "matrix_org";
	}

	/**
	 * @return int whether the form should be rather on the top or bottom of
	 * the admin section. The forms are arranged in ascending order of the
	 * priority values. It is required to return a value between 0 and 100.
	 *
	 * E.g.: 70
	 * @since 9.1
	 */
	public function getPriority() {
		return 6;
	}
}