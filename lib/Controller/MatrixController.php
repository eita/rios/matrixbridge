<?php
namespace OCA\MatrixBridge\Controller;

use OC\Files\View;
use OCA\MatrixBridge\Db\MatrixUser;
use OCA\MatrixBridge\Db\MatrixUserMapper;
use OCA\MatrixBridge\Db\RoomCircle;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use OCP\AppFramework\Http\JSONResponse;
use OCP\Files\Node;
use OCP\IConfig;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IPreview;

use OCP\AppFramework\Controller;
use \OCA\MatrixBridge\Service\MatrixListener;
use OCP\IUserManager;
use OCP\IUserSession;

class MatrixController extends Controller {
	private $userId;
	private $matrixListener;
	private $logger;
	private $roomCircleMapper;
	private $userSession;
	private $userManager;
	private $matrixUserMapper;
    private $config;

	public function __construct($AppName,
                                IRequest $request,
                                $UserId,
                                MatrixListener $matrixListener,
                                ILogger $logger,
                                RoomCircleMapper $roomCircleMapper,
                                IUserSession $userSession,
                                IUserManager $userManager,
                                MatrixUserMapper $matrixUserMapper,
                                IConfig $config){
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
		$this->matrixListener = $matrixListener;
		$this->logger = $logger;
		$this->roomCircleMapper = $roomCircleMapper;
		$this->userSession = $userSession;
		$this->userManager = $userManager;
		$this->matrixUserMapper = $matrixUserMapper;
        $this->config = $config;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
     * @PublicPage
	 */
	public function rooms($params) {
        //$this->logger->debug("MatrixController rooms");
        return $this->matrixListener->roomsHandler();
		//return new TemplateResponse('matrixbridge', 'matrix');
        //return $this->matrixListener->testFileCreation();
	}

	/**
   * @NoAdminRequired
   * @NoCSRFRequired
   * @PublicPage
   */
	public function transactions(string $access_token) {
        //$this->logger->debug("MatrixController transactions");
        if ($access_token && $this->config->getSystemValue('matrix_hs_token','') === $access_token) {
            return $this->matrixListener->transactionsHandler();
        }
        return false;
	}

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     */
	public function users($params) {
        //$this->logger->debug("MatrixController users");

        return $this->matrixListener->usersHandler();
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 */
	public function roomSharedFolders($room_id, $user_id=false, $include_readonly=false) {
		//Returns a list of shared folders to the current room

		if (!$room_id) return new JSONResponse(array("error" => "room_id must be sent"));

		try {
			$nc_user_id = null;
			if ($this->userId) {
				$nc_user_id = $this->userId;
			} else {
				/** @var MatrixUser $matrixUser */
				try {
					$matrixUser = $this->matrixUserMapper->findByMxUsername($user_id);
					$user = $this->userManager->get($matrixUser->getUsername());
					if ($user) {
						$nc_user_id = $user->getUID();
						$this->userSession->setUser($user);
					} else {
						return new JSONResponse(array("error" => "could not find user"));
					}
				} catch (\Exception $e) {
					return new JSONResponse(array("error" => "could not find user"));
				}

			}

			/** @var RoomCircle $roomCircle */
			$roomCircle = $this->roomCircleMapper->findByMxRoomId($room_id);

			if ($roomCircle) {
				if (!\OC::$server->getAppManager()->isEnabledForUser('circles') || !class_exists('\OCA\Circles\ShareByCircleProvider')) {
					return array();
				}
				$fileIds = \OCA\Circles\Api\v1\Circles::getFilesForCircles(array($roomCircle->getCircleUniqueId()));

				$userFolder = \OC::$server->getUserFolder($nc_user_id);
				$results = array();

				foreach ($fileIds as $fileId) {
					$entry = $userFolder->getById($fileId);
					if ($entry) {
						$entry = current($entry);
						if ($entry instanceof \OCP\Files\Folder and $entry->isCreatable()) {
							$directoryListing = $entry->getDirectoryListing();
							$i = 1;
							$results[] = array(
								'id' => $entry->getId(),
								'name' => $entry->getName(),
								'children' => $this->getSubTree($entry)
							);
						}
					}
				}


				//$rootNode = $reportTargetNode = $this->server->tree->getNodeForPath("");

				return new JSONResponse($results);
			}
		} catch (\Exception $e) {

		}


		return new JSONResponse(array("error" => "could not find room"));


		//return new TemplateResponse('matrixbridge', 'matrix');
		//return $this->matrixListener->testFileCreation();
	}

	private function getSubTree(Node $node,$type='folder',$limit=0) {
		try {
			$query = \OC_DB::prepare(
			'SELECT `fileid`, `parent`, `path`
					FROM `*PREFIX*filecache`
					WHERE `mimetype` = 2 AND `path` LIKE \'%'.basename($node->getFullPath('')).'%\''
			);
			$result = $query->execute();
			if ($result === false) {
				\OCP\Util::writeLog('core', 'error building file tree: ' .\OC_DB::getErrorMessage(), \OCP\Util::ERROR);
			} else {
				$parents = array();
				$nodes = array();

				while ($row = $result->fetchRow()) {
					$nodes[(int)$row['fileid']] = $row['path'];
					$parents[(int)$row['fileid']] = (int)$row['parent'];
				}
				$result->closeCursor();
			}
		} catch (\OC\DatabaseException $exp) {
			return array();
		}

		return $this->buildSubTree($node->getId(), $parents, $nodes);

	}

	private function buildSubTree($parentId, &$parents, &$nodes) {
		$children = array();
		foreach ($parents as $child => $parent) {
			if ($parent == $parentId) {
				$children[] = array(
					'id' => $child,
					'name' => basename($nodes[$child]),
					'children' => $this->buildSubTree($child,$parents,$nodes)
				);
			}
		}
		return $children;
	}

}
