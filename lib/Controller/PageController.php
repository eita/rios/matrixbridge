<?php

namespace OCA\MatrixBridge\Controller;

use OCA\MatrixBridge\AppInfo\Application;
use OCA\MatrixBridge\Helper;
use OCA\MatrixBridge\Service\MatrixChatContentSecurityPolicy;
use OCA\MatrixBridge\Service\MatrixConnectionManager;
use OCA\MatrixBridge\Service\MatrixUtils;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\ILogger;
use OCP\IRequest;
use OCP\ISession;
use OCP\Security\ICrypto;
use OCP\Util;

class PageController extends Controller
{
    private $userId;
    private $connManager;
    private $currentUser;
    private $crypto;
    private $session;
    private $helper;
    private $matrixUtils;
    private $logger;

    public function __construct($AppName,
                                IRequest $request,
                                MatrixConnectionManager $matrixConnectionManager,
                                ICrypto $crypto,
                                ISession $session,
                                MatrixUtils $utils,
                                ILogger $logger)
    {
        parent::__construct($AppName, $request);
        $this->userId = $session->get('user_id');
        $this->connManager = $matrixConnectionManager;
        $this->crypto = $crypto;
        $this->session = $session;
        $this->helper = new Helper(\OC::$server->getConfig());
        $this->matrixUtils = $utils;
        $this->logger = $logger;
    }

    /**
     * CAUTION: the @Stuff turns off security checks; for this page no admin is
     *          required and no CSRF check. If you don't know what CSRF is, read
     *          it up in the docs or you might create a security hole. This is
     *          basically the only required method to add this exemption, don't
     *          add it to any other method if you don't exactly know what it does
     *
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function index()
    {
        Util::addScript(Application::APP_ID, 'matrixbridge-main');
        //Util::addScript(Application::APP_ID, 'badge');

        $element_url = $this->helper->getRiotUrl();
        $homeserver_url = $this->helper->getHomeserverUrl();

        $response = new TemplateResponse('matrixbridge', 'main', array('element_url' => $element_url, 'homeserver_url' => $homeserver_url));
        //$response = new TemplateResponse('matrixbridge', 'index', array('element_url' => $element_url, 'riot_url' => $element_url, 'homeserver_url' => $homeserver_url));

        $csp = new MatrixChatContentSecurityPolicy();
        $csp->addAllowedConnectDomain($element_url);
        $csp->addAllowedConnectDomain($homeserver_url);

        $response->setContentSecurityPolicy($csp);
        return $response;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function logout()
    {
        $riot_url = $this->helper->getRiotUrl();

        $response = new TemplateResponse('matrixbridge', 'logout', array('riot_url' => $riot_url));  // templates/logout.php
        $response->setContentSecurityPolicy(new MatrixChatContentSecurityPolicy());
        return $response;
    }

    /**
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function matrixCredentials($param)
    {
        $homeserver_url = filter_var($this->helper->getHomeserverUrl(), FILTER_VALIDATE_URL);
        $conn_info = $this->connManager->getConnectionInfo($this->userId, $homeserver_url);
        return new JSONResponse($conn_info);
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function testing()
    {
        $result = array();
        return new JSONResponse($result);
    }

}
