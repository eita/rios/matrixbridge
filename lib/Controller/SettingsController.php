<?php

namespace OCA\MatrixBridge\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCA\MatrixBridge\Helper;

class SettingsController extends Controller
{

    private $helper;

    public function __construct()
    {
        $this->helper = new Helper(\OC::$server->getConfig());
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function updateSetting($setting, $value)
    {

        $i = 1;
        switch ($setting) {
            case 'matrixbridge-riot-url':
                $this->helper->setRiotUrl($value);
                break;
            case 'matrixbridge-group-rooms-owner':
                $this->helper->setGroupRoomsOwnerName($value);
                break;
            case 'matrixbridge-group-rooms-owner-pw':
                $this->helper->setGroupRoomsOwnerPassword($value);
                break;
            case 'matrixbridge-homeserver-url':
                $this->helper->setHomeserverUrl($value);
                break;
            case 'matrixbridge-identifiers-domain':
                $this->helper->setIdentifiersDomain($value);
                break;
        }

        return new JSONResponse(
            [
                'data' =>
                    [
                        'message' => "TESTE SAVE SETTING: ".$setting
                    ],
                'status' => 'success'
            ]
        );
    }
}
