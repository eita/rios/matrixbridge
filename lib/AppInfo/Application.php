<?php
/**
 * Matrix.org external storage backend
 *
 * @copyright 2016 Cooperativa EITA (eita.org.br)
 *
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 * @author Daniel Tygel <dtygel@eita.org.br>
 * @author Alan Freihof Tygel <alan@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace OCA\MatrixBridge\AppInfo;

use OC\AllConfig;
use OC\Security\CSP\ContentSecurityPolicyManager;
use OCA\Circles\Events\AddingCircleMemberEvent;
use OCA\Circles\Events\DestroyingCircleEvent;
use OCA\Circles\Events\EditingCircleEvent;
use OCA\Circles\Events\EditingCircleMemberEvent;
use OCA\Circles\Events\RemovingCircleMemberEvent;
use OCA\Circles\Events\RequestingCircleMemberEvent;
use OCA\DAV\Events\CalendarMovedToTrashEvent;
use OCA\DAV\Events\CalendarObjectCreatedEvent;
use OCA\DAV\Events\CalendarObjectMovedToTrashEvent;
use OCA\DAV\Events\CalendarObjectRestoredEvent;
use OCA\DAV\Events\CalendarObjectUpdatedEvent;
use OCA\DAV\Events\CalendarRestoredEvent;
use OCA\DAV\Events\CalendarShareUpdatedEvent;
use OCA\DAV\Events\CalendarUpdatedEvent;
use OCA\MatrixBridge\FilesHooks;
use OCA\MatrixBridge\Helper;
use OCA\MatrixBridge\Listeners\Calendar\CalendarMovedToTrashListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarObjectCreatedListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarObjectMovedToTrashListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarObjectRestoredListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarObjectUpdatedListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarRestoredListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarShareUpdatedListener;
use OCA\MatrixBridge\Listeners\Calendar\CalendarUpdatedListener;
use OCA\MatrixBridge\Listeners\Circles\AddingCircleMemberListener;
use OCA\MatrixBridge\Listeners\Circles\DestroyingCircleListener;
use OCA\MatrixBridge\Listeners\Circles\EditingCircleListener;
use OCA\MatrixBridge\Listeners\Circles\EditingCircleMemberListener;
use OCA\MatrixBridge\Listeners\Circles\RemovingCircleMemberListener;
use OCA\MatrixBridge\Listeners\Circles\RequestingCircleMemberListener;
use OCA\MatrixBridge\Listeners\Deck\AclCreatedListener;
use OCA\MatrixBridge\Listeners\Deck\AclDeletedListener;
use OCA\MatrixBridge\Listeners\Deck\CardCreatedListener;
use OCP\App\IAppManager;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;

class Application extends App  implements IBootstrap {
    public const APP_ID = 'matrixbridge';

    public function __construct(array $params = []) {
		parent::__construct(self::APP_ID, $params);
	}

	public function register(IRegistrationContext $context) : void {
        $this->addMatrixHeaders();
        $this->registerHooksAndEvents($context);
        //$context->registerDashboardWidget(MatrixWidget::class);
    }

	public function registerHooksAndEvents(IRegistrationContext $context) {
		$container = $this->getContainer();

		$eventDispatcher = $this->getContainer()->getServer()->getEventDispatcher();
		$eventDispatcher->addListener('OCA\Files::loadAdditionalScripts', [FilesHooks::class, 'onLoadFilesAppScripts']);

		$container->get(\OCA\MatrixBridge\UserHooks::class)->register();
		$container->get(\OCA\MatrixBridge\GroupHooks::class)->register();
		$container->get(\OCA\MatrixBridge\FilesHooks::class)->register();

        $this->registerCircleEventListeners($context);
        $this->registerCalendarEventListeners($context);

        $apps = \OC::$server->get(IAppManager::class);
        if ($apps->isInstalled('deck')) {
            $this->registerDeckEventListeners($context);
        }
    }

    public function boot(IBootContext $context): void
    {
    }

    private function addMatrixHeaders() {
        if(class_exists('\\OCP\\AppFramework\\Http\\EmptyContentSecurityPolicy')) {
            $helper = new Helper(\OC::$server->get(AllConfig::class));
            $manager = \OC::$server->get(ContentSecurityPolicyManager::class);
            $policy = new \OCP\AppFramework\Http\EmptyContentSecurityPolicy();
            $homeserverUrl = $helper->getHomeServerUrl();
            $policy->addAllowedConnectDomain($homeserverUrl);
            $policy->addAllowedScriptDomain($homeserverUrl);
            $policy->addAllowedChildSrcDomain($homeserverUrl);
            $manager->addDefaultPolicy($policy);
/*
            $linkToJs = \OC::$server->getURLGenerator()->linkTo('matrixbridge','js/badge.js') . "?v=1";

            \OCP\Util::addHeader(
                'script',
                [
                    'src' => $linkToJs,
                    'nonce' => \OC::$server->getContentSecurityPolicyNonceManager()->getNonce()
                ], ''
            );
*/

        }
    }

    private function registerCircleEventListeners(IRegistrationContext $context)
    {
        $context->registerEventListener(AddingCircleMemberEvent::class, AddingCircleMemberListener::class);
        $context->registerEventListener(DestroyingCircleEvent::class, DestroyingCircleListener::class);
        $context->registerEventListener(EditingCircleEvent::class, EditingCircleListener::class);
        $context->registerEventListener(EditingCircleMemberEvent::class, EditingCircleMemberListener::class);
        $context->registerEventListener(RemovingCircleMemberEvent::class, RemovingCircleMemberListener::class);
        $context->registerEventListener(RequestingCircleMemberEvent::class, RequestingCircleMemberListener::class);
    }

    private function registerCalendarEventListeners(IRegistrationContext $context)
    {
        $context->registerEventListener(CalendarUpdatedEvent::class, CalendarUpdatedListener::class);
        $context->registerEventListener(CalendarMovedToTrashEvent::class, CalendarMovedToTrashListener::class);
        $context->registerEventListener(CalendarRestoredEvent::class, CalendarRestoredListener::class);
        $context->registerEventListener(CalendarObjectCreatedEvent::class, CalendarObjectCreatedListener::class);
        $context->registerEventListener(CalendarObjectUpdatedEvent::class, CalendarObjectUpdatedListener::class);
        $context->registerEventListener(CalendarObjectMovedToTrashEvent::class, CalendarObjectMovedToTrashListener::class);
        $context->registerEventListener(CalendarObjectRestoredEvent::class, CalendarObjectRestoredListener::class);
        $context->registerEventListener(CalendarShareUpdatedEvent::class, CalendarShareUpdatedListener::class);
    }

    private function registerDeckEventListeners(IRegistrationContext $context)
    {
        $context->registerEventListener(\OCA\Deck\Event\CardCreatedEvent::class, CardCreatedListener::class);
        //$context->registerEventListener(\OCA\Deck\Event\CardUpdatedEvent::class, CardUpdatedListener::class);
        //$context->registerEventListener(\OCA\Deck\Event\CardDeletedEvent::class, CardDeletedListener::class);
        $context->registerEventListener(\OCA\Deck\Event\AclCreatedEvent::class, AclCreatedListener::class);
        //$context->registerEventListener(\OCA\Deck\Event\AclUpdatedEvent::class, AclUpdatedListener::class);
        $context->registerEventListener(\OCA\Deck\Event\AclDeletedEvent::class, AclDeletedListener::class);


    }
}
