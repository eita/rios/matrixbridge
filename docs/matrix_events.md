MATRIX APPLICATION SERVICE API
==============================

EVENT TYPES
-----------

* Member = "m.room.member"
* Create = "m.room.create"
* JoinRules = "m.room.join_rules"
* PowerLevels = "m.room.power_levels"
* Aliases = "m.room.aliases"
* Redaction = "m.room.redaction"
* ThirdPartyInvite = "m.room.third_party_invite"

* RoomHistoryVisibility = "m.room.history_visibility"
* CanonicalAlias = "m.room.canonical_alias"
* RoomAvatar = "m.room.avatar"
* GuestAccess = "m.room.guest_access"

These are used for validation
* Message = "m.room.message"
* Topic = "m.room.topic"
* Name = "m.room.name"


CREATE ROOM
-----------

Several events are chained. these are:

1. m.room.create
2. m.room.member
3. m.room.power_levels
4. m.room.canonical_alias
5. m.room.join_rules
6. m.room.history_visibility
7. m.room.aliases

```javascript
//OK!
{
    "events": [{
        "age": 400,
        "content": {
            "creator": "@viniciuscb:localhost"
        },
        "event_id": "$14794904472eYQUr:localhost",
        "origin_server_ts": 1479490447211,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "",
        "type": "m.room.create",
        "unsigned": {
            "age": 400
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 734,
        "content": {
            "avatar_url": null,
            "displayname": "Vinicius",
            "membership": "join"
        },
        "event_id": "$14794904473XyJZC:localhost",
        "membership": "join",
        "origin_server_ts": 1479490447392,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "@viniciuscb:localhost",
        "type": "m.room.member",
        "unsigned": {
            "age": 734
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 634,
        "content": {
            "ban": 50,
            "events": {
                "m.room.avatar": 50,
                "m.room.canonical_alias": 50,
                "m.room.history_visibility": 100,
                "m.room.name": 50,
                "m.room.power_levels": 100
            },
            "events_default": 0,
            "invite": 0,
            "kick": 50,
            "redact": 50,
            "state_default": 50,
            "users": {
                "@viniciuscb:localhost": 100
            },
            "users_default": 0
        },
        "event_id": "$14794904484EkEBP:localhost",
        "origin_server_ts": 1479490448123,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "",
        "type": "m.room.power_levels",
        "unsigned": {
            "age": 634
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 865,
        "content": {
            "alias": "#publicRoom:localhost"
        },
        "event_id": "$14794904485CJKSD:localhost",
        "origin_server_ts": 1479490448526,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "",
        "type": "m.room.canonical_alias",
        "unsigned": {
            "age": 865
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 805,
        "content": {
            "join_rule": "public"
        },
        "event_id": "$14794904496NxuPS:localhost",
        "origin_server_ts": 1479490449139,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "",
        "type": "m.room.join_rules",
        "unsigned": {
            "age": 805
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 849,
        "content": {
            "history_visibility": "shared"
        },
        "event_id": "$14794904497amAVU:localhost",
        "origin_server_ts": 1479490449618,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "",
        "type": "m.room.history_visibility",
        "unsigned": {
            "age": 849
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

{
    "events": [{
        "age": 821,
        "content": {
            "aliases": ["#publicRoom:localhost"]
        },
        "event_id": "$14794904508bIEoQ:localhost",
        "origin_server_ts": 1479490450186,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "localhost",
        "type": "m.room.aliases",
        "unsigned": {
            "age": 821
        },
        "user_id": "@viniciuscb:localhost"
    }]
}

```

RECEIVE MESSAGE
---------------

```javascript
{
    "events": [{
        "age": 410,
        "content": {
            "body": "esta é uma nova mensagem",
            "msgtype": "m.text"
        },
        "event_id": "$147949340412VtVkV:localhost",
        "origin_server_ts": 1479493404678,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "type": "m.room.message",
        "unsigned": {
            "age": 410
        },
        "user_id": "@viniciuscb:localhost"
    }]
}
```


RECEIVE FILE MESSAGE
--------------------

```javascript
{
    "events": [{
        "age": 455,
        "content": {
            "body": "Telegram.desktop",
            "info": {
                "mimetype": "application/x-desktop",
                "size": 105
            },
            "msgtype": "m.file",
            "url": "mxc://localhost/DCwWRUkdebGGjwpNpjbZCVAn"
        },
        "event_id": "$14794897611iTQnt:localhost",
        "origin_server_ts": 1479489761251,
        "room_id": "!tFUmzAzTSquTOGzmTs:localhost",
        "sender": "@viniciuscb:localhost",
        "type": "m.room.message",
        "unsigned": {
            "age": 455
        },
        "user_id": "@viniciuscb:localhost"
    }]
}
```

LEAVE ROOM
----------

```javascript
{
    "events": [{
        "age": 715,
        "content": {
            "membership": "leave"
        },
        "event_id": "$14794911049RhgYy:localhost",
        "membership": "leave",
        "origin_server_ts": 1479491104764,
        "prev_content": {
            "avatar_url": null,
            "displayname": "Vinicius",
            "membership": "join"
        },
        "replaces_state": "$14780955601hMvly:localhost",
        "room_id": "!tFUmzAzTSquTOGzmTs:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "@viniciuscb:localhost",
        "type": "m.room.member",
        "unsigned": {
            "age": 715,
            "prev_content": {
                "avatar_url": null,
                "displayname": "Vinicius",
                "membership": "join"
            },
            "prev_sender": "@viniciuscb:localhost",
            "replaces_state": "$14780955601hMvly:localhost"
        },
        "user_id": "@viniciuscb:localhost"
    }]
}
```

INVITE USER TO ROOM
-------------------

```javascript
{
    "events": [{
        "age": 394,
        "content": {
            "membership": "invite"
        },
        "event_id": "$147949327511aOWCw:localhost",
        "membership": "invite",
        "origin_server_ts": 1479493275882,
        "room_id": "!KQeqNgPSRxOsOEnMyK:localhost",
        "sender": "@viniciuscb:localhost",
        "state_key": "@dtygel:localhost",
        "type": "m.room.member",
        "unsigned": {
            "age": 394
        },
        "user_id": "@viniciuscb:localhost"
    }]
}
```

