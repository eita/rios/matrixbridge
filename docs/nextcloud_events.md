Events in Nextcloud
===================
(routes called and responses received)

Sharing
-------

* Some documentation here: https://docs.nextcloud.com/server/10/developer_manual/core/externalapi.html

#### share: (autocomplete) search for user to share
* Request URL: http://nextcloud.localhost/ocs/v1.php/apps/files_sharing/api/v1/sharees?format=json&search=next&perPage=200&itemType=folder
* Request Method: GET
* Response: 

```javascript
{
    "ocs": {
        "meta": {
            "status": "ok",
            "statuscode": 100,
            "message": null
        },
        "data": {
            "exact": {
                "users": [],
                "groups": [],
                "remotes": []
            },
            "users": [{
                "label": "nextcloud user",
                "value": {
                    "shareType": 0,
                    "shareWith": "58111ba0-45ff-1036-8ac4-17d116ce7630"
                }
            }, {
                "label": "nextcloud user 2",
                "value": {
                    "shareType": 0,
                    "shareWith": "82a82982-469d-1036-970b-db485d1925c0"
                }
            }, {
                "label": "nextcloud3",
                "value": {
                    "shareType": 0,
                    "shareWith": "nextcloud3"
                }
            }],
            "groups": [],
            "remotes": []
        }
    }
}
```

#### get informations about a specific shared folder
* Request URL: http://nextcloud.localhost/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json&path=%2Ftest&reshares=true
* Request Method: GET
* Response:

```javascript
{
    "ocs": {
        "meta": {
            "status": "ok",
            "statuscode": 200,
            "message": null
        },
        "data": [{
            "id": "9",
            "share_type": 0,
            "uid_owner": "eita",
            "displayname_owner": "eita",
            "permissions": 31,
            "stime": 1481109710,
            "parent": null,
            "expiration": null,
            "token": null,
            "uid_file_owner": "eita",
            "displayname_file_owner": "eita",
            "path": "\/test",
            "item_type": "folder",
            "mimetype": "httpd\/unix-directory",
            "storage_id": "home::eita",
            "storage": "1",
            "item_source": 10,
            "file_source": 10,
            "file_parent": 2,
            "file_target": "\/test",
            "share_with": "nextcloud3",
            "share_with_displayname": "nextcloud3",
            "mail_send": 0
        }]
    }
}
```


#### (create share) share folder with user in nextcloud:
* Request URL: http://nextcloud.localhost/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json
* Request Method: POST
* Form Data: 
  * Share type: local user
    * shareType:0
    * shareWith:nextcloud3
    * permissions:31
    * path:/test
  * Share type: remote user (by e-mail)
    * shareType:6
    * shareWith:andrelsm@iname.com
    * permissions:31
    * path:/test
  * Share by link
    * password:
    * passwordChanged:false
    * permissions:31
    * expireDate:
    * shareType:3
    * path:/test
* Successful Response:

```javascript
{
    "ocs": {
        "meta": {
            "status": "ok",
            "statuscode": 200,
            "message": null
        },
        "data": {
            "id": "9",
            "share_type": 0,
            "uid_owner": "eita",
            "displayname_owner": "eita",
            "permissions": 31,
            "stime": 1481109710,
            "parent": null,
            "expiration": null,
            "token": null,
            "uid_file_owner": "eita",
            "displayname_file_owner": "eita",
            "path": "\/test",
            "item_type": "folder",
            "mimetype": "httpd\/unix-directory",
            "storage_id": "home::eita",
            "storage": "1",
            "item_source": 10,
            "file_source": 10,
            "file_parent": 2,
            "file_target": "\/test",
            "share_with": "nextcloud3",
            "share_with_displayname": "nextcloud3",
            "mail_send": 0
        }
    }
}

//No caso de um sharing de link, os dados são praticamente iguais aos acima, 
//Apenas o campo url a mais e o share_with*=null :

            "share_with": null,
            "share_with_displayname": null,
            "url": "http:\/\/nextcloud.localhost\/index.php\/s\/1r0vhpmpclgCYLO",
```
* Unsuccessful Response:
```javascript
{
    "ocs": {
        "meta": {
            "status": "failure",
            "statuscode": 403,
            "message": "Sharing test failed, could not find andrelsm@iname.com, maybe the server is currently unreachable."
        },
        "data": []
    }
}
```

#### share: delete

* Request URL: http://nextcloud.localhost/ocs/v2.php/apps/files_sharing/api/v1/shares/8?format=json
* Request Method: DELETE
* Response: 
```javascript
{
    "ocs": {
        "meta": {
            "status": "ok",
            "statuscode": 200,
            "message": null
        },
        "data": []
    }
}
```
