/*
 * Copyright (c) 2015
 *
 * This file is licensed under the Affero General Public License version 3
 * or later.
 *
 * See the COPYING-README file.
 *
 */

(function() {
	/**
	 * @class OCA.MatrixBridge.RoomModel
	 * @classdesc
	 *
	 * Displays activity information for a given file
	 *
	 */
	var RoomModel = OC.Backbone.Model.extend(/** @lends OCA.MatrixBridge.RoomModel.prototype */{
	});

	OCA.MatrixBridge = OCA.MatrixBridge || {};
	OCA.MatrixBridge.RoomModel = RoomModel;
})();

