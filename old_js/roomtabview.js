/*
 * Copyright (c) 2017
 *
 * This file is licensed under the Affero General Public License version 3
 * or later.
 *
 * See the COPYING-README file.
 *
 */

(function() {
	var TEMPLATE =
		'<div class="chatroom-section">' +
			'<div class="chatrooms_title">Link this folder to a matrix.org chat room</div>' +
		'<div class="loading hidden" style="height: 50px"></div>' +
		'<div class="emptycontent">' +
		'    <div class="icon-chatroom"></div>' +
		'    <p>{{emptyMessage}}</p>' +
		'</div>' +
		'<ul class="chatrooms hidden"></ul>' +
		'<input type="button" class="applyButton" value="{{applyLabel}}"' +
		'</div>';
	var CHATROOM_TEMPLATE =
		'    <li class="chatroom box">' +
		'		<p>' +
		'		<input type="checkbox" class="selectRoom"/>'+
		'		<label>{{{room_name}}}</label>' +
		'		</p>' +
		'    </li>';

	/**
	 * @class OCA.MatrixBridge.RoomTabView
	 * @classdesc
	 *
	 * Displays chatroom information for a given file
	 *
	 */
	var RoomTabView = OCA.Files.DetailTabView.extend(/** @lends OCA.MatrixBridge.RoomTabView.prototype */ {
		id: 'chatroomTabView',
		className: 'chatroomTabView tab',

		events: {
			'click .applyButton': '_onClickApplyButton'
		},

		_loading: false,
		_plugins: [],

		initialize: function() {
			this.collection = new OCA.MatrixBridge.RoomCollection();
			this.collection.setObjectType('files');
			this.collection.on('request', this._onRequest, this);
			this.collection.on('sync', this._onEndRequest, this);
			this.collection.on('error', this._onError, this);
			this.collection.on('add', this._onAddModel, this);

			this._plugins = OC.Plugins.getPlugins('OCA.MatrixBridge.RenderingPlugins');
			_.each(this._plugins, function(plugin) {
				if (_.isFunction(plugin.initialize)) {
					plugin.initialize();
				}
			});
		},

		template: function(data) {
			if (!this._template) {
				this._template = Handlebars.compile(TEMPLATE);
			}
			return this._template(data);
		},

		get$: function() {
			return this.$el;
		},

		getLabel: function() {
			return t('matrixbridge', 'Chat Rooms');
		},

		setFileInfo: function(fileInfo) {
			this._fileInfo = fileInfo;
			if (this._fileInfo) {
				this.collection.setObjectId(this._fileInfo.get('id'));
				this.collection.reset();
				this.collection.fetch();

				_.each(this._plugins, function(plugin) {
					if (_.isFunction(plugin.setFileInfo)) {
						plugin.setFileInfo('files', fileInfo.get('id'));
					}
				});
			} else {
				this.collection.reset();

				_.each(this._plugins, function(plugin) {
					if (_.isFunction(plugin.resetFileInfo)) {
						plugin.resetFileInfo();
					}
				});
			}
		},

		/**
		 * Returns true for files, false for folders.
		 *
		 * @return {bool} false for files, true for folders
		 */
		canDisplay: function(fileInfo) {
			if (!fileInfo) {
				return false;
			}
			return fileInfo.isDirectory();
		},

		_onError: function() {
			var errorMessage = this.collection.errorMessage;
			if (_.isEmpty(errorMessage)) {
				errorMessage = t('matrixbridge', 'An error occurred while loading chatrooms');
			}

			var $emptyContent = this.$el.find('.emptycontent');
			$emptyContent.removeClass('hidden');
			$emptyContent.find('p').text(errorMessage);
		},

		_onRequest: function() {
			if (this.collection.lastGivenId === 0) {
				this.render();
			}
			this.$el.find('.applyButton').addClass('hidden');
		},

		_onEndRequest: function() {
			this.$container.removeClass('hidden');
			this.$el.find('.loading').addClass('hidden');
			if (this.collection.length) {
				this.$el.find('.emptycontent').addClass('hidden');
			}
			this.$el.find('.applyButton').removeClass('hidden');
		},

		_onClickApplyButton: function() {
			console.log('apply');
			alert('apply!');
			/*
			this.collection.fetch({
				reset: false
			});
			*/
		},

		/**
		 * Format an chatroom model for display
		 *
		 * @param {OCA.MatrixBridge.RoomModel} chatroom
		 * @return {Object}
		 */
		_formatItem: function(chatroom) {

			var output = chatroom.attributes;

			/*
			var subject = chatroom.get('subject'),
				subject_rich = chatroom.get('subject_rich');
			if (subject_rich[0].length > 1) {
				subject = OCA.MatrixBridge.RichObjectStringParser.parseMessage(subject_rich[0], subject_rich[1]);
			}
			var message = chatroom.get('message'),
				message_rich = chatroom.get('message_rich');
			if (message_rich[0].length > 1) {
				message = OCA.MatrixBridge.RichObjectStringParser.parseMessage(message_rich[0], message_rich[1]);
			}

			var output = {
				subject: subject,
				formattedDate: chatroom.getRelativeDate(),
				formattedDateTooltip: chatroom.getFullDate(),
				timestamp: moment(chatroom.get('datetime')).valueOf(),
				message: message,
				icon: chatroom.get('icon')
			}; */

			/**
			 * Disable previews in the rightside bar,
			 * it's always the same image anyway.
			 if (chatroom.has('previews')) {
					output.previews = _.map(chatroom.get('previews'), function(data) {
						return {
							previewClass: data.isMimeTypeIcon ? 'preview-mimetype-icon': '',
							source: data.source
						};
					});
				}
			 */
			return output;
		},

		chatroomTemplate: function(params) {
			if (!this._chatroomTemplate) {
				this._chatroomTemplate = Handlebars.compile(CHATROOM_TEMPLATE);
			}

			return this._chatroomTemplate(params);
		},

		_onAddModel: function(model, collection, options) {
			var $el = $(this.chatroomTemplate(this._formatItem(model)));

			_.each(this._plugins, function(plugin) {
				if (_.isFunction(plugin.prepareModelForDisplay)) {
					plugin.prepareModelForDisplay(model, $el, 'RoomTabView');
				}
			});

			if (!_.isUndefined(options.at) && collection.length > 1) {
				this.$container.find('li').eq(options.at).before($el);
			} else {
				this.$container.append($el);
			}

			this._postRenderItem($el);
		},

		_postRenderItem: function($el) {
			$el.find('.avatar').each(function() {
				var element = $(this);
				if (element.data('user-display-name')) {
					element.avatar(element.data('user'), 21, undefined, false, undefined, element.data('user-display-name'));
				} else {
					element.avatar(element.data('user'), 21);
				}
			});
			$el.find('.avatar-name-wrapper').each(function() {
				var element = $(this);
				var avatar = element.find('.avatar');
				var label = element.find('strong');

				$.merge(avatar, label).contactsMenu(element.data('user'), 0, element);
			});
			$el.find('.has-tooltip').tooltip({
				placement: 'bottom'
			});
		},


		/**
		 * Renders this details view
		 */
		render: function() {
			if (this._fileInfo) {
				this.$el.html(this.template({
					emptyMessage: t('matrixbridge', 'No chatroom yet'),
					applyLabel: t('matrixbridge', 'Apply changes')
				}));
				this.$container = this.$el.find('ul.chatrooms');
			}
		}
	});

	OCA.MatrixBridge = OCA.MatrixBridge || {};
	OCA.MatrixBridge.RoomTabView = RoomTabView;
})();

