$(document).ready(function() {
	$('iframe').on('load',function() {
		var iframe = document.getElementsByTagName('iframe')[0];
		var win;
		try {
			win = iframe.contentWindow;
		} catch(e) {
			win = iframe.contentWindow;
		}
		$.ajax({ url: 'matrix_credentials/logout'}).done(function(data) {
			var msg = {accessToken: data.matrix_token, homeserverUrl: data.home_server_url, identityServerUrl: data.identity_server_url, userId: data.matrix_username, guest: "false", action: 'im.vector.logout'};
			console.log(msg);
			win.postMessage(JSON.stringify(msg), "*");
		});
	});
});
