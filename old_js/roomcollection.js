/*
 * Copyright (c) 2015
 *
 * This file is licensed under the Affero General Public License version 3
 * or later.
 *
 * See the COPYING-README file.
 *
 */

(function() {

	OCA.MatrixBridge = OCA.MatrixBridge || {};

	/**
	 * @class OCA.MatrixBridge.RoomCollection
	 * @classdesc
	 *
	 * Displays activity information for a given file
	 *
	 */
	var RoomCollection = OC.Backbone.Collection.extend(
		/** @lends OCA.MatrixBridge.RoomCollection.prototype */ {

		firstKnownId: 0,
		lastGivenId: 0,
		hasMore: false,

		errorMessage: '',

		/**
		 * Id of the file for which to filter activities by
		 *
		 * @var int
		 */
		_objectId: null,

		/**
		 * Type of the object to filter by
		 *
		 * @var string
		 */
		_objectType: null,

		model: OCA.MatrixBridge.RoomModel,

		/**
		 * Sets the object id to filter by or null for all.
		 * 
		 * @param {int} objectId file id or null
		 */
		setObjectId: function(objectId) {
			this._objectId = objectId;
			this.firstKnownId = 0;
			this.lastGivenId = 0;
			this.hasMore = false;
		},

		/**
		 * Sets the object type to filter by or null for all.
		 * 
		 * @param {string} objectType string
		 */
		setObjectType: function(objectType) {
			this._objectType = objectType;
			this.firstKnownId = 0;
			this.lastGivenId = 0;
			this.hasMore = false;
		},

		/**
		 *
		 * @param ocsResponse
		 * @param response
		 * @returns {Array}
		 */
		parse: function(response, options) {
			this._saveHeaders(options.xhr.getAllResponseHeaders());

			if (options.xhr.status === 204) {
				// No rooms found
				return [];
			} else if (options.xhr.status === 400) {
				//Server error
				this.errorMessage = response.meta.message;
				return [];
			}

			return response.data.rooms;
		},

		/**
		 * Read the X-Room-First-Known and X-Room-Last-Given headers
		 * @param headers
		 */
		_saveHeaders: function(headers) {
			var self = this;
			this.hasMore = false;

			headers = headers.split("\n");
			_.each(headers, function (header) {
				var parts = header.split(':');
				if (parts[0].toLowerCase() === 'x-activity-first-known') {
					self.firstKnownId = parseInt(parts[1].trim(), 10);
				} else if (parts[0].toLowerCase() === 'x-activity-last-given') {
					self.lastGivenId = parseInt(parts[1].trim(), 10);
				} else if (parts[0].toLowerCase() === 'link') {
					self.hasMore = true;
				}
			});
		},

		url: function() {
			var query = {
				format: 'json'
			};
			var url = OC.generateUrl('apps/matrixbridge/rooms', 2);
			if (this.lastGivenId) {
				query.since = this.lastGivenId;
			}
			if (this._objectId && this._objectType) {
				query.object_type = this._objectType;
				query.object_id = this._objectId;
			}
			url += '?' + OC.buildQueryString(query);
			return url;
		}
	});

	OCA.MatrixBridge.RoomCollection = RoomCollection;
})();

