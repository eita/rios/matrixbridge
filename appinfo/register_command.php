<?php

use OCA\Circles\Db\CircleRequest;
use OCA\Circles\Service\CircleService;
use OCA\Circles\Service\FederatedUserService;
use OCA\Circles\Service\MemberService;

$userManager = \OC::$server->getUserManager();
$roomCircleMapper = \OC::$server->query(OCA\MatrixBridge\Db\RoomCircleMapper::class);
$matrixUserMapper = \OC::$server->query(OCA\MatrixBridge\Db\MatrixUserMapper::class);
$userSession = \OC::$server->getUserSession();

//Circles classes
$circleService = \OC::$server->get(CircleService::class);
$memberService = \OC::$server->get(MemberService::class);
$federatedUserService = \OC::$server->get(FederatedUserService::class);
$circleRequest = \OC::$server->get(CircleRequest::class);


$application->add(new OCA\MatrixBridge\Command\SyncRoomsToCircles(
    $userManager,
    $roomCircleMapper,
    $matrixUserMapper,
    $userSession,
    $circleService,
    $memberService,
    $federatedUserService,
    $circleRequest
));


