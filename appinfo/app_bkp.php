<?php
/**
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 * @author Daniel Tygel <dtygel@eita.org.br>
 * @author Alan Freihof Tygel <alan@eita.org.br>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use OCA\MatrixBridge\AppInfo\Application;
use OCA\MatrixBridge\Helper;

if(class_exists('\\OCP\\AppFramework\\Http\\EmptyContentSecurityPolicy')) {
	$helper = new Helper(\OC::$server->getConfig());
	$manager = \OC::$server->getContentSecurityPolicyManager();
	$policy = new \OCP\AppFramework\Http\EmptyContentSecurityPolicy();
	$homeserverUrl = $helper->getHomeServerUrl();
	$policy->addAllowedConnectDomain($homeserverUrl);
	$policy->addAllowedScriptDomain($homeserverUrl);
	$policy->addAllowedChildSrcDomain($homeserverUrl);
	$manager->addDefaultPolicy($policy);

	$linkToJs = \OC::$server->getURLGenerator()->linkTo('matrixbridge','js/badge.js') . "?v=1";

	\OCP\Util::addHeader(
		'script',
		[
			'src' => $linkToJs,
			'nonce' => \OC::$server->getContentSecurityPolicyNonceManager()->getNonce()
		], ''
	);
}

$app = \OC::$server->query(Application::class);
$app->register();






