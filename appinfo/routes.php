<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\MatrixBridge\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
	'ocs' => [
		['name' => 'Rooms#get', 'url' => '/rooms', 'verb' => 'GET'],
	],
    'routes' => [
        ['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
        ['name' => 'page#logout', 'url' => '/logout', 'verb' => 'GET'],
        ['name' => 'page#do_echo', 'url' => '/echo', 'verb' => 'POST'],
        ['name' => 'page#matrix_credentials', 'url' => '/matrix_credentials/{param}', 'verb' => 'GET'],
        ['name' => 'matrix#rooms', 'url' => '/matrix_api/rooms', 'verb' => 'GET'],
        ['name' => 'matrix#transactions', 'url' => '/matrix_api/transactions/{id}', 'verb' => 'PUT'],
        ['name' => 'matrix#users', 'url' => '/matrix_api/users', 'verb' => 'GET'],
        ['name' => 'settings#updateSetting', 'url' => '/ajax/updateSetting', 'verb' => 'POST'],
        ['name' => 'page#testing', 'url' => '/test', 'verb' => 'GET'],
        ['name' => 'matrix#roomSharedFolders', 'url' => '/room_shared_folders', 'verb' => 'GET'],
]
];
