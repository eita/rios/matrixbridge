# Nextcloud - Matrix Bridge

**💬 Matrix integrated to Nextcloud**

- **Rooms integrated with circles:** each matrix room have a correspondent Nextcloud circle. Users can share files, calendars and others with this circle, and a notification is sent to the room.
- **Element inside Nextcloud:** the Element matrix web interface is integrated to the nextcloud interface.  

## Installation

This plugin assumes that the Matrix/Synapse server and the Nextcloud installation haves the same user base. We've made this work using two setups:

* Using a LDAP server as user base for both Synapse and Nextcloud.
* Having synpase using Nextcloud as user base using OAuth/OIDC (beta)

### Method 1: Using LDAP server as common user base for Matrix/Synapse and Nextcloud

#### Installation of the nextcloud plugin

```bash
# in nextcloud root
cd apps
git clone https://gitlab.com/eita/rios/matrixbridge.git
cd matrixbridge
git checkout rios-vivos-24
git submodule update --init --recursive
```

#### Configuration of nextcloud plugin

#### Instalation and configuration of LDAP

#### Instalation and configuration of synapse

### Method 2: Configure Synapse to use Nextcloud as user base.

TODO complete documentation.
