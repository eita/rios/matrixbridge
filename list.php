<?php
/**
 * @copyright Copyright (c) 2016, Cooperativa Eita (eita.org.br)
 *
 * @author Vinicius Brand <vinicius@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */
// Check if we are a user
OCP\User::checkLoggedIn();

$tmpl = new OCP\Template('matrixbridge', 'list', '');

/* Load Status Manager */
#\OCP\Util::addStyle('matrixbridge', 'external');
#\OCP\Util::addScript('matrixbridge', 'statusmanager');
#\OCP\Util::addScript('matrixbridge', 'rollingqueue');

OCP\Util::addScript('matrixbridge', 'app');
#OCP\Util::addScript('matrixbridge', 'mountsfilelist');

$tmpl->printPage();
