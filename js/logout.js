$(document).ready(function() {
	$('iframe').on('load', function() {
		const iframe = document.getElementsByTagName('iframe')[0]
		let win
		try {
			win = iframe.contentWindow
		} catch (e) {
			win = iframe.contentWindow
		}
		$.ajax({ url: 'matrix_credentials/logout' }).done(function(data) {
			const msg = { accessToken: data.matrix_token, homeserverUrl: data.home_server_url, identityServerUrl: data.identity_server_url, userId: data.matrix_username, guest: 'false', action: 'im.vector.logout' }
			// eslint-disable-next-line no-console
			console.log(msg)
			win.postMessage(JSON.stringify(msg), '*')
		})
	})
})
