let iframe
let win
let refreshIntervalId
const badge = { rooms: {}, count: 0 }
let globalCredentials

$.ajax({ url: OC.generateUrl('apps/matrixbridge/matrix_credentials/stored') }).done(function(data) {
	//$('#matrix_loading').hide();
	if (data == null || data.error) {
		console.log('caso 1. data:')
		console.log(data)
		if (iframe) {
			$('#matrix_loading').hide()
			$('#matrix_error').show()
			iframe.remove()
		}
	} else {
		console.log('caso 2')
		console.log(data)
		globalCredentials = data
		if (iframe) {
			let n = 0
			const msg = {
				accessToken: data.matrix_token,
				homeserverUrl: data.home_server_url,
				identityServerUrl: data.identity_server_url,
				userId: data.matrix_username,
				deviceId: data.device_id,
				action: (data.is_new_session) ? 'im.vector.newLogin' : 'im.vector.login',
			}
			refreshIntervalId = setInterval(function() {
				if (win) {
					// console.log('postMessageParent: sent login request', msg);
					win.postMessage(JSON.stringify(msg), '*')
					n++
					if (n === 1000) {
						clearInterval(refreshIntervalId)
					}
				}
			}, 300)
		}
	}
}).fail(function(data) {
	console.log('caso 3')
	clearInterval(refreshIntervalId)
	if (iframe) {
		$('#matrix_loading').hide()
		$('#matrix_error').show()
		iframe.remove()
	}
})

$(document).ready(function() {
	$('[data-id="matrixbridge"] a').append("<div class='w3-badge' id='badgeMatrixBridge'></div>")
	badgeElement = $('#badgeMatrixBridge')
	iframe = $('#matrixIframe')
	if (iframe) {
		iframe.on('load', function() {
  		try {
  			win = iframe[0].contentWindow
  		} catch (e) {
  			win = iframe[0].contentWindow
  		}

  		// authentication
  		window.addEventListener('message', function(event) {
				// if(event.origin !== destUrl) return; TODO: pass valid destination url to javascript script
				let data = {}
				try {
					data = JSON.parse(event.data)
					if (data.status == 'im.vector.notif_count') {
						let title = document.title.replace(/\(.*?\) /, '')
						if (data.msg == 0) {
							badgeElement.hide()
						} else {
							title = '(' + data.msg + ') ' + title
							badgeElement
								.text(data.msg)
								.show()
						}
						document.title = title
					} else if (data.action == 'im.vector.newLogin') {
						clearInterval(refreshIntervalId)
						console.log('postMessageParent: data received:', event.data)
						$.ajax({ url: OC.generateUrl('apps/matrixbridge/matrix_credentials/new') }).done(function(newLogin) {
							$('#matrix_loading').hide()
							if (newLogin == null || newLogin.error) {
								if (iframe) {
									$('#matrix_loading').hide()
									$('#matrix_error').show()
									iframe.remove()
								}
							} else {
								if (iframe && win) {
									const msg = {
										accessToken: newLogin.matrix_token,
										homeserverUrl: newLogin.home_server_url,
										identityServerUrl: newLogin.identity_server_url,
										userId: newLogin.matrix_username,
										deviceId: newLogin.device_id,
										action: 'im.vector.newLogin',
									}
									console.log('postMessageParent: I\'m forcing a new login')
									win.postMessage(JSON.stringify(msg), '*')
								}
							}
						}).fail(function(failed) {
							if (iframe) {
								$('#matrix_loading').hide()
								$('#matrix_error').show()
								iframe.remove()
							}
						})
					}

					if (data.status != 'im.vector.loading') {
						clearInterval(refreshIntervalId)
						$('#matrix_loading').hide()
						if (data.status == 'im.vector.error') {
							console.log('postMessageParent: error! Message is "' + data.msg + '"')
	                $('#matrix_error').show()
						} else {
							iframe.css('visibility', 'visible')
						}
					}
				} catch (e) {
					console.log('postMessageParent: error! Data from rios-chat-web is not a well formatted JSON!')

				}
			}, false)

  		// setTimeout(function() {
  		// 	$('#matrix_loading_text').append('<span> (caso venha a demorar muito a carregar, tente recarregar a p&aacute;gina com o bot&atilde;o SHIFT apertado)</span>');
  		// },5000);

  		// get iframe window title and propagate to the current window
  		try {
  			const target = win.document.querySelector('head > title')
  			const i = 1
  		} catch (e) {
  		}
  	})
	}
})

/**
 * @param home_server_url
 * @param matrix_token
 * @param next_batch
 */
function updateBadge(home_server_url, matrix_token, next_batch = null) {

	const filter = '&filter={"presence":{"not_types":["*"],"limit":0},"account_data":{"not_types":["*"],"limit":0},"room":{"timeline":{"types":["m.room.message"],"limit":1},"ephemeral":{"not_types":["*"],"limit":0},"state":{"not_types":["*"],"limit":0},"account_data":{"not_types":["*"],"limit":0}},"event_fields":["sender"]}'
	let url = home_server_url + '/_matrix/client/r0/sync?access_token=' + matrix_token + filter
	if (next_batch) {
		url += '&since=' + next_batch
	}
	$.get(url, function(data) {
		const joins = data.rooms.join
		const oldBadge = {}
		for (const key in joins) {
			oldBadge[key] = badge.rooms.hasOwnProperty(key)
				? badge.rooms[key]
				: 0
    	if (joins.hasOwnProperty(key) && joins[key].unread_notifications.hasOwnProperty('notification_count')) {
				badge.rooms[key] = joins[key].unread_notifications.notification_count
    	} else {
				badge.rooms[key] = 0
			}
			badge.count += (badge.rooms[key] - oldBadge[key])
		}
		let title = document.title.replace(/\(.*?\) /, '')
		if (badge.count == 0) {
			badgeElement.hide()
		} else {
			title = '(' + badge.count.toString() + ') ' + title
			badgeElement
				.text(badge.count.toString())
				.show()
		}
		document.title = title
		setTimeout(function() { updateBadge(home_server_url, matrix_token, data.next_batch) }, 5000)
	})
}
