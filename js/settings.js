/**
 * @author Vinicius Brand <vinicius@eita.org.br>
 *
 * @copyright Copyright (c) 2017, Vinicius Brand
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your opinion) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

/**
 * @param setting
 * @param value
 */
function setMatrixBridgeValue(setting, value) {
	OC.msg.startSaving('#matrixbridge_settings_msg')
	$.post(
		OC.generateUrl('/apps/matrixbridge/ajax/updateSetting'), { setting, value }
	).done(function(response) {
		OC.msg.finishedSaving('#matrixbridge_settings_msg', response)
		hideUndoButton(setting, value)
	}).fail(function(response) {
		OC.msg.finishedSaving('#matrixbridge_settings_msg', response)
	})
	preview(setting, value)
}

$(document).ready(function() {

	$('#matrixbridge-group-rooms-owner, #matrixbridge-riot-url, #matrixbridge-group-rooms-owner-pw, #matrixbridge-homeserver-url, #matrixbridge-identifiers-domain').change(function(e) {
		const el = $(this)
		const self = this
		$.when(el.focusout()).then(function() {
			setMatrixBridgeValue(self.id, $(this).val())
		})
		if (e.keyCode == 13) {
			setMatrixBridgeValue(self.id, $(this).val())
		}
	})

})
