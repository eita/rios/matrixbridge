<?php

use OCA\MatrixBridge\Helper;
use OCA\MatrixBridge\Service\ApiFactory;
use Test\TestCase;

class MatrixIntegrationTest extends TestCase {

	/** @var Helper */
	private $helper;

	public function setUp() {
		parent::setUp();
		$this->helper = new Helper(\OC::$server->getConfig());

	}

	public function testHomeserverConfigured() {
		$this->assertNotEmpty($this->helper->getHomeserverUrl(),"Homeserver must be configured.");
	}

	//Test connection to matrix server
	public function testMatrixServerOnline() {
		$header_check = get_headers($this->helper->getHomeserverUrl());
		$response_code = $header_check[0];
		$this->assertTrue(preg_match("/200/",$response_code) || preg_match("/302/", $response_code),
			"Could not connect to matrix server (is it online?)");
	}

	//Authenticate and generate new token
	public function testLoginToMatrixServerWithCredentials() {
		$this->assertNotEmpty($this->helper->getTestUserName(),"A Test user must be defined to run matrix tests.");
		$testUserName = $this->helper->getTestUserName();
		$testPassword = $this->helper->getTestUserPassword();
		$api = ApiFactory::createApi($this->helper->getHomeserverUrl(), $this->helper->getTestUserName(), $this->helper->getTestUserPassword());

		//Exception not thrown - credentials worked
		$this->assertTrue(true);
	}

	public function testFailLoginToMatrixServer() {
		$this->expectException(MatrixOrg_Exception_Connection::class);
		$api = ApiFactory::createApi($this->helper->getHomeserverUrl(), "random_dummy_blablabla", "  ");
	}

	//Connects in the matrix server using user's token

	//Gets information about a room





}