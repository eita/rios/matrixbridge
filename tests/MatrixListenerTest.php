<?php

use OCA\Circles\Service\CirclesService;
use OCA\MatrixBridge\Service\MatrixListener;
use OCA\MatrixBridge\Db\RoomCircleMapper;
use Test\TestCase;

/**
 *  @group DB
 */
class MatrixListenerTest extends TestCase {

	private $container;
	private $app;

	/** @var MatrixListener */
	private $matrixListener;

	/** @var RoomCircleMapper */
	private $roomCircleMapper;

	/** @var CirclesService */
	private $circlesServiceMock;

	private $domain = "synapse.localhost";
	private $mainUser = "rosanak";

	private $tplVars = [];

	private $roomName;
	private $roomId;
	private $userId;
	private $userDisplayName;

	public function setUp() {
		parent::setUp();
		$this->app = new \OCA\MatrixBridge\AppInfo\Application();

		//$this->helper = new Helper(\OC::$server->getConfig());
		$this->container = $this->app->getContainer();

		$this->matrixListener = \OC::$server->query(OCA\MatrixBridge\Service\MatrixListener::class);
		$this->roomCircleMapper = \OC::$server->query(OCA\MatrixBridge\Db\RoomCircleMapper::class);

		$this->tplVars['ROOM_NAME'] = "test_".$this->randomString();
		$this->tplVars['ROOM_ID'] = "!test{$this->randomString(14)}:{$this->domain}";
		$this->tplVars['USER_ID'] = "@{$this->mainUser}:{$this->domain}";
		$this->tplVars['USER_DISPLAYNAME'] = $this->mainUser;

		$this->circlesServiceMock =  $this->getMockBuilder(CirclesService::class)
			->disableOriginalConstructor()
			->getMock();

		$a = 1

		$cloj = function () use ($a){ return $this->circlesServiceMock};

			$this->container->registerService(CirclesService::class,
			} );
	}

	#FIXME these tests are dependent of one another.
	public function testOnRoomCreate() {
		$payloads = $this->loadPayloads('1_create_room');
		$payloads = $this->replaceVariables($payloads,$this->tplVars);

		$this->circlesServiceMock->expects($this->once())
			->method('createCircle');

		foreach ($payloads as $payload) {
			$this->matrixListener->setEventBody($payload);
			$this->matrixListener->transactionsHandler();
		}

//		$circle = $this->roomCircleMapper->getCircle($this->tplVars['ROOM_NAME']);

//		$this->assertNotNull($circle);
//		$this->assertEquals($this->tplVars['ROOM_NAME'], $circle->getName());



	}

	private function randomString($length=10) {
		return substr(md5(mt_rand()), 0, $length);
	}

	private function loadPayloads($folderName): array {
		$files = glob(dirname(__FILE__) . "/payloads/$folderName/[0-9]*");
		if ($files) {
			sort($files);
		}
		$files_content = [];

		foreach ($files as $filename) {
			$files_content[] = file_get_contents($filename);
		}

		return $files_content;
	}

	private function replaceVariables($payloads, $tplVars): array {
		$new_payloads = [];
		$keys = array_keys($tplVars);
		$keys = array_map(function($key) { return '{'.$key.'}';},$keys);
		foreach ($payloads as $index => $payload) {

			$new_payloads[$index] = str_replace($keys, array_values($tplVars), $payload);
		}
		return $new_payloads;
	}


	//(m.room.member) User enters the room (public room) -> user enters the circle
	//(m.room.member) Invite user to room -> invite user to circle
	//(m.room.member) User accepts invitation to room -> User accepts invitation to circle
	//(m.room.member) Remove/Ban/Kick user of room -> remove user of circle
	//(m.room.member) User leaves the room -> remove user of circle
	//(m.room.power_levels) Change membership status in room -> change membership status in circle
	//(m.room.redaction) Delete room -> deletes circle (on user leave)
	//(m.room.name) Changes room name -> changes circle name
	//(m.room.message) File sent to the room (in a message)-> downloads file to nextcloud, share it with circle (add filesize limit?)
	//Message deleted -> delete file and share
	//Circle permissions are the same as room (public circle/public room, etc)
	//Change user avatar on matrix -> changes user avatar on nextcloud, and do not propagate again to matrix
	//Share file with circle related to room



}