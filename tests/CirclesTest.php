<?php


use OCA\Circles\Service\CirclesService;
use OCA\Circles\Service\MembersService;
use OCA\MatrixBridge\Helper;
use OCA\Circles\AppInfo\Application;
use OCP\IDBConnection;
use Test\TestCase;

class CirclesTest extends TestCase {
	private $container;
	private $app;

	/** @var CirclesService|\PHPUnit_Framework_MockObject_MockObject */
	private $circlesService;


	/** @var MembersService|\PHPUnit_Framework_MockObject_MockObject */
	private $membersService;

	/** @var Helper|\PHPUnit_Framework_MockObject_MockObject */
	private $helper;

	public function setUp() {
		parent::setUp();
		$this->app = new Application();
		$this->container = $this->app->getContainer();

		$this->circlesService = $this->createMock(CirclesService::class);
		$this->helper = $this->createMock(Helper::class);

		\OC::$server->registerService(IDBConnection::class, function () {
			return $this->createMock(IDBConnection::class);
		});
	}

	public function testCirclesAppInstalled() {
		$appManager = $this->container->query(\OCP\App\IAppManager::class);
		$this->assertTrue($appManager->isInstalled('circles'));
	}

	public function testChangeCirclesServiceUser() {
		$this->helper = new Helper(\OC::$server->getConfig());

		$this->circlesService =  $this->container->query(CirclesService::class);

		$userId = "abcde";

		//$this->helper->changeUserIdCirclesService($this->circlesService, $userId);

		$method = new ReflectionProperty('OCA\Circles\Service\CirclesService', 'userId');
		$method->setAccessible(true);

		$this->assertEquals($method->getValue($this->circlesService),$userId);
	}

	public function testChangeMembersServiceUser() {
		$this->helper = new Helper(\OC::$server->getConfig());

		$this->membersService =  $this->container->query(MembersService::class);

		$userId = "abcde";

		$this->helper->changeUserIdMembersService($this->membersService, $userId);

		$method = new ReflectionProperty('OCA\Circles\Service\MembersService', 'userId');
		$method->setAccessible(true);

		$this->assertEquals($method->getValue($this->membersService),$userId);
	}

}