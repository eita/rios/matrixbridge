<?php


use Test\TestCase;

class ApplicationTest extends TestCase {

	private $container;
	private $app;

	public function setUp(): void {
		parent::setUp();
		$this->app = new \OCA\MatrixBridge\AppInfo\Application();
		$this->container = $this->app->getContainer();
	}

	public function testAppInstalled() {
		$appManager = $this->container->query(\OCP\App\IAppManager::class);
		$this->assertTrue($appManager->isInstalled('matrixbridge'));
	}




}